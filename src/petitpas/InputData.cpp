/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "InputData.h"
#include "JsonInputFile.h"
#include "JsonResultExporter.h"
#include "VTKResultExporter.h"

#include "../core/ArgumentParserResult.h"
#include "../core/Mesh.h"
#include "../core/Analysis.h"
#include "../core/MaterialBehavior.h"
#include "../core/Logger.h"

#include <stdexcept>
#include <sstream>

namespace PetitPas
{

InputData::InputData(const ArgumentParserResult& result):
    mesh(NULL),
    analysis(NULL)
{
    // Check mandatory arguments.
    if(result.ContainsArgument("-i") == false)
    {
        throw std::runtime_error("No input file, use the '-i' option to specify one.");
    }

    if(result.ContainsArgument("-o") == false)
    {
        throw std::runtime_error("No output result file, use '-o' option to specify one.");
    }

    // Parse input file.
    std::string inputFilePath = result.GetArgumentValue("-i");
    JsonInputFile inputFile(inputFilePath);

    // Create solver data from input data.
    this->CreateAnalysis(inputFile);

    // Store output file path.
    this->resultFilePath = result.GetArgumentValue("-o");

    // Check output file format.
    if(result.ContainsArgument("-of"))
    {
        const std::string& format = result.GetArgumentValue("-of");
        if(format == "vtk")
            this->resultFileFormat = ResultFileFormat_VTK;
        else if (format == "json")
            this->resultFileFormat = ResultFileFormat_Json;
        else
            throw std::runtime_error("Unknown output file format in command line '-of'.");
    }
    else
    {
        // Default output format is Json.
        this->resultFileFormat = ResultFileFormat_Json;
    }
}

InputData::~InputData()
{
    this->ClearData();
}

void InputData::ClearData()
{
    delete this->analysis;
    this->analysis = NULL;

    delete this->mesh;
    this->mesh = NULL;

    for(std::map<std::string, MaterialBehavior*>::iterator materialIterator = this->materialBehaviors.begin();
        materialIterator != materialBehaviors.end();
        ++materialIterator)
    {
        delete materialIterator->second;
        materialIterator->second = NULL;
    }
    materialBehaviors.clear();
}

void InputData::CreateAnalysis(const JsonInputFile& inputFile)
{
    this->ClearData();

    this->mesh = new Mesh();
    this->analysis = new Analysis(*(this->mesh));

    // Get input objects from the input file representation.
    const std::list<InputNode>& inputNodes = inputFile.GetInputNodes();
    const std::list<InputElement>& inputElements = inputFile.GetInputElements();
    const std::list<InputMaterial>& inputMaterials = inputFile.GetInputMaterials();
    const std::list<InputBoundaryCondition>& inputBoundaryConditions = inputFile.GetInputBoundaryConditions();

    // Nodes.
    for(std::list<InputNode>::const_iterator nodeIterator = inputNodes.begin();
        nodeIterator != inputNodes.end();
        ++nodeIterator)
    {
        PetitPas::Point point(nodeIterator->coordinates);
        if(nodeIterator->id == 0)
        {
            throw std::runtime_error("The node id can not be zero.");
        }
        this->mesh->CreateNode(point, nodeIterator->id);
    }

    // Materials.
    for(std::list<InputMaterial>::const_iterator materialIterator = inputMaterials.begin();
        materialIterator != inputMaterials.end();
        ++materialIterator)
    {
        // Detect material hypothesis.
        MaterialBehavior::MaterialHypothesis materialHypothesis;
        if(materialIterator->hypothesis == "2d_plane_strain")
        {
            materialHypothesis = MaterialBehavior::MaterialHypothesis_2DPlaneStrain;
        }
        else if(materialIterator->hypothesis == "2d_plane_stress")
        {
            materialHypothesis = MaterialBehavior::MaterialHypothesis_2DPlaneStress;
        }
        else if(materialIterator->hypothesis == "3d")
        {
            materialHypothesis = MaterialBehavior::MaterialHypothesis_3D;
        }
        else
        {
            throw std::runtime_error("Unknown material hypothesis.");
        }

        // Check duplicate of material id.
        if(this->materialBehaviors.count(materialIterator->id) > 0)
            throw std::runtime_error("Duplicate material id.");

        // Finally create the material and save it.
        MaterialBehavior* newMaterial = new MaterialBehavior(materialIterator->youngModulus, materialIterator->poissonRatio, materialHypothesis);
        this->materialBehaviors.insert(std::pair<std::string, MaterialBehavior*>(materialIterator->id, newMaterial));
    }

    // Elements.
    for(std::list<InputElement>::const_iterator elementIterator = inputElements.begin();
        elementIterator != inputElements.end();
        ++elementIterator)
    {
        // Check that material exists.
        std::map<std::string, MaterialBehavior*>::const_iterator materialIterator = this->materialBehaviors.find(elementIterator->materialId);
        if(materialIterator == this->materialBehaviors.end())
        {
            throw std::runtime_error("Unknown material id for the element.");
        }

        if(elementIterator->id == 0)
        {
            throw std::runtime_error("The element id can not be zero.");
        }

        this->mesh->CreateElement(elementIterator->nodeIds, *(materialIterator->second), elementIterator->id);
    }

    // Boundary conditions.
    for(std::list<InputBoundaryCondition>::const_iterator boundaryConditionIterator = inputBoundaryConditions.begin();
        boundaryConditionIterator != inputBoundaryConditions.end();
        ++boundaryConditionIterator)
    {
        if(boundaryConditionIterator->type == "force")
        {
            this->analysis->SetForce(boundaryConditionIterator->nodeId, boundaryConditionIterator->degreeOfFreedomIndex, boundaryConditionIterator->value);
        }
        else if(boundaryConditionIterator->type == "zero_displacement")
        {
            if (boundaryConditionIterator->allDegreesOfFreedomIndex)
            {
                this->analysis->SetZeroDisplacement(boundaryConditionIterator->nodeId);
            }
            else
            {
                this->analysis->SetZeroDisplacement(boundaryConditionIterator->nodeId, boundaryConditionIterator->degreeOfFreedomIndex);
            }
        }
        else
        {
            throw std::runtime_error("Unknown boundary condition type.");
        }
    }

    // Write summary
    std::stringstream message;
    message << this->mesh->GetNodes().size() << " nodes" << std::endl;
    message << this->mesh->GetElements().size() << " elements";
    Logger::LogMessage(message.str());
}

const Analysis& InputData::GetAnalysis() const
{
    return (*this->analysis);
}

const std::string& InputData::GetResultFilePath() const
{
    return this->resultFilePath;
}

ResultExporter* InputData::GetResultExporter(const Solution& solution)
{
    ResultExporter* resultExporter = NULL;

    switch(this->resultFileFormat)
    {
    case ResultFileFormat_Json:
        resultExporter = new JsonResultExporter(solution);
        break;
    case ResultFileFormat_VTK:
        resultExporter = new VTKResultExporter(solution);
        break;
    default:
        throw std::runtime_error("Unknown result file format.");
    }

    return resultExporter;
}

} //namespace PetitPas
