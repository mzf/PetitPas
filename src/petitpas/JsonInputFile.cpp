/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "JsonInputFile.h"

#include <fstream>
#include <sstream>
#include <stdexcept>

namespace PetitPas
{

JsonInputFile::JsonInputFile(const std::string& jsonFilePath)
{
    // Open the file.
    std::ifstream inputFileStream(jsonFilePath.c_str());
    if(inputFileStream.good() == false)
    {
        std::stringstream message;
        message << "Can not open the given input file: ";
        message << jsonFilePath;
        throw std::runtime_error(message.str());
    }

    // Create a json object from the file.
    Json::Value jsonRoot;
    Json::Reader jsonReader;
    bool isParsingSuccessful = jsonReader.parse(inputFileStream, jsonRoot);
    if(!isParsingSuccessful)
    {
        // Failure while parsing the json file.
        std::stringstream messageStream;
        messageStream << "Failed to parse the input file: ";
        messageStream << jsonReader.getFormattedErrorMessages();
        throw std::runtime_error(messageStream.str());
    }

    // Validate the json structure.
    this->ValidateInputSchema(jsonRoot);

    // Create object representations of the data.
    this->CreateInputObjects(jsonRoot);
}

const std::list<InputNode>& JsonInputFile::GetInputNodes() const
{
    return this->inputNodes;
}

const std::list<InputElement>& JsonInputFile::GetInputElements() const
{
    return this->inputElements;
}

const std::list<InputMaterial>& JsonInputFile::GetInputMaterials() const
{
    return this->inputMaterials;
}

const std::list<InputBoundaryCondition>& JsonInputFile::GetInputBoundaryConditions() const
{
    return this->inputBoundaryConditions;
}

void JsonInputFile::ValidateInputSchema(const Json::Value& jsonRoot)
{
    // Nodes.
    if(!jsonRoot.isMember("nodes"))
    {
        throw std::runtime_error("The json input file must have a 'nodes' entry.");
    }
    this->ValidateNodesSchema(jsonRoot["nodes"]);

    // Elements.
    if(!jsonRoot.isMember("elements"))
    {
        throw std::runtime_error("The json input file must have a 'elements' entry.");
    }
    this->ValidateElementsSchema(jsonRoot["elements"]);

    // Materials.
    if(!jsonRoot.isMember("materials"))
    {
        throw std::runtime_error("The json input file must have a 'materials' entry.");
    }
    this->ValidateMaterialsSchema(jsonRoot["materials"]);

    // Boundary Conditions.
    if(!jsonRoot.isMember("boundary_conditions"))
    {
        throw std::runtime_error("The json input file must have a 'boundary_conditions' entry.");
    }
    this->ValidateBoundaryConditionsSchema(jsonRoot["boundary_conditions"]);
}

void JsonInputFile::ValidateNodesSchema(const Json::Value& jsonNodes)
{
    if(!jsonNodes.isArray())
    {
        throw std::runtime_error("The json 'nodes' object must be an array.");
    }

    unsigned int nodeCount = jsonNodes.size();
    if(nodeCount == 0)
    {
        throw std::runtime_error("The json 'nodes' array must not be empty.");
    }

    // Check nodes structure.
    for ( unsigned int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++ )
    {
        const Json::Value& jsonNode = jsonNodes[nodeIndex];

        // Check 'id'.
        if(!jsonNode.isMember("id"))
        {
            throw std::runtime_error("The node must have an 'id' field.");
        }

        if(!jsonNode["id"].isUInt())
        {
            throw std::runtime_error("A node id must be a positive integer.");
        }

        if(!jsonNode.isMember("coordinates"))
        {
            std::stringstream message;
            message << "The node " << jsonNode["id"].asUInt() << " must contains a 'coordinates' field.";
            throw std::runtime_error(message.str());
        }

        // Check 'coordinates'.
        const Json::Value& jsonNodeCoordinates = jsonNode["coordinates"];
        if(!jsonNodeCoordinates.isArray())
        {
            std::stringstream message;
            message << "The 'coordinates' of node " << jsonNode["id"].asUInt() << " must be an array.";
            throw std::runtime_error(message.str());
        }

        unsigned int coordinateCount = jsonNodeCoordinates.size();
        if(coordinateCount == 0)
        {
            std::stringstream message;
            message << "The 'coordinates' array of node "<< jsonNode["id"].asUInt() << " must be be empty.";
            throw std::runtime_error(message.str());
        }

        for(unsigned int nodeCoordinatesIndex = 0; nodeCoordinatesIndex < coordinateCount; nodeCoordinatesIndex++)
        {
            const Json::Value& jsonCoordinate = jsonNodeCoordinates[nodeCoordinatesIndex];
            if(!jsonCoordinate.isDouble())
            {
                std::stringstream message;
                message << "The 'coordinates' array of node "<< jsonNode["id"].asUInt() << " must contains 'double' values.";
                throw std::runtime_error(message.str());
            }
        }
    }
}

void JsonInputFile::ValidateElementsSchema(const Json::Value& jsonElements)
{
    if(!jsonElements.isArray())
    {
        throw std::runtime_error("The json 'elements' object must be an array.");
    }

    unsigned int elementCount = jsonElements.size();
    if(elementCount == 0)
    {
        throw std::runtime_error("The json 'elements' array must not be empty.");
    }

    // Check elements structure.
    for ( unsigned int elementIndex = 0; elementIndex < elementCount; elementIndex++ )
    {
        const Json::Value& jsonElement = jsonElements[elementIndex];

        // Check 'id'.
        if(!jsonElement.isMember("id"))
        {
            throw std::runtime_error("The element must contains an 'id' field.");
        }

        if(!jsonElement["id"].isUInt())
        {
            throw std::runtime_error("An element id must be a positive integer.");
        }

        // Check 'node_ids'.
        if(!jsonElement.isMember("node_ids"))
        {
            std::stringstream message;
            message << "The element " << jsonElement["id"].asUInt() << " must contains a 'node_ids' field.";
            throw std::runtime_error(message.str());
        }

        const Json::Value& jsonNodeIds = jsonElement["node_ids"];
        if(!jsonNodeIds.isArray())
        {
            std::stringstream message;
            message << "The 'node_ids' of element " << jsonElement["id"].asUInt() << " must be an array.";
            throw std::runtime_error(message.str());
        }

        unsigned int nodeCount = jsonNodeIds.size();
        if(nodeCount == 0)
        {
            std::stringstream message;
            message << "The 'node_ids' array of element "<< jsonElement["id"].asUInt() << " must be be empty.";
            throw std::runtime_error(message.str());
        }

        for(unsigned int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
        {
            const Json::Value& jsonNodeId = jsonNodeIds[nodeIndex];
            if(!jsonNodeId.isUInt())
            {
                std::stringstream message;
                message << "The 'node_ids' array of element "<< jsonElement["id"].asUInt() << " must contains positive integer values.";
                throw std::runtime_error(message.str());
            }
        }

        // Check 'material'.
        if(!jsonElement.isMember("material"))
        {
            std::stringstream message;
            message << "The element " << jsonElement["id"].asUInt() << " must contains a 'material' field.";
            throw std::runtime_error(message.str());
        }

        if(!jsonElement["material"].isString())
        {
            std::stringstream message;
            message << "The 'material' of element " << jsonElement["id"].asUInt() << " must be a string.";
            throw std::runtime_error(message.str());
        }
    }

}

void JsonInputFile::ValidateMaterialsSchema(const Json::Value& jsonMaterials)
{
    if(!jsonMaterials.isArray())
    {
        throw std::runtime_error("The json 'materials' object must be an array.");
    }

    unsigned int materialCount = jsonMaterials.size();
    if(materialCount == 0)
    {
        throw std::runtime_error("The json 'materials' array must not be empty.");
    }

    // Check materials structure.
    for ( unsigned int materialIndex = 0; materialIndex < materialCount; materialIndex++ )
    {
        const Json::Value& jsonMaterial = jsonMaterials[materialIndex];

        // Check 'id'.
        if(!jsonMaterial.isMember("id"))
        {
            throw std::runtime_error("The material must contains an 'id' field.");
        }

        if(!jsonMaterial["id"].isString())
        {
            throw std::runtime_error("The 'id' of material must be a string.");
        }

        // Check 'young_modulus'.
        if(!jsonMaterial.isMember("young_modulus"))
        {
            std::stringstream message;
            message << "The material " << jsonMaterial["id"].asUInt() << " must contains a 'young_modulus' field.";
            throw std::runtime_error(message.str());
        }

        if(!jsonMaterial["young_modulus"].isDouble())
        {
            std::stringstream message;
            message << "The 'young_modulus' of material " << jsonMaterial["id"].asUInt() << " must be a double value.";
            throw std::runtime_error(message.str());
        }

        // Check 'poisson_ratio'.
        if(!jsonMaterial.isMember("poisson_ratio"))
        {
            std::stringstream message;
            message << "The material " << jsonMaterial["id"].asUInt() << " must contains a 'poisson_ratio' field.";
            throw std::runtime_error(message.str());
        }

        if(!jsonMaterial["poisson_ratio"].isDouble())
        {
            std::stringstream message;
            message << "The 'poisson_ratio' of material " << jsonMaterial["id"].asUInt() << " must be a double value.";
            throw std::runtime_error(message.str());
        }

        // Check 'hypothesis'.
        if(!jsonMaterial.isMember("hypothesis"))
        {
            std::stringstream message;
            message << "The material " << jsonMaterial["id"].asUInt() << " must contains a 'hypothesis' field.";
            throw std::runtime_error(message.str());
        }

        if(!jsonMaterial["hypothesis"].isString())
        {
            throw std::runtime_error("The 'hypothesis' of material must be a string.");
        }
    }
}

void JsonInputFile::ValidateBoundaryConditionsSchema(const Json::Value& jsonBoundaryConditions)
{
    if(!jsonBoundaryConditions.isArray())
    {
        throw std::runtime_error("The json 'boundary_conditions' object must be an array.");
    }

    unsigned int boundaryConditionCount = jsonBoundaryConditions.size();
    if(boundaryConditionCount == 0)
    {
        throw std::runtime_error("The json 'boundary_conditions' array must not be empty.");
    }

    // Check boundary conditions structure.
    for ( unsigned int boundaryConditionIndex = 0; boundaryConditionIndex < boundaryConditionCount; boundaryConditionIndex++ )
    {
        const Json::Value& jsonBoundaryCondition = jsonBoundaryConditions[boundaryConditionIndex];

        // Check 'type'.
        if(!jsonBoundaryCondition.isMember("type"))
        {
            throw std::runtime_error("The boundary condition must contains a 'type' field.");
        }

        if(!jsonBoundaryCondition["type"].isString())
        {
            throw std::runtime_error("The boundary condition 'type' must be a string.");
        }

        // Check 'node_id'.
        if(!jsonBoundaryCondition.isMember("node_id"))
        {
            throw std::runtime_error("The boundary condition must contains a 'node_id' field.");
        }

        if(!jsonBoundaryCondition["node_id"].isUInt())
        {
            throw std::runtime_error("The boundary condition 'node_id' must be a positive integer.");
        }

        // Check 'degree_of_freedom'.
        if(jsonBoundaryCondition.isMember("degree_of_freedom") && !jsonBoundaryCondition["degree_of_freedom"].isUInt())
        {
            throw std::runtime_error("The boundary condition 'degree_of_freedom' must be a positive integer.");
        }

        // Check 'value' (not mandatory).
        if(jsonBoundaryCondition.isMember("value"))
        {
            if(!jsonBoundaryCondition["value"].isDouble())
            {
                throw std::runtime_error("The boundary condition 'value' must be a double value.");
            }
        }
    }
}

void JsonInputFile::CreateInputObjects(const Json::Value& jsonRoot)
{
    // Nodes.
    const Json::Value& jsonNodes = jsonRoot["nodes"];
    if(!jsonNodes.isArray())
        throw std::runtime_error("The node list must be a json array.");
    for ( unsigned int nodeIndex = 0; nodeIndex < jsonNodes.size(); nodeIndex++ )
    {
        const Json::Value& jsonNode = jsonNodes[nodeIndex];
        this->inputNodes.push_back(this->GetInputNode(jsonNode));
    }

    // Materials.
    const Json::Value& jsonMaterials = jsonRoot["materials"];
    if(!jsonMaterials.isArray())
        throw std::runtime_error("The material list must be a json array.");
    for ( unsigned int materialIndex = 0; materialIndex < jsonMaterials.size(); materialIndex++ )
    {
        const Json::Value& jsonMaterial = jsonMaterials[materialIndex];
        this->inputMaterials.push_back(this->GetInputMaterial(jsonMaterial));
    }

    // Elements.
    const Json::Value& jsonElements = jsonRoot["elements"];
    if(!jsonElements.isArray())
        throw std::runtime_error("The element list must be a json array.");
    for ( unsigned int elementIndex = 0; elementIndex < jsonElements.size(); elementIndex++ )
    {
        const Json::Value& jsonElement = jsonElements[elementIndex];
        this->inputElements.push_back(this->GetInputElement(jsonElement));
    }

    // Boundary conditions.
    const Json::Value& jsonBoundaryConditions = jsonRoot["boundary_conditions"];
    if(!jsonBoundaryConditions.isArray())
        throw std::runtime_error("The boundary condition list must be a json array.");
    for ( unsigned int boundaryConditionIndex = 0; boundaryConditionIndex < jsonBoundaryConditions.size(); boundaryConditionIndex++ )
    {
        const Json::Value& jsonBoundaryCondition = jsonBoundaryConditions[boundaryConditionIndex];
        this->inputBoundaryConditions.push_back(this->GetInputBoundaryCondition(jsonBoundaryCondition));
    }
}

InputNode JsonInputFile::GetInputNode(const Json::Value& jsonNode)
{
    InputNode newNode;

    // id.
    newNode.id = jsonNode["id"].asUInt();

    // coordinates.
    const Json::Value& jsonNodeCoordinates = jsonNode["coordinates"];
    for(unsigned int nodeCoordinatesIndex = 0; nodeCoordinatesIndex < jsonNodeCoordinates.size(); nodeCoordinatesIndex++)
    {
        newNode.coordinates.push_back(jsonNodeCoordinates[nodeCoordinatesIndex].asDouble());
    }

    return newNode;
}

InputElement JsonInputFile::GetInputElement(const Json::Value& jsonElement)
{
    InputElement newElement;

    // id.
    newElement.id = jsonElement["id"].asUInt();

    // node_ids
    const Json::Value& jsonNodeIds = jsonElement["node_ids"];
    for(unsigned int nodeIdsIndex = 0; nodeIdsIndex < jsonNodeIds.size(); nodeIdsIndex++)
    {
        newElement.nodeIds.push_back(jsonNodeIds[nodeIdsIndex].asUInt());
    }

    // material
    newElement.materialId = jsonElement["material"].asString();

    return newElement;
}

InputMaterial JsonInputFile::GetInputMaterial(const Json::Value& jsonMaterial)
{
    InputMaterial newMaterial;

    // id
    newMaterial.id = jsonMaterial["id"].asString();

    // young_modulus
    newMaterial.youngModulus = jsonMaterial["young_modulus"].asDouble();

    // poisson_ratio
    newMaterial.poissonRatio = jsonMaterial["poisson_ratio"].asDouble();

    // hypothesis
    newMaterial.hypothesis = jsonMaterial["hypothesis"].asString();

    return newMaterial;
}

InputBoundaryCondition JsonInputFile::GetInputBoundaryCondition(const Json::Value& jsonBoundaryCondition)
{
    InputBoundaryCondition newBoundaryCondition;

    // type
    newBoundaryCondition.type = jsonBoundaryCondition["type"].asString();

    // node_id
    newBoundaryCondition.nodeId = jsonBoundaryCondition["node_id"].asUInt();

    // degree_of_freedom
    if(jsonBoundaryCondition.isMember("degree_of_freedom") == false)
    {
        newBoundaryCondition.allDegreesOfFreedomIndex = true;
        newBoundaryCondition.degreeOfFreedomIndex = 42; // invalid ;)
    }
    else
    {
        newBoundaryCondition.allDegreesOfFreedomIndex = false;
        newBoundaryCondition.degreeOfFreedomIndex = jsonBoundaryCondition["degree_of_freedom"].asUInt();
    }

    // value
    if(newBoundaryCondition.type == "zero_displacement")
    {
        newBoundaryCondition.value = 0.0;
    }
    else
    {
        newBoundaryCondition.value = jsonBoundaryCondition["value"].asDouble();
    }

    return newBoundaryCondition;
}


} //namespace PetitPas
