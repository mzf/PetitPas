/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INPUT_OBJECTS_H
#define INPUT_OBJECTS_H

#include <vector>
#include <string>

namespace PetitPas
{
    // Small structures to represents data after parsing the input file.

    struct InputNode
    {
        unsigned int id;
        std::vector<double> coordinates;
    };

    struct InputElement
    {
        unsigned int id;
        std::vector<unsigned int> nodeIds;
        std::string materialId;
    };

    struct InputMaterial
    {
        std::string id;
        double youngModulus;
        double poissonRatio;
        std::string hypothesis;
    };

    struct InputBoundaryCondition
    {
        std::string type;
        unsigned int nodeId;
        bool allDegreesOfFreedomIndex;
        unsigned int degreeOfFreedomIndex;
        double value;
    };

}//namespace PetitPas


#endif // INPUT_OBJECTS_H
