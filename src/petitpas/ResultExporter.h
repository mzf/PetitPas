/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RESULT_EXPORTER_H
#define RESULT_EXPORTER_H

#include <string>

namespace PetitPas
{

    /** \brief Interface that defines a result exporter.
     */
    class ResultExporter
    {
    public:

        /** \brief Virtual Destructor.
         */
        virtual ~ResultExporter() {}

        /** \brief Export the results to a specified file.
         *         This method must be overrided by all result exporters.
         *
         * \param filePath file path and file name of the future result file.
         *
         * \throw std::runtime_error if \a filePath can not be created or if something failed during export.
         *
         */
        virtual void ExportToFile(const std::string& filePath) = 0;

    };

}//namespace PetitPas


#endif // RESULT_EXPORTER_H
