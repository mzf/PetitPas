/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VTK_RESULT_EXPORTER_H
#define VTK_RESULT_EXPORTER_H

#include "ResultExporter.h"

#include <fstream>
#include <string>
#include <map>

namespace PetitPas
{
    class Solution;
    class DisplacementResult;
    class StrainResult;
    class StressResult;
    class Matrix;

    /** \brief Export result of an analysis to default json format.
     */
    class VTKResultExporter : public ResultExporter
    {
    public:
        /** \brief Constructor from an analysis solution.
         *         The exporter will create result from this solution before exporting them.
         *
         * \param solution The Solution object from an Analysis object.
         *
         */
        VTKResultExporter(const Solution& solution);

        /** \brief Export the results to a vtk file.
         *         Results will be created before export (displacement, stress and strain).
         *
         * \param vtkFilePath file path and file name of the future vtk file.
         *
         * \throw std::runtime_error if \a vtkFilePath can not be created or if something failed during export.
         *
         */
        void ExportToFile(const std::string& vtkFilePath);

    private:

        void WriteHeader(std::ofstream& fileStream);
        void WriteMesh(std::ofstream& fileStream);
        void WriteResults(std::ofstream& fileStream);
        void WriteDisplacementResult(std::ofstream& fileStream, const DisplacementResult& displacementResult);
        void WriteStrainAndStressResults(std::ofstream& fileStream, const StrainResult& strainResult, const StressResult& stressResult);
        void WriteTensorFromVerticalMatrix(std::ostream& fileStream, const Matrix& verticalTensor);
        unsigned int GetVTKCellType(unsigned int nodeCount, unsigned int dimension) const;

        const Solution& solution;
        std::map<unsigned int, unsigned int> nodeIndexFromId;
        std::map<unsigned int, unsigned int> nodeIdFromIndex;
        std::map<unsigned int, unsigned int> elementIdFromIndex;

    };

}//namespace PetitPas


#endif // JSON_RESULT_EXPORTER_H
