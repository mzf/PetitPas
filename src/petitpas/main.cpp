/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "InputData.h"
#include "ResultExporter.h"
#include "../core/ArgumentParser.h"
#include "../core/Analysis.h"
#include "../core/Solution.h"
#include "../core/Logger.h"

#include <iostream>

int main(int argumentCount, char* argumentVector[])
{
    PetitPas::Logger::LogMessage("=========================");
    PetitPas::Logger::LogMessage("==== PetitPas Solver ====");
    PetitPas::Logger::LogMessage("=========================");

    try
    {
        // Parse command line arguments.
        PetitPas::ArgumentParser argumentParser;
        argumentParser.AddArgument("-i", "--input_file", "Path and name of the input file.", PetitPas::ArgumentParser::RequiresValue);
        argumentParser.AddArgument("-o", "--output_file", "Path and name of the output file that will contains results.", PetitPas::ArgumentParser::RequiresValue);
        argumentParser.AddArgument("-of", "--output_format", "Type of result file. Allowed values are: 'json' and 'vtk'. If omitted, default is 'json'.", PetitPas::ArgumentParser::RequiresValue);

        PetitPas::ArgumentParserResult parserResult = argumentParser.ParseArguments(argumentCount, argumentVector);
        std::cout << parserResult.GetErrorMessage() << std::endl;
        if(parserResult.MustExit())
        {
            return 2;
        }

        // Create input data.
        PetitPas::Logger::LogTime();
        PetitPas::Logger::LogMessage("Parse input data...");

        PetitPas::InputData inputData(parserResult);
        const PetitPas::Analysis& analysis = inputData.GetAnalysis();

        PetitPas::Logger::LogMessage("Parse input data OK.");
        PetitPas::Logger::LogTime();

        // Solve.
        PetitPas::Logger::LogMessage("Solve...");

        PetitPas::Solution solution = analysis.GetSolution();

        PetitPas::Logger::LogMessage("Solve OK.");
        PetitPas::Logger::LogTime();

        // Export results.
        PetitPas::Logger::LogMessage("Export results...");

        PetitPas::ResultExporter* resultExporter = inputData.GetResultExporter(solution);
        resultExporter->ExportToFile(inputData.GetResultFilePath());

        PetitPas::Logger::LogMessage("Export results OK.");
        PetitPas::Logger::LogTime();

        delete resultExporter;
    }
    catch(std::exception& e)
    {
        PetitPas::Logger::LogMessage("Error:");
        PetitPas::Logger::LogMessage(e.what());
        return 1;
    }

    PetitPas::Logger::LogMessage("End of process.");
    PetitPas::Logger::LogTime();

    return 0;
}
