/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JSON_RESULT_EXPORTER_H
#define JSON_RESULT_EXPORTER_H

#include "ResultExporter.h"
#include "json/json.h"

#include <string>

namespace PetitPas
{
    class Solution;
    class DisplacementResult;
    class StrainResult;
    class StressResult;

    /** \brief Export result of an analysis to default json format.
     */
    class JsonResultExporter : public ResultExporter
    {
    public:
        /** \brief Constructor from an analysis solution.
         *         The exporter will create result from this solution before exporting them.
         *
         * \param solution The Solution object from an Analysis object.
         *
         */
        JsonResultExporter(const Solution& solution);

        /** \brief Export the results to a json file.
         *         Results will be created before export (displacement, stress and strain).
         *
         * \param jsonFilePath file path and file name of the future json file.
         *
         * \throw std::runtime_error if \a jsonFilePath can not be created or if something failed during export.
         *
         */
        void ExportToFile(const std::string& jsonFilePath);

    private:

        Json::Value GetJsonDisplacementResult(const DisplacementResult& displacementResult);
        Json::Value GetJsonStressResult(const StressResult& stressResult);
        Json::Value GetJsonStrainResult(const StrainResult& strainResult);

        const Solution& solution;

    };

}//namespace PetitPas


#endif // JSON_RESULT_EXPORTER_H
