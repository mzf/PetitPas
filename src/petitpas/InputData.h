/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef INPUT_DATA_H
#define INPUT_DATA_H

#include "InputObjects.h"
#include "JsonInputFile.h"

#include <vector>
#include <string>
#include <list>

namespace PetitPas
{
    class Mesh;
    class Analysis;
    class MaterialBehavior;
    class ResultExporter;
    class Solution;
    class ArgumentParserResult;

    /** \brief Represents the input data for the PetitPas solver.
     */
    class InputData
    {
    public:
        /** \brief Constructor from a command line input.
         *
         * \param argumentCount Number of argument of the command line.
         * \param argumentVector Array of char arguments.
         *
         */
        InputData(const ArgumentParserResult& argumentParserResult);

        /** \brief Destructor.
         *
         */
        ~InputData();

        /** \brief Get the analysis created from the input data.
         *
         * \return An Analysis object that reflects the parsed data.
         *         The object and all associated objects (Mesh...) are held by this class and will be destroyed by the destructor.
         *
         */
        const Analysis& GetAnalysis() const;

        /** \brief Get the file name and path specified for the output.
         *
         * \return A string object that contains the file name and path. If no output file, it returns an empty string.
         *
         */
        const std::string& GetResultFilePath() const;

        /** \brief Get A new result exporter according to the specified input data.
         *         The caller must delete the given ResultExporter instance after using it.
         *
         * \param solution A Solution object from an Analysis.
         * \return A new ResultExporter object. The caller must delete this instance after use.
         *
         * \throw std::runtime_error if no output file has been defined in the input parameters.
         */
        ResultExporter* GetResultExporter(const Solution& solution);


    private:

        enum ResultFileFormat
        {
            ResultFileFormat_Json,
            ResultFileFormat_VTK
        };

        void CreateAnalysis(const JsonInputFile& inputFile);
        void ClearData();

        Mesh* mesh;
        Analysis* analysis;
        std::map<std::string, MaterialBehavior*> materialBehaviors;
        std::string resultFilePath;
        ResultFileFormat resultFileFormat;

    };

}//namespace PetitPas


#endif // INPUT_DATA_H
