/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "VTKResultExporter.h"

#include "../core/Matrix.h"
#include "../core/Solution.h"
#include "../core/DisplacementResult.h"
#include "../core/StrainResult.h"
#include "../core/StressResult.h"
#include "../core/Element.h"
#include "../core/Mesh.h"

#include <stdexcept>
#include <sstream>

namespace PetitPas
{

VTKResultExporter::VTKResultExporter(const Solution& solution) :
    solution(solution)
{

}

void VTKResultExporter::ExportToFile(const std::string& vtkFilePath)
{
    std::ofstream fileStream(vtkFilePath.c_str());
    if(!fileStream.good())
    {
        std::stringstream message;
        message << "Can not open the given result file name: ";
        message << vtkFilePath;
        throw std::runtime_error(message.str());
    }

    this->WriteHeader(fileStream);
    this->WriteMesh(fileStream);
    this->WriteResults(fileStream);

    fileStream.close();
}

void VTKResultExporter::WriteHeader(std::ofstream& fileStream)
{
    fileStream << "# vtk DataFile Version 2.0" << std::endl;
    fileStream << "PetitPas Result File" << std::endl; // This line must not exceed 256 chars including the std::endl.
    fileStream << "ASCII" << std::endl;
}

void VTKResultExporter::WriteMesh(std::ofstream& fileStream)
{
    fileStream << "DATASET UNSTRUCTURED_GRID" << std::endl;
    const Mesh& mesh = this->solution.GetMesh();

    // Nodes are points in VTK.
    const std::vector<Node*>& nodes = mesh.GetNodes();
    fileStream << "POINTS " << nodes.size() << " double" << std::endl;

    // We must keep a track of the order of nodes when we write them,
    // because CELLS (elements) nodes are indexes in this list.
    unsigned int nodeIndex = 0;

    for(std::vector<Node*>::const_iterator nodeIterator = nodes.begin();
        nodeIterator != nodes.end();
        ++nodeIterator)
    {
        Node* currentNode = (*nodeIterator);
        std::vector<double> nodeCoordinates = currentNode->GetCoordinates();

        // VTK needs 3D coordinates:
        nodeCoordinates.resize(3, 0.0);

        bool isFirstCoordinate = true;
        for(std::vector<double>::const_iterator coordinateIterator = nodeCoordinates.begin();
            coordinateIterator != nodeCoordinates.end();
            ++coordinateIterator)
        {
            if(isFirstCoordinate == false)
            {
                fileStream << " ";
            }
            else
            {
                isFirstCoordinate = false;
            }
            fileStream << (*coordinateIterator);
        }
        fileStream << std::endl;

        // Keep track of index in the list to write element nodes later.
        this->nodeIndexFromId[currentNode->GetId()] = nodeIndex;
        this->nodeIdFromIndex[nodeIndex] = currentNode->GetId();
        nodeIndex++;
    }

    // Elements are cells in VTK.
    const std::vector<Element*>& elements = mesh.GetElements();

    // We must write the total number of data written in the CELL command.
    // So we store the command in a buffer and write the CELL keyword at the end.
    std::stringstream cellStream;
    std::stringstream cellTypeStream;

    unsigned int totalNumberOfCellValues = 0;
    unsigned int elementIndex = 0;
    for(std::vector<Element*>::const_iterator elementIterator = elements.begin();
        elementIterator != elements.end();
        ++elementIterator)
    {
        Element* currentElement = (*elementIterator);
        const std::vector<Node*>& nodes = currentElement->GetNodes();

        // Node count and cell type.
        unsigned int nodeCount = nodes.size();
        cellStream << nodeCount;
        totalNumberOfCellValues++;
        cellTypeStream << this->GetVTKCellType(nodeCount, currentElement->GetDimension()) << std::endl;

        // For each node, write the index.
        for(std::vector<Node*>::const_iterator nodeIterator = nodes.begin();
            nodeIterator != nodes.end();
            ++nodeIterator)
        {
            Node* currentNode = (*nodeIterator);
            std::map<unsigned int, unsigned int>::const_iterator indexIterator = this->nodeIndexFromId.find(currentNode->GetId());
            if(indexIterator == this->nodeIndexFromId.end())
            {
                throw std::runtime_error("Can not find the node index.");
            }
            cellStream << " " << indexIterator->second;
            totalNumberOfCellValues++;
        }

        // Keep track of the index of this element to write elemental results later.
        this->elementIdFromIndex[elementIndex] = currentElement->GetId();
        elementIndex++;

        cellStream << std::endl;
    }

    // Finally write the CELL and CELL_TYPES commands.
    unsigned int elementCount = elements.size();
    fileStream << "CELLS " << elementCount << " " << totalNumberOfCellValues << std::endl;
    fileStream << cellStream.str();
    fileStream << "CELL_TYPES " << elementCount << std::endl;
    fileStream << cellTypeStream.str();
}

void VTKResultExporter::WriteResults(std::ofstream& fileStream)
{
    DisplacementResult displacementResult(this->solution);
    this->WriteDisplacementResult(fileStream, displacementResult);

    StrainResult strainResult(this->solution);
    StressResult stressResult(strainResult);
    this->WriteStrainAndStressResults(fileStream, strainResult, stressResult);
}

void VTKResultExporter::WriteDisplacementResult(std::ofstream& fileStream, const DisplacementResult& displacementResult)
{
    unsigned int nodeCount = this->nodeIdFromIndex.size();
    fileStream << "POINT_DATA " << nodeCount << std::endl;
    fileStream << "VECTORS Displacement double" << std::endl;

    // We must write the result in the node index order, as defined in the "POINTS" section.
    for(unsigned int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
    {
        std::map<unsigned int, unsigned int>::const_iterator nodeIdIterator = this->nodeIdFromIndex.find(nodeIndex);
        if(nodeIdIterator == this->nodeIdFromIndex.end())
        {
            throw std::runtime_error("Unknown node index while writing the displacement result.");
        }
        unsigned int nodeId = nodeIdIterator->second;

        std::vector<double> values = displacementResult.GetNodalDisplacements(nodeId);
        // VTK needs 3D data.
        values.resize(3, 0.0);
        fileStream << values[0] << " " << values[1] << " " << values[2] << std::endl;
    }
}

void VTKResultExporter::WriteStrainAndStressResults(std::ofstream& fileStream, const StrainResult& strainResult, const StressResult& stressResult)
{
    // We store the stress and strain commands, and write them at the end of the method.
    std::stringstream strainStream;
    std::stringstream stressStream;

    const Mesh& mesh = this->solution.GetMesh();

    // Write the result in the element index order, as defined in the "CELLS" section.
    unsigned int elementCount = this->elementIdFromIndex.size();
    for(unsigned int elementIndex = 0; elementIndex < elementCount; elementIndex++)
    {
        std::map<unsigned int, unsigned int>::const_iterator elementIdIterator = this->elementIdFromIndex.find(elementIndex);
        if(elementIdIterator == this->elementIdFromIndex.end())
        {
            throw std::runtime_error("Unknown element index while writing the strain result.");
        }

        // Get element.
        unsigned int elementId = elementIdIterator->second;
        Element* element = mesh.GetElement(elementId);

        // Get element's nodes.
        const std::vector<Node*>& nodes = element->GetNodes();
        unsigned int firstNodeId = nodes[0]->GetId();

        // Get the element's strain.
        const Matrix& strainMatrix = strainResult.GetElementalStrainAtNode(elementId, firstNodeId);
        this->WriteTensorFromVerticalMatrix(strainStream, strainMatrix);

        // Get the element's stress.
        const Matrix& stressMatrix = stressResult.GetElementalStressAtNode(elementId, firstNodeId);
        this->WriteTensorFromVerticalMatrix(stressStream, stressMatrix);
    }

    // Finally write all elemental tensor fields.
    fileStream << "CELL_DATA " << elementCount << std::endl;
    fileStream << "TENSORS Strain double" << std::endl;
    fileStream << strainStream.str();
    fileStream << "TENSORS Stress double" << std::endl;
    fileStream << stressStream.str();
}

void VTKResultExporter::WriteTensorFromVerticalMatrix(std::ostream& fileStream, const Matrix& verticalTensor)
{
    if((verticalTensor.GetRowCount() != 3) && (verticalTensor.GetRowCount() != 6))
    {
        throw std::runtime_error("The tensor must have 3 or 6 rows to be exported to vtk.");
    }

    if(verticalTensor.GetColumnCount() != 1)
    {
        throw std::runtime_error("The tensor must be a vertical matrix.");
    }

    // Tensors are 3x3.
    double tensorXX, tensorYY, tensorZZ, tensorXY, tensorXZ, tensorYZ;

    if((verticalTensor.GetRowCount() == 3))
    {
        //2D Tensor => 3D Tensor
        tensorXX = verticalTensor.GetValueAt(0,0);
        tensorYY = verticalTensor.GetValueAt(1,0);
        tensorZZ = 0.0;
        tensorXY = verticalTensor.GetValueAt(2,0);
        tensorXZ = 0.0;
        tensorYZ = 0.0;
    }
    else
    {
        //3D Tensor
        tensorXX = verticalTensor.GetValueAt(0,0);
        tensorYY = verticalTensor.GetValueAt(1,0);
        tensorZZ = verticalTensor.GetValueAt(2,0);
        tensorXY = verticalTensor.GetValueAt(3,0);
        tensorXZ = verticalTensor.GetValueAt(4,0);
        tensorYZ = verticalTensor.GetValueAt(5,0);
    }

    fileStream << tensorXX << " " << tensorXY << " " << tensorXZ << std::endl;
    fileStream << tensorXY << " " << tensorYY << " " << tensorYZ << std::endl;
    fileStream << tensorXZ << " " << tensorYZ << " " << tensorZZ << std::endl;
}

unsigned int VTKResultExporter::GetVTKCellType(unsigned int nodeCount, unsigned int dimension) const
{
    if(dimension == 2 && nodeCount == 3)
    {
        return 5; //VTK Triangle.
    }

    if(dimension == 3 && nodeCount == 4)
    {
        return 10; //VTK Tetrahedron.
    }

    std::stringstream message;
    message << "Unsupported cell type to export ";
    message << " (nodeCount=" << nodeCount;
    message << " dimension=" << dimension;
    message << ").";
    throw std::runtime_error(message.str());
}

} //namespace PetitPas
