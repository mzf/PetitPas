/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "JsonResultExporter.h"

#include "../core/Matrix.h"
#include "../core/Solution.h"
#include "../core/DisplacementResult.h"
#include "../core/StrainResult.h"
#include "../core/StressResult.h"
#include "../core/Element.h"

#include <stdexcept>
#include <fstream>
#include <sstream>

namespace PetitPas
{

JsonResultExporter::JsonResultExporter(const Solution& solution) :
    solution(solution)
{

}

void JsonResultExporter::ExportToFile(const std::string& jsonFilePath)
{
    std::ofstream jsonFileStream(jsonFilePath.c_str());
    if(!jsonFileStream.good())
    {
        std::stringstream message;
        message << "Can not open the given result file name: ";
        message << jsonFilePath;
        throw std::runtime_error(message.str());
    }

    // Our json array to store all the results.
    Json::Value jsonResults(Json::arrayValue);

    // Create displacement result and serialize it.
    DisplacementResult displacementResult(this->solution);
    Json::Value jsonDisplacementResult = this->GetJsonDisplacementResult(displacementResult);
    jsonResults.append(jsonDisplacementResult);

    // Create strain result and serialize it.
    StrainResult strainResult(this->solution);
    Json::Value jsonStrainResult = this->GetJsonStrainResult(strainResult);
    jsonResults.append(jsonStrainResult);

    // Create stress result and serialize it.
    StressResult stressResult(strainResult);
    Json::Value jsonStressResult = this->GetJsonStressResult(stressResult);
    jsonResults.append(jsonStressResult);

    // Finally create the global json object and write it to the stream.
    Json::Value jsonRoot;
    jsonRoot["results"] = jsonResults;
    jsonFileStream << jsonRoot;
    if(!jsonFileStream.good())
    {
        throw std::runtime_error("Something went wrong while writing json data to the file.");
    }

    jsonFileStream.close();
}

Json::Value JsonResultExporter::GetJsonDisplacementResult(const DisplacementResult& displacementResult)
{
    // Example of json output:
    // {
    //   "type" : "displacement",
    //   "values" : [
    //                {"node_id" : 2, "values" : [10.0, 20.0, 30.0]},
    //                {"node_id" : 3, "values" : [10.0, 20.0, 30.0]},
    //                {"node_id" : 4, "values" : [10.0, 20.0, 30.0]}
    //              ]
    // }

    Json::Value jsonDisplacementResult;
    jsonDisplacementResult["type"] = "displacement";
    Json::Value jsonDisplacementValues(Json::arrayValue);

    // Create json object for each nodal value.
    const std::set<unsigned int>& nodeIds = displacementResult.GetNodeIds();
    for(std::set<unsigned int>::const_iterator nodeIdIterator = nodeIds.begin();
        nodeIdIterator != nodeIds.end();
        ++nodeIdIterator)
    {
        unsigned int nodeId = *nodeIdIterator;
        std::vector<double> values = displacementResult.GetNodalDisplacements(nodeId);

        Json::Value jsonNodalValue;
        jsonNodalValue["node_id"] = nodeId;
        Json::Value nodalArrayValues(Json::arrayValue);
        for(std::vector<double>::const_iterator valueIterator = values.begin();
            valueIterator != values.end();
            ++valueIterator)
        {
            nodalArrayValues.append(*valueIterator);
        }
        jsonNodalValue["values"] = nodalArrayValues;

        jsonDisplacementValues.append(jsonNodalValue);
    }

    jsonDisplacementResult["values"] = jsonDisplacementValues;

    return jsonDisplacementResult;
}

Json::Value JsonResultExporter::GetJsonStrainResult(const StrainResult& strainResult)
{
    // Example of json output:
    // {
    //   "type" : "strain",
    //   "values" : [
    //                {"element_id": 1, "node_id" : 2, "values" : [10.0, 20.0, 30.0]},
    //                {"element_id": 1, "node_id" : 3, "values" : [10.0, 20.0, 30.0]},
    //                {"element_id": 1, "node_id" : 4, "values" : [10.0, 20.0, 30.0]}
    //              ]
    // }

    Json::Value jsonStrainResult;

    jsonStrainResult["type"] = "strain";
    Json::Value jsonStrainValues(Json::arrayValue);

    // Create json object for each element/node value.
    const std::vector<Element*>& elements = strainResult.GetElements();
    for(std::vector<Element*>::const_iterator elementIterator = elements.begin();
        elementIterator != elements.end();
        ++elementIterator)
    {
        Element* currentElement = (*elementIterator);
        const std::vector<Node*>& elementNodes = currentElement->GetNodes();
        for(std::vector<Node*>::const_iterator nodeIterator = elementNodes.begin();
            nodeIterator != elementNodes.end();
            ++nodeIterator)
        {
            Node* currentNode = (*nodeIterator);

            // Get the strain matrix at this element's node.
            const Matrix& strainMatrix = strainResult.GetElementalStrainAtNode(currentElement->GetId(), currentNode->GetId());

            Json::Value jsonElementNodeValue;
            jsonElementNodeValue["element_id"] = currentElement->GetId();
            jsonElementNodeValue["node_id"] = currentNode->GetId();

            // Copy the matrix in the json array.
            Json::Value elementNodeArrayValues(Json::arrayValue);
            for(unsigned int rowIndex = 0; rowIndex < strainMatrix.GetRowCount(); rowIndex++)
            {
                elementNodeArrayValues.append(strainMatrix.GetValueAt(rowIndex, 0));
            }
            jsonElementNodeValue["values"] = elementNodeArrayValues;

            jsonStrainValues.append(jsonElementNodeValue);
        }
    }

    jsonStrainResult["values"] = jsonStrainValues;

    return jsonStrainResult;
}

Json::Value JsonResultExporter::GetJsonStressResult(const StressResult& stressResult)
{
    // Example of json output:
    // {
    //   "type" : "stress",
    //   "values" : [
    //                {"element_id": 1, "node_id" : 2, "values" : [10.0, 20.0, 30.0]},
    //                {"element_id": 1, "node_id" : 3, "values" : [10.0, 20.0, 30.0]},
    //                {"element_id": 1, "node_id" : 4, "values" : [10.0, 20.0, 30.0]}
    //              ]
    // }

    Json::Value jsonStressResult;

    jsonStressResult["type"] = "stress";
    Json::Value jsonStressValues(Json::arrayValue);

    // Create json object for each element/node value.
    const std::vector<Element*>& elements = stressResult.GetElements();
    for(std::vector<Element*>::const_iterator elementIterator = elements.begin();
        elementIterator != elements.end();
        ++elementIterator)
    {
        Element* currentElement = (*elementIterator);
        const std::vector<Node*>& elementNodes = currentElement->GetNodes();
        for(std::vector<Node*>::const_iterator nodeIterator = elementNodes.begin();
            nodeIterator != elementNodes.end();
            ++nodeIterator)
        {
            Node* currentNode = (*nodeIterator);

            // Get the stress matrix at this element's node.
            const Matrix& stressMatrix = stressResult.GetElementalStressAtNode(currentElement->GetId(), currentNode->GetId());

            Json::Value jsonElementNodeValue;
            jsonElementNodeValue["element_id"] = currentElement->GetId();
            jsonElementNodeValue["node_id"] = currentNode->GetId();

            // Copy the matrix in the json array.
            Json::Value elementNodeArrayValues(Json::arrayValue);
            for(unsigned int rowIndex = 0; rowIndex < stressMatrix.GetRowCount(); rowIndex++)
            {
                elementNodeArrayValues.append(stressMatrix.GetValueAt(rowIndex, 0));
            }
            jsonElementNodeValue["values"] = elementNodeArrayValues;

            jsonStressValues.append(jsonElementNodeValue);
        }
    }

    jsonStressResult["values"] = jsonStressValues;

    return jsonStressResult;
}


} //namespace PetitPas
