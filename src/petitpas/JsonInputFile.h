/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef JSON_INPUT_FILE_H
#define JSON_INPUT_FILE_H

#include "json/json.h"
#include "InputObjects.h"

#include <string>
#include <list>

namespace PetitPas
{

    /** \brief This class handles the json input file, validate its content
     *         and extracts useful informations (nodes, elements...).
     */
    class JsonInputFile
    {
    public:

        /** \brief Constructor with a json file path.
         *
         * \param inputFilePath The name and path of the json file.
         *
         * \throw std::runtime_error if \a inputFilePath could not be opened or
         *                           if the json structure is not a valid input file.
         */
        JsonInputFile(const std::string& inputFilePath);

        /** \brief Get the nodes of this json file.
         *
         * \return List of InputNode objects.
         *
         */
        const std::list<InputNode>& GetInputNodes() const;

        /** \brief Get the elements of this json file.
         *
         * \return List of InputElement objects.
         *
         */
        const std::list<InputElement>& GetInputElements() const;

        /** \brief Get the materials of this json file.
         *
         * \return List of InputMaterial objects.
         *
         */
        const std::list<InputMaterial>& GetInputMaterials() const;

        /** \brief Get the boundary conditions of this json file.
         *
         * \return List of InputBoundaryCondition objects.
         *
         */
        const std::list<InputBoundaryCondition>& GetInputBoundaryConditions() const;

    private:

        // Json schema validation
        void ValidateInputSchema(const Json::Value& jsonRoot);
        void ValidateNodesSchema(const Json::Value& jsonNodes);
        void ValidateElementsSchema(const Json::Value& jsonElements);
        void ValidateMaterialsSchema(const Json::Value& jsonMaterials);
        void ValidateBoundaryConditionsSchema(const Json::Value& jsonBoundaryConditions);

        // Input objects creation.
        void CreateInputObjects(const Json::Value& jsonRoot);
        InputNode               GetInputNode(const Json::Value& jsonNode);
        InputElement            GetInputElement(const Json::Value& jsonElement);
        InputMaterial           GetInputMaterial(const Json::Value& jsonMaterial);
        InputBoundaryCondition  GetInputBoundaryCondition(const Json::Value& jsonBoundaryCondition);

        // Lists to store data after parsing.
        std::list<InputNode> inputNodes;
        std::list<InputElement> inputElements;
        std::list<InputMaterial> inputMaterials;
        std::list<InputBoundaryCondition> inputBoundaryConditions;


    };

}//namespace PetitPas


#endif // JSON_INPUT_FILE_H
