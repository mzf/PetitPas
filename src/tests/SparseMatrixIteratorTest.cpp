/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/SparseMatrixIterator.h"
#include "../core/Matrix.h"
#include "../core/SparseMatrixContainer.h"
#include <Eigen/Sparse>

SUITE(SparseMatrixIterator)
{

TEST(IteratorTest)
{
    PetitPas::Matrix matrix(4, 4, PetitPas::Matrix::MatrixContainerType_Sparse);

    matrix.SetValueAt(1, 2, 1.0);
    matrix.SetValueAt(3, 2, 2.0);
    matrix.SetValueAt(1, 0, 3.0);

    PetitPas::SparseMatrixContainer* container = dynamic_cast<PetitPas::SparseMatrixContainer*>(matrix.GetContainer());
    CHECK_EQUAL(true, container != NULL);

    PetitPas::SparseMatrixIterator currentIterator = container->GetBeginIterator();

    CHECK_EQUAL(1, currentIterator->row());
    CHECK_EQUAL(0, currentIterator->col());
    CHECK_CLOSE(3.0, currentIterator->value(), 1e-6);

    ++currentIterator;
    CHECK_EQUAL(1, currentIterator->row());
    CHECK_EQUAL(2, currentIterator->col());
    CHECK_CLOSE(1.0, currentIterator->value(), 1e-6);

    ++currentIterator;
    CHECK_EQUAL(3, currentIterator->row());
    CHECK_EQUAL(2, currentIterator->col());
    CHECK_CLOSE(2.0, currentIterator->value(), 1e-6);

    ++currentIterator;
    PetitPas::SparseMatrixIterator endIt = container->GetEndIterator();
    CHECK_EQUAL(false, currentIterator != endIt);
}

TEST(EigenSparseFromIterator)
{
    PetitPas::Matrix matrix(4, 4, PetitPas::Matrix::MatrixContainerType_Sparse);

    matrix.SetValueAt(1, 2, 1.0);
    matrix.SetValueAt(3, 2, 2.0);
    matrix.SetValueAt(1, 0, 3.0);

    PetitPas::SparseMatrixContainer* container = dynamic_cast<PetitPas::SparseMatrixContainer*>(matrix.GetContainer());
    CHECK_EQUAL(true, container != NULL);

    Eigen::SparseMatrix<double> eigenMatrix;
    eigenMatrix.resize(4,4);
    eigenMatrix.setFromTriplets(container->GetBeginIterator(), container->GetEndIterator());

    CHECK_CLOSE(0.0, eigenMatrix.coeff(0, 0), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(0, 1), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(0, 2), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(0, 3), 1e-6);
    CHECK_CLOSE(3.0, eigenMatrix.coeff(1, 0), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(1, 1), 1e-6);
    CHECK_CLOSE(1.0, eigenMatrix.coeff(1, 2), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(1, 3), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(2, 0), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(2, 1), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(2, 2), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(2, 3), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(3, 0), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(3, 1), 1e-6);
    CHECK_CLOSE(2.0, eigenMatrix.coeff(3, 2), 1e-6);
    CHECK_CLOSE(0.0, eigenMatrix.coeff(3, 3), 1e-6);
}


} //SUITE Point
