/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/ReferenceElement.h"
#include "../core/ReferenceElementBuilder.h"

#include <stdexcept>

SUITE(ReferenceElementBuilder)
{

TEST(ReferenceElementBuilderSingleton)
{
    PetitPas::ReferenceElementBuilder& referenceElementBuilder1 = PetitPas::ReferenceElementBuilder::GetInstance();
    PetitPas::ReferenceElementBuilder& referenceElementBuilder2 = PetitPas::ReferenceElementBuilder::GetInstance();

    CHECK_EQUAL((&referenceElementBuilder1), (&referenceElementBuilder2));

    const PetitPas::ReferenceElement* referenceElement1 = referenceElementBuilder1.GetReferenceElement(3, 2);
    const PetitPas::ReferenceElement* referenceElement2 = referenceElementBuilder2.GetReferenceElement(3, 2);

    CHECK_EQUAL(referenceElement1, referenceElement2);
}

TEST(ReferenceElemenetBuilderBadConfiguration)
{
    PetitPas::ReferenceElementBuilder& referenceElementBuilder = PetitPas::ReferenceElementBuilder::GetInstance();

    // Check bad node/dimension configuration.
    CHECK_THROW(referenceElementBuilder.GetReferenceElement(0, 0), std::runtime_error);
    CHECK_THROW(referenceElementBuilder.GetReferenceElement(1, 1), std::runtime_error);
    CHECK_THROW(referenceElementBuilder.GetReferenceElement(100, 35), std::runtime_error);
}

TEST(ReferenceElementBuilderTriangle3Nodes2D)
{
    PetitPas::ReferenceElementBuilder& referenceElementBuilder = PetitPas::ReferenceElementBuilder::GetInstance();
    const PetitPas::ReferenceElement* referenceElement = referenceElementBuilder.GetReferenceElement(3, 2);

    CHECK_EQUAL((unsigned int)3, referenceElement->GetIntegrationPoints().size());
    CHECK_EQUAL((unsigned int)3, referenceElement->GetIntegrationWeights().size());
}

} // SUITE ReferenceElementBuilder

