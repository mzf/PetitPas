/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/ResultNotFoundException.h"

SUITE(ResultNotFoundException)
{

TEST(ResultNotFoundExceptionConstructorNoParameter)
{
    // No parameter constructor, the message must not contain the "element" or "node" reference.
    PetitPas::ResultNotFoundException noArgumentException;
    std::string message(noArgumentException.what());
    CHECK_EQUAL(false, message.empty());
    CHECK_EQUAL(true, message.find("Result not found") != message.npos);
    CHECK_EQUAL(false, message.find("element") != message.npos);
    CHECK_EQUAL(false, message.find("node") != message.npos);
}

TEST(ResultNotFoundExceptionConstructorElementId)
{
    // Constructor with element id.
    PetitPas::ResultNotFoundException elementIdException(5, -1);
    std::string message(elementIdException.what());
    CHECK_EQUAL(false, message.empty());
    CHECK_EQUAL(true, message.find("Result not found") != message.npos);
    CHECK_EQUAL(true, message.find("element") != message.npos);
    CHECK_EQUAL(true, message.find("5") != message.npos);
    CHECK_EQUAL(false, message.find("node") != message.npos);
}

TEST(ResultNotFoundExceptionConstructorNodeId)
{
    // Constructor with node id.
    PetitPas::ResultNotFoundException nodeIdException(-1, 42);
    std::string message(nodeIdException.what());
    CHECK_EQUAL(false, message.empty());
    CHECK_EQUAL(true, message.find("Result not found") != message.npos);
    CHECK_EQUAL(false, message.find("element") != message.npos);
    CHECK_EQUAL(true, message.find("node") != message.npos);
    CHECK_EQUAL(true, message.find("42") != message.npos);
}

TEST(ResultNotFoundExceptionConstructorAllIds)
{
    // Constructor with all ids.
    PetitPas::ResultNotFoundException resultException(123, 456);
    std::string message(resultException.what());
    CHECK_EQUAL(false, message.empty());
    CHECK_EQUAL(true, message.find("Result not found") != message.npos);
    CHECK_EQUAL(true, message.find("element") != message.npos);
    CHECK_EQUAL(true, message.find("123") != message.npos);
    CHECK_EQUAL(true, message.find("node") != message.npos);
    CHECK_EQUAL(true, message.find("456") != message.npos);
}

} // SUITE ResultNotFoundException

