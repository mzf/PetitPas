/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/ArgumentParserResult.h"
#include <stdexcept>

SUITE(ArgumentParserResult)
{

TEST(ArgumentParserResultStatus)
{
    std::map<std::string, std::string> arguments;
    PetitPas::ArgumentParserResult resultContinue(arguments, PetitPas::ArgumentParserResult::ContinueStatus, "");
    CHECK_EQUAL(false, resultContinue.MustExit());

    PetitPas::ArgumentParserResult resultExit(arguments, PetitPas::ArgumentParserResult::MustExitStatus, "");
    CHECK_EQUAL(true, resultExit.MustExit());
}

TEST(ArgumentParserResultErrorMessage)
{
    std::string errorMessage("This is an error message.");
    std::map<std::string, std::string> arguments;
    PetitPas::ArgumentParserResult result(arguments, PetitPas::ArgumentParserResult::ContinueStatus, errorMessage);
    CHECK_EQUAL(true, result.GetErrorMessage() == errorMessage);
}

TEST(ArgumentParserResultContainsArgument)
{
    std::map<std::string, std::string> arguments;
    arguments["test"] = "123";
    arguments["test2"] = "";
    PetitPas::ArgumentParserResult result(arguments, PetitPas::ArgumentParserResult::ContinueStatus, "");

    CHECK_EQUAL(true, result.ContainsArgument("test"));
    CHECK_EQUAL(true, result.ContainsArgument("test2"));
    CHECK_EQUAL(false, result.ContainsArgument("test1"));
    CHECK_EQUAL(false, result.ContainsArgument(""));
}

TEST(ArgumentParserResultGetArgumentValue)
{
    std::map<std::string, std::string> arguments;
    arguments["test"] = "123";
    arguments["test2"] = "";
    PetitPas::ArgumentParserResult result(arguments, PetitPas::ArgumentParserResult::ContinueStatus, "");

    CHECK_EQUAL("123", result.GetArgumentValue("test"));
    CHECK_EQUAL("", result.GetArgumentValue("test2"));
    CHECK_THROW(result.GetArgumentValue("test1"), std::runtime_error);
}

} // SUITE ArgumentParserResult

