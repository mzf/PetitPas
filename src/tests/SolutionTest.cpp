/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/Solution.h"
#include "../core/Mesh.h"
#include <stdexcept>

// Implemented in MeshTest.cpp
void FillSmallMesh(PetitPas::Mesh& mesh);

SUITE(Solution)
{

TEST(SolutionConstructor)
{
    PetitPas::Mesh mesh;
    FillSmallMesh(mesh);

    // Check exception with invalid node id.
    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    PetitPas::Node node(point, 6);
    std::vector<PetitPas::Node*> badNodes;
    badNodes.push_back(&node);
    PetitPas::NodalValuesMatrix badNodalValues(badNodes);

    CHECK_THROW(PetitPas::Solution(mesh, badNodalValues), std::runtime_error);

    // Check no exceptions.
    std::vector<PetitPas::Node*> goodNodes = mesh.GetNodes();
    PetitPas::NodalValuesMatrix goodNodalValues(goodNodes);
    PetitPas::Solution goodSolution(mesh, goodNodalValues);
}

TEST(SolutionGetNodalValue)
{
    PetitPas::Mesh mesh;
    FillSmallMesh(mesh);
    std::vector<PetitPas::Node*> goodNodes = mesh.GetNodes();
    PetitPas::NodalValuesMatrix nodalValues(goodNodes);

    // Fill displacement values.
    double value = 1.0;
    for(unsigned int nodeId = 1; nodeId < 6; nodeId++)
    {
        for(unsigned int dofIndex = 0; dofIndex < 2; dofIndex++)
        {
            nodalValues.SetValue(nodeId, dofIndex, value);
            value += 1.0;
        }
    }

    // Create the solution.
    PetitPas::Solution solution(mesh, nodalValues);

    // Check the nodal values.
    value = 1.0;
    for(unsigned int nodeId = 1; nodeId < 6; nodeId++)
    {
        for(unsigned int dofIndex = 0; dofIndex < 2; dofIndex++)
        {
            CHECK_CLOSE(value, solution.GetNodalValue(nodeId, dofIndex), 1e-6);
            value += 1.0;
        }
    }

    //Check the nodal values without dof index
    value = 1.0;
    for(unsigned int nodeId = 1; nodeId < 6; nodeId++)
    {
        std::vector<double> values = solution.GetNodalValues(nodeId);
        CHECK_EQUAL((unsigned int)2, values.size());
        for(unsigned int dofIndex = 0; dofIndex < 2; dofIndex++)
        {
            CHECK_CLOSE(value, values[dofIndex], 1e-6);
            value += 1.0;
        }
    }
}

} //SUITE Solution
