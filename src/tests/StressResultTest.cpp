/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/StressResult.h"
#include "../core/StrainResult.h"
#include "../core/Solution.h"
#include "../core/Mesh.h"
#include "../core/ResultNotFoundException.h"

// Implemented in MeshTest.cpp
void FillSmallMesh(PetitPas::Mesh& mesh);

SUITE(StressResult)
{

TEST(StressResultConstructor)
{
    // Create a strain result from a solution object.
    PetitPas::Mesh mesh;
    FillSmallMesh(mesh);
    std::vector<PetitPas::Node*> goodNodes = mesh.GetNodes();
    PetitPas::NodalValuesMatrix nodalValues(goodNodes);

    // Fill displacement values.
    double value = 1.0;
    for(unsigned int nodeId = 1; nodeId < 6; nodeId++)
    {
        for(unsigned int dofIndex = 0; dofIndex < 2; dofIndex++)
        {
            nodalValues.SetValue(nodeId, dofIndex, value);
            value += 1.0;
        }
    }

    // Create the solution.
    PetitPas::Solution solution(mesh, nodalValues);

    // Create the Strain result.
    PetitPas::StrainResult strainResult(solution);

    // Create the Stress result!
    PetitPas::StressResult stressResult(strainResult);

    // Check bad element id and bad node id.
    CHECK_THROW(stressResult.GetElementalStressAtNode(3, 1), PetitPas::ResultNotFoundException);
    CHECK_THROW(stressResult.GetElementalStressAtNode(1, 4), PetitPas::ResultNotFoundException);

    // Check no error on valid input.
    const PetitPas::Matrix& stressMatrix = stressResult.GetElementalStressAtNode(1, 2);
    CHECK_EQUAL((unsigned int)3, stressMatrix.GetRowCount());
    CHECK_EQUAL((unsigned int)1, stressMatrix.GetColumnCount());

}

} // SUITE StressResult
