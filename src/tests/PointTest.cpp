/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/Point.h"
#include <stdexcept>

SUITE(Point)
{

TEST(PointConstructor)
{
    std::vector<double> emptyCoordinates;
    CHECK_THROW(PetitPas::Point badPoint(emptyCoordinates), std::runtime_error);

    std::vector<double> coordinates(10);
    for(int i = 0; i<10; i++)
    {
        coordinates[i] = i;
    }

    PetitPas::Point point(coordinates);

    std::vector<double> readCoords = point.GetCoordinates();

    CHECK_EQUAL((unsigned int)10, readCoords.size());

    for(int i = 0; i<10; i++)
    {
        CHECK_CLOSE((double)i, readCoords[i], 1e-6);
    }
}

TEST(PointDimension)
{
    std::vector<double> coords(5);

    PetitPas::Point point(coords);

    CHECK_EQUAL((unsigned int)5, point.GetDimension());
}

} //SUITE Point
