/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/BasisFunctionGenerator.h"
#include "../core/MaterialBehavior.h"

SUITE(BasisFunctionGenerator)
{

TEST(BasisFunctionGeneratorTriangle2D)
{
    //Create a triangle in 2D
    std::vector<double> coords1(2);
    coords1[0] = 1.0;
    coords1[1] = 2.0;
    PetitPas::Point point1(coords1);
    PetitPas::Node node1(point1, 1);

    std::vector<double> coords2(2);
    coords2[0] = 3.0;
    coords2[1] = 4.0;
    PetitPas::Point point2(coords2);
    PetitPas::Node node2(point2, 2);

    std::vector<double> coords3(2);
    coords3[0] = 4.0;
    coords3[1] = 0.0;
    PetitPas::Point point3(coords3);
    PetitPas::Node node3(point3, 3);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);

    //Create a dummy material
    PetitPas::MaterialBehavior material(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    PetitPas::Element element(nodes, 0, material);

    std::vector<PetitPas::PolynomialFunction> functions = PetitPas::BasisFunctionGenerator::GetPolynomialFunctions(element);

    CHECK_EQUAL((unsigned int)3, functions.size());

    //Check value of the functions
    CHECK_CLOSE(1.0, functions[0].GetValue(coords1), 1e-6);
    CHECK_CLOSE(0.0, functions[0].GetValue(coords2), 1e-6);
    CHECK_CLOSE(0.0, functions[0].GetValue(coords3), 1e-6);

    CHECK_CLOSE(0.0, functions[1].GetValue(coords1), 1e-6);
    CHECK_CLOSE(1.0, functions[1].GetValue(coords2), 1e-6);
    CHECK_CLOSE(0.0, functions[1].GetValue(coords3), 1e-6);

    CHECK_CLOSE(0.0, functions[2].GetValue(coords1), 1e-6);
    CHECK_CLOSE(0.0, functions[2].GetValue(coords2), 1e-6);
    CHECK_CLOSE(1.0, functions[2].GetValue(coords3), 1e-6);

    //Check that the result is the same with node objects instead of pointers.
    std::vector<PetitPas::Node> nodeObjects;
    nodeObjects.push_back(node1);
    nodeObjects.push_back(node2);
    nodeObjects.push_back(node3);

    std::vector<PetitPas::PolynomialFunction> functionsWithObjects = PetitPas::BasisFunctionGenerator::GetPolynomialFunctions(nodeObjects);
    CHECK_EQUAL((unsigned int)3, functionsWithObjects.size());
    CHECK_CLOSE(1.0, functionsWithObjects[0].GetValue(coords1), 1e-6);
    CHECK_CLOSE(0.0, functionsWithObjects[0].GetValue(coords2), 1e-6);
    CHECK_CLOSE(0.0, functionsWithObjects[0].GetValue(coords3), 1e-6);

    CHECK_CLOSE(0.0, functionsWithObjects[1].GetValue(coords1), 1e-6);
    CHECK_CLOSE(1.0, functionsWithObjects[1].GetValue(coords2), 1e-6);
    CHECK_CLOSE(0.0, functionsWithObjects[1].GetValue(coords3), 1e-6);

    CHECK_CLOSE(0.0, functionsWithObjects[2].GetValue(coords1), 1e-6);
    CHECK_CLOSE(0.0, functionsWithObjects[2].GetValue(coords2), 1e-6);
    CHECK_CLOSE(1.0, functionsWithObjects[2].GetValue(coords3), 1e-6);

}

TEST(BasisFunctionGeneratorTriangle2DReference)
{
    //Check the reference 2D Triangle
    /* ^
       |
      1+
       |\
       | \
       |  \
       |   \
      0+----+-->
       0    1
    */

    std::vector<double> coords(2);
    coords[0] = 0.0;
    coords[1] = 0.0;
    PetitPas::Point point1(coords);
    PetitPas::Node node1(point1, 1);

    coords[0] = 1.0;
    coords[1] = 0.0;
    PetitPas::Point point2(coords);
    PetitPas::Node node2(point2, 2);

    coords[0] = 0.0;
    coords[1] = 1.0;
    PetitPas::Point point3(coords);
    PetitPas::Node node3(point3, 3);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);

    //Create a dummy material
    PetitPas::MaterialBehavior material(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    PetitPas::Element element(nodes, 0, material);

    std::vector<PetitPas::PolynomialFunction> functions = PetitPas::BasisFunctionGenerator::GetPolynomialFunctions(element);

    CHECK_EQUAL((unsigned int)3, functions.size());

    //Check that value at (1/3, 1/3) is 1/3 for each function.
    double aThird = 1.0/3.0;
    coords[0] = aThird;
    coords[1] = aThird;

    CHECK_CLOSE(aThird, functions[0].GetValue(coords), 1e-6);
    CHECK_CLOSE(aThird, functions[1].GetValue(coords), 1e-6);
    CHECK_CLOSE(aThird, functions[2].GetValue(coords), 1e-6);

}

TEST(BasisFunctionGeneratorTetrahedron3DReference)
{
    //Check the reference 3D Tetrahedron with 4 nodes.

    std::vector<double> coords(3);
    coords[0] = 0.0;
    coords[1] = 0.0;
    coords[2] = 0.0;
    PetitPas::Point point1(coords);
    PetitPas::Node node1(point1, 1);

    coords[0] = 1.0;
    coords[1] = 0.0;
    coords[2] = 0.0;
    PetitPas::Point point2(coords);
    PetitPas::Node node2(point2, 2);

    coords[0] = 0.0;
    coords[1] = 1.0;
    coords[2] = 0.0;
    PetitPas::Point point3(coords);
    PetitPas::Node node3(point3, 3);

    coords[0] = 0.0;
    coords[1] = 0.0;
    coords[2] = 1.0;
    PetitPas::Point point4(coords);
    PetitPas::Node node4(point4, 4);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);
    nodes.push_back(&node4);

    //Create a dummy material
    PetitPas::MaterialBehavior material(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_3D);

    PetitPas::Element element(nodes, 0, material);

    std::vector<PetitPas::PolynomialFunction> functions = PetitPas::BasisFunctionGenerator::GetPolynomialFunctions(element);

    CHECK_EQUAL((unsigned int)4, functions.size());

    //Check that value at (1/3, 1/3) is 1/3 for each function.
    double aThird = 1.0/3.0;

    // XY Triangle
    coords[0] = aThird;
    coords[1] = aThird;
    coords[2] = 0.0;
    CHECK_CLOSE(aThird, functions[0].GetValue(coords), 1e-6);
    CHECK_CLOSE(aThird, functions[1].GetValue(coords), 1e-6);
    CHECK_CLOSE(aThird, functions[2].GetValue(coords), 1e-6);
    CHECK_CLOSE(0.0, functions[3].GetValue(coords), 1e-6);

    // XZ Triangle
    coords[0] = aThird;
    coords[1] = 0.0;
    coords[2] = aThird;
    CHECK_CLOSE(aThird, functions[0].GetValue(coords), 1e-6);
    CHECK_CLOSE(aThird, functions[1].GetValue(coords), 1e-6);
    CHECK_CLOSE(0.0, functions[2].GetValue(coords), 1e-6);
    CHECK_CLOSE(aThird, functions[3].GetValue(coords), 1e-6);

    // YZ Triangle
    coords[0] = 0.0;
    coords[1] = aThird;
    coords[2] = aThird;
    CHECK_CLOSE(aThird, functions[0].GetValue(coords), 1e-6);
    CHECK_CLOSE(0.0, functions[1].GetValue(coords), 1e-6);
    CHECK_CLOSE(aThird, functions[2].GetValue(coords), 1e-6);
    CHECK_CLOSE(aThird, functions[3].GetValue(coords), 1e-6);

}

} // SUITE BasisFunctionGenerator
