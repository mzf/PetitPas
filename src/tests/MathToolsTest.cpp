/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/MathTools.h"

SUITE(MathTools)
{

TEST(MathToolsIsDoubleClose)
{
    // Without tolerance.
    CHECK_EQUAL(true, PetitPas::MathTools::IsDoubleClose(0.0, 0.0));
    CHECK_EQUAL(true, PetitPas::MathTools::IsDoubleClose(1.0, 1.0));
    CHECK_EQUAL(true, PetitPas::MathTools::IsDoubleClose(-1.0, -1.0));
    CHECK_EQUAL(false, PetitPas::MathTools::IsDoubleClose(0.0, 1.0));
    CHECK_EQUAL(false, PetitPas::MathTools::IsDoubleClose(1.0, 0.0));
    CHECK_EQUAL(false, PetitPas::MathTools::IsDoubleClose(-1.0, 0.0));

    //With tolerance.
    CHECK_EQUAL(true, PetitPas::MathTools::IsDoubleClose(0.0, 0.001, 1e-3));
    CHECK_EQUAL(false, PetitPas::MathTools::IsDoubleClose(0.0, 0.001, 1e-4));
    CHECK_EQUAL(true, PetitPas::MathTools::IsDoubleClose(1.0, 1.09, 1e-1));
    CHECK_EQUAL(false, PetitPas::MathTools::IsDoubleClose(1.0, 1.09, 1e-2));
    CHECK_EQUAL(true, PetitPas::MathTools::IsDoubleClose(-1.0, -1.09, 1e-1));
    CHECK_EQUAL(false, PetitPas::MathTools::IsDoubleClose(-1.0, -1.09, 1e-2));

}

} // SUITE MathTools
