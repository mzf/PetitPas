/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/Matrix.h"
#include "../core/MatrixNotInvertibleException.h"

SUITE(Matrix)
{

TEST(MatrixSize)
{
    PetitPas::Matrix matrix(3,4);
    CHECK_EQUAL((unsigned int)3, matrix.GetRowCount());
    CHECK_EQUAL((unsigned int)4, matrix.GetColumnCount());

    //Check that 0-dimension throws an exception.
    CHECK_THROW(PetitPas::Matrix matrix(0,2), std::runtime_error);
    CHECK_THROW(PetitPas::Matrix matrix(2,0), std::runtime_error);
}

TEST(MatrixInitValues)
{
    PetitPas::Matrix matrix(3,4);

    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            CHECK_CLOSE(0.0, matrix.GetValueAt(i, j), 1e-6);
        }
    }
}

TEST(MatrixSetValues)
{
    PetitPas::Matrix matrix(3,4);

    matrix.SetValueAt(0, 0, 1.0);
    matrix.SetValueAt(0, 2, 2.0);
    matrix.SetValueAt(1, 0, 3.0);
    matrix.SetValueAt(2, 3, 4.0);
    matrix.SetValueAt(2, 1, 5.0);

    CHECK_CLOSE(1.0, matrix.GetValueAt(0, 0), 1e-6);
    CHECK_CLOSE(2.0, matrix.GetValueAt(0, 2), 1e-6);
    CHECK_CLOSE(3.0, matrix.GetValueAt(1, 0), 1e-6);
    CHECK_CLOSE(4.0, matrix.GetValueAt(2, 3), 1e-6);
    CHECK_CLOSE(5.0, matrix.GetValueAt(2, 1), 1e-6);
}

TEST(MatrixCheckBoundaries)
{
    PetitPas::Matrix matrix(3,4);

    CHECK_THROW(matrix.SetValueAt(3, 0, 1.0), std::runtime_error);
    CHECK_THROW(matrix.SetValueAt(0, 4, 2.0), std::runtime_error);
    CHECK_THROW(matrix.SetValueAt(10, 0, 3.0), std::runtime_error);
    CHECK_THROW(matrix.SetValueAt(2, 35, 4.0), std::runtime_error);
    CHECK_THROW(matrix.SetValueAt(22, 55, 5.0), std::runtime_error);

    CHECK_THROW(matrix.GetValueAt(3, 0), std::runtime_error);
    CHECK_THROW(matrix.GetValueAt(0, 4), std::runtime_error);
    CHECK_THROW(matrix.GetValueAt(10, 0), std::runtime_error);
    CHECK_THROW(matrix.GetValueAt(2, 35), std::runtime_error);
    CHECK_THROW(matrix.GetValueAt(22, 55), std::runtime_error);

}

TEST(MatrixAssignmentOperator)
{
    PetitPas::Matrix A(3,4);

    double value = 1.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            A.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    PetitPas::Matrix B(1,2);

    B = A;

    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            CHECK_CLOSE(A.GetValueAt(i,j), B.GetValueAt(i,j), 1e-6);
        }
    }

}

TEST(MatrixCopyConstructor)
{
    PetitPas::Matrix A(3,4);

    double value = 1.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            A.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    PetitPas::Matrix B(A);

    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            CHECK_CLOSE(A.GetValueAt(i,j), B.GetValueAt(i,j), 1e-6);
        }
    }

    // Check that the memory is not shared anymore.
    A.SetValueAt(0,0,100.0);
    double result = A.GetValueAt(0,0) - B.GetValueAt(0,0);
    CHECK_CLOSE(99.0, result, 1e-6);
}

TEST(MatrixAdditionOperator)
{
    PetitPas::Matrix A(3,4);

    double value = 1.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            A.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    PetitPas::Matrix B(3,4);

    value = 101.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            B.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    PetitPas::Matrix C = A + B;

    value = 102.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            CHECK_CLOSE(value, C.GetValueAt(i,j), 1e-6);
            value += 2.0;
        }
    }

    // Check bad dimensions.
    PetitPas::Matrix BadMatrix(2,2);
    CHECK_THROW(C = A + BadMatrix, std::runtime_error);

}

TEST(MatrixSubstractionOperator)
{
    PetitPas::Matrix A(3,4);

    double value = 1.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            A.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    PetitPas::Matrix B(3,4);

    value = 100.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            B.SetValueAt(i,j, value);
            value -= 1.0;
        }
    }

    PetitPas::Matrix C = A - B;

    value = -99.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            CHECK_CLOSE(value, C.GetValueAt(i,j), 1e-6);
            value += 2.0;
        }
    }

    // Check bad dimensions.
    PetitPas::Matrix BadMatrix(2,2);
    CHECK_THROW(C = A - BadMatrix, std::runtime_error);

}

TEST(MatrixMultiplicationOperator)
{
    PetitPas::Matrix A(3,4);

    double value = 1.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            A.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    PetitPas::Matrix B(4,2);

    value = 1.0;
    for(unsigned int i=0; i<4; i++)
    {
        for(unsigned int j=0; j<2; j++)
        {
            B.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    PetitPas::Matrix C = A * B;

    CHECK_EQUAL((unsigned int)2, C.GetColumnCount());
    CHECK_EQUAL((unsigned int)3, C.GetRowCount());

    CHECK_CLOSE(50.0, C.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(60.0, C.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(114.0, C.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(140.0, C.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(178.0, C.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(220.0, C.GetValueAt(2,1), 1e-6);

    //Check incompatible dimensions
    PetitPas::Matrix D(2,2);
    CHECK_THROW(A*D, std::runtime_error);
}

TEST(MatrixTranspose)
{
    PetitPas::Matrix A(3,4);

    double value = 1.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            A.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    PetitPas::Matrix B = A.GetTranspose();

    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            CHECK_CLOSE(A.GetValueAt(i,j), B.GetValueAt(j,i), 1e-6);
        }
    }
}

TEST(MatrixIsSquare)
{
    PetitPas::Matrix A(4,4);
    CHECK_EQUAL(true, A.IsSquare());

    PetitPas::Matrix B(3,4);
    CHECK_EQUAL(false, B.IsSquare());
}

TEST(MatrixDeterminant)
{
    //Check non square matrix error
    PetitPas::Matrix NonSquare(2,3);
    CHECK_THROW(NonSquare.GetDeterminant(), std::runtime_error);

    //1x1
    PetitPas::Matrix M11(1,1);
    M11.SetValueAt(0,0, 55.0);
    CHECK_CLOSE(55.0, M11.GetDeterminant(), 1e-6);

    //2x2
    PetitPas::Matrix M22(2,2);
    M22.SetValueAt(0,0, 1.0);
    M22.SetValueAt(0,1, 2.0);
    M22.SetValueAt(1,0, 3.0);
    M22.SetValueAt(1,1, 4.0);
    CHECK_CLOSE(-2.0, M22.GetDeterminant(), 1e-6);

    //3x3
    PetitPas::Matrix M33(3,3);
    M33.SetValueAt(0,0, 10.0);
    M33.SetValueAt(0,1, 2.0);
    M33.SetValueAt(0,2, 3.0);
    M33.SetValueAt(1,0, 4.0);
    M33.SetValueAt(1,1, 5.0);
    M33.SetValueAt(1,2, 6.0);
    M33.SetValueAt(2,0, 7.0);
    M33.SetValueAt(2,1, 8.0);
    M33.SetValueAt(2,2, 9.0);
    CHECK_CLOSE(-27.0, M33.GetDeterminant(), 1e-6);

    //4x4
    PetitPas::Matrix M44(4,4);
    M44.SetValueAt(0,0, 1.0);
    M44.SetValueAt(0,1, 2.0);
    M44.SetValueAt(0,2, 3.0);
    M44.SetValueAt(0,3, -1.0);
    M44.SetValueAt(1,0, 2.0);
    M44.SetValueAt(1,1, 3.0);
    M44.SetValueAt(1,2, 1.0);
    M44.SetValueAt(1,3, 2.0);
    M44.SetValueAt(2,0, 3.0);
    M44.SetValueAt(2,1, 1.0);
    M44.SetValueAt(2,2, 2.0);
    M44.SetValueAt(2,3, 0.0);
    M44.SetValueAt(3,0, 1.0);
    M44.SetValueAt(3,1, 2.0);
    M44.SetValueAt(3,2, -1.0);
    M44.SetValueAt(3,3, 1.0);
    CHECK_CLOSE(32.0, M44.GetDeterminant(), 1e-6);
}

TEST(MatrixCofactors)
{
    //Check non square matrix error
    PetitPas::Matrix NonSquare(2,3);
    CHECK_THROW(NonSquare.GetCofactorMatrix(), std::runtime_error);

    PetitPas::Matrix M33(3,3);
    M33.SetValueAt(0,0, -3.0);
    M33.SetValueAt(0,1, 5.0);
    M33.SetValueAt(0,2, 6.0);
    M33.SetValueAt(1,0, -1.0);
    M33.SetValueAt(1,1, 2.0);
    M33.SetValueAt(1,2, 2.0);
    M33.SetValueAt(2,0, 1.0);
    M33.SetValueAt(2,1, -1.0);
    M33.SetValueAt(2,2, -1.0);

    PetitPas::Matrix CofactorMatrix = M33.GetCofactorMatrix();
    CHECK_CLOSE(0.0, CofactorMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(1.0, CofactorMatrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(-1.0, CofactorMatrix.GetValueAt(0,2), 1e-6);
    CHECK_CLOSE(-1.0, CofactorMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(-3.0, CofactorMatrix.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(2.0, CofactorMatrix.GetValueAt(1,2), 1e-6);
    CHECK_CLOSE(-2.0, CofactorMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(-0.0, CofactorMatrix.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(-1.0, CofactorMatrix.GetValueAt(2,2), 1e-6);
}

TEST(MatrixMultiplyByAValue)
{
    PetitPas::Matrix A(3,4);

    double value = 1.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            A.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    A.MultiplyBy(10.0);

    value = 10.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<4; j++)
        {
            CHECK_CLOSE(value, A.GetValueAt(i,j), 1e-6);
            value += 10.0;
        }
    }
}

TEST(MatrixInverse)
{

    //Check non square matrix error
    PetitPas::Matrix NonSquare(2,3);
    CHECK_THROW(NonSquare.GetInverse(), std::runtime_error);

    //Check that a not invertible matrix
    PetitPas::Matrix NotInvertible(3,3);
    double value = 1.0;
    for(unsigned int i=0; i<3; i++)
    {
        for(unsigned int j=0; j<3; j++)
        {
            NotInvertible.SetValueAt(i,j, value);
            value += 1.0;
        }
    }
    CHECK_THROW(NotInvertible.GetInverse(), PetitPas::MatrixNotInvertibleException);


    //2x2
    PetitPas::Matrix M22(2,2);
    M22.SetValueAt(0,0, 2.0);
    M22.SetValueAt(0,1, 1.0);
    M22.SetValueAt(1,0, 3.0);
    M22.SetValueAt(1,1, 4.0);

    PetitPas::Matrix InverseMatrix22 = M22.GetInverse();

    CHECK_CLOSE(0.8, InverseMatrix22.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(-0.2, InverseMatrix22.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(-0.6, InverseMatrix22.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(0.4, InverseMatrix22.GetValueAt(1,1), 1e-6);

    //3x3
    PetitPas::Matrix M33(3,3);
    M33.SetValueAt(0,0, -3.0);
    M33.SetValueAt(0,1, 5.0);
    M33.SetValueAt(0,2, 6.0);
    M33.SetValueAt(1,0, -1.0);
    M33.SetValueAt(1,1, 2.0);
    M33.SetValueAt(1,2, 2.0);
    M33.SetValueAt(2,0, 1.0);
    M33.SetValueAt(2,1, -1.0);
    M33.SetValueAt(2,2, -1.0);

    PetitPas::Matrix InverseMatrix33 = M33.GetInverse();

    CHECK_CLOSE(0.0, InverseMatrix33.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(1.0, InverseMatrix33.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(2.0, InverseMatrix33.GetValueAt(0,2), 1e-6);
    CHECK_CLOSE(-1.0, InverseMatrix33.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(3.0, InverseMatrix33.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(0.0, InverseMatrix33.GetValueAt(1,2), 1e-6);
    CHECK_CLOSE(1.0, InverseMatrix33.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(-2.0, InverseMatrix33.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(1.0, InverseMatrix33.GetValueAt(2,2), 1e-6);
}

TEST(MatrixSwapRows)
{
    PetitPas::Matrix matrix(4,3);
    double value=1.0;
    for(unsigned int i=0; i<4; i++)
    {
        for(unsigned int j=0; j<3; j++)
        {
            matrix.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    CHECK_THROW(matrix.SwapRows(4,0), std::runtime_error);
    CHECK_THROW(matrix.SwapRows(0,4), std::runtime_error);

    matrix.SwapRows(1, 3);

    CHECK_CLOSE(1.0, matrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(2.0, matrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(3.0, matrix.GetValueAt(0,2), 1e-6);

    CHECK_CLOSE(10.0, matrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(11.0, matrix.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(12.0, matrix.GetValueAt(1,2), 1e-6);

    CHECK_CLOSE(7.0, matrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(8.0, matrix.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(9.0, matrix.GetValueAt(2,2), 1e-6);

    CHECK_CLOSE(4.0, matrix.GetValueAt(3,0), 1e-6);
    CHECK_CLOSE(5.0, matrix.GetValueAt(3,1), 1e-6);
    CHECK_CLOSE(6.0, matrix.GetValueAt(3,2), 1e-6);
}

TEST(MatrixSwapColumns)
{
    PetitPas::Matrix matrix(4,3);
    double value=1.0;
    for(unsigned int i=0; i<4; i++)
    {
        for(unsigned int j=0; j<3; j++)
        {
            matrix.SetValueAt(i,j, value);
            value += 1.0;
        }
    }

    CHECK_THROW(matrix.SwapColumns(3,0), std::runtime_error);
    CHECK_THROW(matrix.SwapColumns(0,3), std::runtime_error);

    matrix.SwapColumns(1, 2);

    CHECK_CLOSE(1.0, matrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(3.0, matrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(2.0, matrix.GetValueAt(0,2), 1e-6);

    CHECK_CLOSE(4.0, matrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(6.0, matrix.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(5.0, matrix.GetValueAt(1,2), 1e-6);

    CHECK_CLOSE(7.0, matrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(9.0, matrix.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(8.0, matrix.GetValueAt(2,2), 1e-6);

    CHECK_CLOSE(10.0, matrix.GetValueAt(3,0), 1e-6);
    CHECK_CLOSE(12.0, matrix.GetValueAt(3,1), 1e-6);
    CHECK_CLOSE(11.0, matrix.GetValueAt(3,2), 1e-6);
}

TEST(MatrixEqualOperator)
{
    PetitPas::Matrix referenceMatrix(3, 4);
    double value = -5.0;
    for(unsigned int rowIndex = 0; rowIndex < 3; rowIndex++)
    {
        for(unsigned int columnIndex = 0; columnIndex < 4; columnIndex++)
        {
            referenceMatrix.SetValueAt(rowIndex, columnIndex, value);
            value += 1.0;
        }
    }

    PetitPas::Matrix badRowCountMatrix(4,4);
    CHECK_EQUAL(false, badRowCountMatrix == referenceMatrix);

    PetitPas::Matrix badColumnCountMatrix(4,2);
    CHECK_EQUAL(false, badColumnCountMatrix == referenceMatrix);

    PetitPas::Matrix badDataMatrix(3,4);
    CHECK_EQUAL(false, badDataMatrix == referenceMatrix);

    PetitPas::Matrix otherReferenceMatrix(referenceMatrix);
    CHECK_EQUAL(true, otherReferenceMatrix == referenceMatrix);
    otherReferenceMatrix.SetValueAt(1,2, 0.003);
    CHECK_EQUAL(false, otherReferenceMatrix == referenceMatrix);

    CHECK_EQUAL(true, referenceMatrix == referenceMatrix);
}

TEST(MatrixAddValueAt)
{
    PetitPas::Matrix matrix(3, 4);

    matrix.AddValueAt(2,2,10.0);
    CHECK_CLOSE(10.0, matrix.GetValueAt(2,2), 1e-6);

    matrix.AddValueAt(2,2,10.0);
    CHECK_CLOSE(20.0, matrix.GetValueAt(2,2), 1e-6);

    matrix.AddValueAt(2,2,-20.0);
    CHECK_CLOSE(0.0, matrix.GetValueAt(2,2), 1e-6);

    CHECK_THROW(matrix.AddValueAt(3,2,5.0), std::runtime_error);
    CHECK_THROW(matrix.AddValueAt(2,4,5.0), std::runtime_error);
}

TEST(MatrixMultiplyValueAt)
{
    PetitPas::Matrix matrix(3, 4);

    matrix.SetValueAt(2,2, 10.0);
    matrix.MultiplyValueAt(2,2, 2.0);
    CHECK_CLOSE(20.0, matrix.GetValueAt(2,2), 1e-6);

    matrix.MultiplyValueAt(2,2, 0.5);
    CHECK_CLOSE(10.0, matrix.GetValueAt(2,2), 1e-6);

    matrix.MultiplyValueAt(2,2, -10.0);
    CHECK_CLOSE(-100.0, matrix.GetValueAt(2,2), 1e-6);

    matrix.MultiplyValueAt(2,2, 0.0);
    CHECK_CLOSE(0.0, matrix.GetValueAt(2,2), 1e-6);

    CHECK_THROW(matrix.MultiplyValueAt(3,2,5.0), std::runtime_error);
    CHECK_THROW(matrix.MultiplyValueAt(2,4,5.0), std::runtime_error);
}

} // SUITE Matrix
