/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/ArgumentParser.h"

SUITE(ArgumentParser)
{

TEST(ArgumentParserNoArgument)
{
    PetitPas::ArgumentParser parser;
    char* argumentVector[] = {(char*)"myProgram", (char*)"test", (char*)"123"};
    PetitPas::ArgumentParserResult result = parser.ParseArguments(3, argumentVector);
    CHECK_EQUAL(false, result.ContainsArgument("test"));
    CHECK_EQUAL(false, result.ContainsArgument("123"));
    CHECK_EQUAL("", result.GetErrorMessage());

    result = parser.ParseArguments(0, NULL);
    CHECK_EQUAL(false, result.MustExit());
}

TEST(ArgumentParserAddArgumentStandAlone)
{
    PetitPas::ArgumentParser parser;
    parser.AddArgument("test", "alt_test", "description", PetitPas::ArgumentParser::StandAlone);

    // normal name
    char* argumentVector[] = {(char*)"myProgram", (char*)"test", (char*)"123"};
    PetitPas::ArgumentParserResult result = parser.ParseArguments(3, argumentVector);
    CHECK_EQUAL(true, result.ContainsArgument("test"));
    CHECK_EQUAL(false, result.ContainsArgument("123"));
    CHECK_EQUAL(false, result.ContainsArgument("alt_test"));
    CHECK_EQUAL("", result.GetErrorMessage());

    // alternative name
    char* argumentVectorAlternative[] = {(char*)"myProgram", (char*)"test123", (char*)"alt_test"};
    result = parser.ParseArguments(3, argumentVectorAlternative);
    CHECK_EQUAL(true, result.ContainsArgument("test"));
    CHECK_EQUAL(false, result.ContainsArgument("123"));
    CHECK_EQUAL(false, result.ContainsArgument("alt_test"));
    CHECK_EQUAL("", result.GetErrorMessage());
}

TEST(ArgumentParserAddArgumentRequiresValue)
{
    PetitPas::ArgumentParser parser;
    parser.AddArgument("test", "alt_test", "description", PetitPas::ArgumentParser::RequiresValue);

    // normal name
    char* argumentVector[] = {(char*)"myProgram", (char*)"test", (char*)"123"};
    PetitPas::ArgumentParserResult result = parser.ParseArguments(3, argumentVector);
    CHECK_EQUAL(true, result.ContainsArgument("test"));
    CHECK_EQUAL(false, result.ContainsArgument("123"));
    CHECK_EQUAL(false, result.ContainsArgument("alt_test"));
    CHECK_EQUAL("", result.GetErrorMessage());
    CHECK_EQUAL("123", result.GetArgumentValue("test"));

    // alternative name
    char* argumentVectorAlternative[] = {(char*)"myProgram", (char*)"test123", (char*)"alt_test", (char*)"42"};
    result = parser.ParseArguments(4, argumentVectorAlternative);
    CHECK_EQUAL(true, result.ContainsArgument("test"));
    CHECK_EQUAL(false, result.ContainsArgument("123"));
    CHECK_EQUAL(false, result.ContainsArgument("alt_test"));
    CHECK_EQUAL("", result.GetErrorMessage());
    CHECK_EQUAL("42", result.GetArgumentValue("test"));

    // no value after the argument
    char* argumentVectorAlternativeBad[] = {(char*)"myProgram", (char*)"test123", (char*)"alt_test"};
    result = parser.ParseArguments(3, argumentVectorAlternativeBad);
    CHECK_EQUAL(false, result.ContainsArgument("test"));
    CHECK_EQUAL(false, result.ContainsArgument("123"));
    CHECK_EQUAL(false, result.ContainsArgument("alt_test"));
    CHECK_EQUAL(false, result.GetErrorMessage() == "");
}

void CheckHelpParserResult(const PetitPas::ArgumentParserResult& result)
{
    CHECK_EQUAL(true, result.MustExit());
    const std::string& helpMessage = result.GetErrorMessage();
    CHECK_EQUAL(false, helpMessage.empty());
    CHECK_EQUAL(true, helpMessage.find("usage:") != helpMessage.npos);
    CHECK_EQUAL(true, helpMessage.find("arguments:") != helpMessage.npos);
}

TEST(ArgumentParserHelpMessage)
{
    PetitPas::ArgumentParser parser;
    parser.AddArgument("test", "alt_test", "description", PetitPas::ArgumentParser::RequiresValue);

    // "help"
    {
        char* argumentVector[] = {(char*)"myProgram", (char*)"help"};
        PetitPas::ArgumentParserResult result = parser.ParseArguments(2, argumentVector);
        CheckHelpParserResult(result);
    }

    // "-h"
    {
        char* argumentVector[] = {(char*)"myProgram", (char*)"-h"};
        PetitPas::ArgumentParserResult result = parser.ParseArguments(2, argumentVector);
        CheckHelpParserResult(result);
    }

    // "--help"
    {
        char* argumentVector[] = {(char*)"myProgram", (char*)"--help"};
        PetitPas::ArgumentParserResult result = parser.ParseArguments(2, argumentVector);
        CheckHelpParserResult(result);
    }

    // "help" with other valid arguments
    {
        char* argumentVector[] = {(char*)"myProgram", (char*)"help", (char*)"test"};
        PetitPas::ArgumentParserResult result = parser.ParseArguments(3, argumentVector);
        CheckHelpParserResult(result);
    }

    // "help" with other invalid arguments
    {
        char* argumentVector[] = {(char*)"myProgram", (char*)"help", (char*)"dummy"};
        PetitPas::ArgumentParserResult result = parser.ParseArguments(3, argumentVector);
        CheckHelpParserResult(result);
    }

    // "help" not the first argument
    {
        char* argumentVector[] = {(char*)"myProgram", (char*)"dummy", (char*)"help"};
        PetitPas::ArgumentParserResult result = parser.ParseArguments(3, argumentVector);
        CheckHelpParserResult(result);
    }
}

} // SUITE ArgumentParser

