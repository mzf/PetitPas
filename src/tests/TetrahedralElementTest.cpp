/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/Element.h"
#include "../core/MaterialBehavior.h"
#include "../core/Matrix.h"
#include "../core/Mesh.h"
#include "../core/Analysis.h"
#include "../core/Solution.h"
#include "../core/StrainResult.h"
#include "../core/StressResult.h"
#include <stdexcept>

SUITE(TetrahedralElement)
{


TEST(TetrahedralElementConstructor)
{
    std::vector<double> coords(3);
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);
    PetitPas::Node node3(point, 3);
    PetitPas::Node node4(point, 4);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);
    nodes.push_back(&node4);

    //Create a dummy material
    PetitPas::MaterialBehavior material(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_3D);

    PetitPas::Element element(nodes, 0, material);

}

TEST(TetrahedralElementStiffnessMatrix)
{
    std::vector<double> coords(3);
    coords[0] = 0.0;
    coords[1] = 0.0;
    coords[2] = 0.0;
    PetitPas::Point point1(coords);
    PetitPas::Node node1(point1, 1);

    coords[0] = 1.0;
    coords[1] = 0.0;
    coords[2] = 0.0;
    PetitPas::Point point2(coords);
    PetitPas::Node node2(point2, 2);

    coords[0] = 0.0;
    coords[1] = 1.0;
    coords[2] = 0.0;
    PetitPas::Point point3(coords);
    PetitPas::Node node3(point3, 3);

    coords[0] = 0.0;
    coords[1] = 0.0;
    coords[2] = 1.0;
    PetitPas::Point point4(coords);
    PetitPas::Node node4(point4, 4);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);
    nodes.push_back(&node4);

    //Create a dummy material
    PetitPas::MaterialBehavior material(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_3D);

    PetitPas::Element element(nodes, 0, material);

    PetitPas::Matrix stiffnessMatrix = element.GetRawStiffnessMatrix();
}

TEST(TetrahedralPrismInCompression)
{
    // This test case is from oofem documentation: tests/sm/patch300.in
    // It is a prism meshed with tetrahedrons and loaded in compression.

    // Create the Mesh.
    PetitPas::Mesh mesh;

    //Add nodes.
    std::vector<double> coords(3);

    coords[0] = 0.0;
    coords[1] = 0.0;
    coords[2] = 0.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 1);
    }

    coords[0] = 3.0;
    coords[1] = 0.0;
    coords[2] = 0.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 2);
    }

    coords[0] = 0.0;
    coords[1] = 3.0;
    coords[2] = 0.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 3);
    }

    coords[0] = 0.0;
    coords[1] = 0.0;
    coords[2] = 6.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 4);
    }

    coords[0] = 3.0;
    coords[1] = 0.0;
    coords[2] = 6.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 5);
    }

    coords[0] = 0.0;
    coords[1] = 3.0;
    coords[2] = 6.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 6);
    }

    // Create the material.
    PetitPas::MaterialBehavior material(15.0, 0.25, PetitPas::MaterialBehavior::MaterialHypothesis_3D);

    // Add elements.
    std::vector<unsigned int> elementNodeIds(4);

    elementNodeIds[0] = 1;
    elementNodeIds[1] = 2;
    elementNodeIds[2] = 3;
    elementNodeIds[3] = 4;
    mesh.CreateElement(elementNodeIds, material, 1);

    elementNodeIds[0] = 4;
    elementNodeIds[1] = 5;
    elementNodeIds[2] = 2;
    elementNodeIds[3] = 3;
    mesh.CreateElement(elementNodeIds, material, 2);

    elementNodeIds[0] = 4;
    elementNodeIds[1] = 6;
    elementNodeIds[2] = 5;
    elementNodeIds[3] = 3;
    mesh.CreateElement(elementNodeIds, material, 3);

    // Create the analysis.
    PetitPas::Analysis analysis(mesh);

    // Add boundary conditions.

    // nodes 1,2,3 fully fixed.
    analysis.SetZeroDisplacement(1, 0);
    analysis.SetZeroDisplacement(1, 1);
    analysis.SetZeroDisplacement(1, 2);
    analysis.SetZeroDisplacement(2, 0);
    analysis.SetZeroDisplacement(2, 1);
    analysis.SetZeroDisplacement(2, 2);
    analysis.SetZeroDisplacement(3, 0);
    analysis.SetZeroDisplacement(3, 1);
    analysis.SetZeroDisplacement(3, 2);

    // nodes 4,5,6 fixed in X and Y
    analysis.SetZeroDisplacement(4, 0);
    analysis.SetZeroDisplacement(4, 1);
    analysis.SetZeroDisplacement(5, 0);
    analysis.SetZeroDisplacement(5, 1);
    analysis.SetZeroDisplacement(6, 0);
    analysis.SetZeroDisplacement(6, 1);

    // apply a vertical force of -1.5 at nodes 4,5,6
    analysis.SetForce(4, 2, -1.5);
    analysis.SetForce(5, 2, -1.5);
    analysis.SetForce(6, 2, -1.5);

    // Solve and get solution.
    PetitPas::Solution solution = analysis.GetSolution();

    // Check the displacements.
    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 1), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 2), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(2, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(2, 1), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(2, 2), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(3, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(3, 1), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(3, 2), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(4, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(4, 1), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(5, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(5, 1), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(6, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(6, 1), 1e-6);

    CHECK_CLOSE(-0.333333333, solution.GetNodalValue(4, 2), 1e-6);
    CHECK_CLOSE(-0.333333333, solution.GetNodalValue(5, 2), 1e-6);
    CHECK_CLOSE(-0.333333333, solution.GetNodalValue(6, 2), 1e-6);

    // Check strains.
    PetitPas::StrainResult strainResult(solution);

    const PetitPas::Matrix& element1StrainMatrix = strainResult.GetElementalStrainAtNode(1, 1);
    CHECK_CLOSE(0.0,         element1StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,         element1StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(-0.05555555, element1StrainMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(0.0,         element1StrainMatrix.GetValueAt(3,0), 1e-6);
    CHECK_CLOSE(0.0,         element1StrainMatrix.GetValueAt(4,0), 1e-6);
    CHECK_CLOSE(0.0,         element1StrainMatrix.GetValueAt(5,0), 1e-6);

    const PetitPas::Matrix& element2StrainMatrix = strainResult.GetElementalStrainAtNode(2, 2);
    CHECK_CLOSE(0.0,         element2StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,         element2StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(-0.05555555, element2StrainMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(0.0,         element2StrainMatrix.GetValueAt(3,0), 1e-6);
    CHECK_CLOSE(0.0,         element2StrainMatrix.GetValueAt(4,0), 1e-6);
    CHECK_CLOSE(0.0,         element2StrainMatrix.GetValueAt(5,0), 1e-6);

    const PetitPas::Matrix& element3StrainMatrix = strainResult.GetElementalStrainAtNode(3, 3);
    CHECK_CLOSE(0.0,         element3StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,         element3StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(-0.05555555, element3StrainMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(0.0,         element3StrainMatrix.GetValueAt(3,0), 1e-6);
    CHECK_CLOSE(0.0,         element3StrainMatrix.GetValueAt(4,0), 1e-6);
    CHECK_CLOSE(0.0,         element3StrainMatrix.GetValueAt(5,0), 1e-6);

    // Check stresses.
    PetitPas::StressResult stressResult(strainResult);

    const PetitPas::Matrix& element1StressMatrix = stressResult.GetElementalStressAtNode(1, 1);
    CHECK_CLOSE(-0.33333333, element1StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(-0.33333333, element1StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(-1.0,        element1StressMatrix.GetValueAt(2,0), 1e-2);
    CHECK_CLOSE(0.0,         element1StressMatrix.GetValueAt(3,0), 1e-2);
    CHECK_CLOSE(0.0,         element1StressMatrix.GetValueAt(4,0), 1e-2);
    CHECK_CLOSE(0.0,         element1StressMatrix.GetValueAt(5,0), 1e-2);

    const PetitPas::Matrix& element2StressMatrix = stressResult.GetElementalStressAtNode(2, 2);
    CHECK_CLOSE(-0.33333333, element2StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(-0.33333333, element2StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(-1.0,        element2StressMatrix.GetValueAt(2,0), 1e-2);
    CHECK_CLOSE(0.0,         element2StressMatrix.GetValueAt(3,0), 1e-2);
    CHECK_CLOSE(0.0,         element2StressMatrix.GetValueAt(4,0), 1e-2);
    CHECK_CLOSE(0.0,         element2StressMatrix.GetValueAt(5,0), 1e-2);

    const PetitPas::Matrix& element3StressMatrix = stressResult.GetElementalStressAtNode(3, 3);
    CHECK_CLOSE(-0.33333333, element3StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(-0.33333333, element3StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(-1.0,        element3StressMatrix.GetValueAt(2,0), 1e-2);
    CHECK_CLOSE(0.0,         element3StressMatrix.GetValueAt(3,0), 1e-2);
    CHECK_CLOSE(0.0,         element3StressMatrix.GetValueAt(4,0), 1e-2);
    CHECK_CLOSE(0.0,         element3StressMatrix.GetValueAt(5,0), 1e-2);

}

} //SUITE TetrahedralElement

