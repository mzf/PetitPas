/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/PolynomialFunction.h"
#include <stdexcept>

SUITE(PolynomialFunction)
{

TEST(PolynomialFunctionConstructor)
{
    //Empty size
    std::vector<PetitPas::PolynomialTerm> terms;
    CHECK_THROW(PetitPas::PolynomialFunction func(terms), std::runtime_error);

    //Bad dimension of the terms
    std::vector<int> degrees(2);
    terms.push_back(PetitPas::PolynomialTerm(degrees));
    degrees.resize(5);
    terms.push_back(PetitPas::PolynomialTerm(degrees));
    CHECK_THROW(PetitPas::PolynomialFunction func(terms), std::runtime_error);
}

TEST(PolynomialFunctionGetValue)
{
    //f(x,y,z) = 2 + 3*x + 4*y + 5*z + x^2*y + x*y*z + z^3
    std::vector<PetitPas::PolynomialTerm> terms;

    std::vector<int> degrees(3);
    terms.push_back(PetitPas::PolynomialTerm(degrees, 2.0));

    degrees[0] = 1;
    degrees[1] = 0;
    degrees[2] = 0;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 3.0));

    degrees[0] = 0;
    degrees[1] = 1;
    degrees[2] = 0;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 4.0));

    degrees[0] = 0;
    degrees[1] = 0;
    degrees[2] = 1;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 5.0));

    degrees[0] = 2;
    degrees[1] = 1;
    degrees[2] = 0;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 1.0));

    degrees[0] = 1;
    degrees[1] = 1;
    degrees[2] = 1;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 1.0));

    degrees[0] = 0;
    degrees[1] = 0;
    degrees[2] = 3;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 1.0));

    PetitPas::PolynomialFunction func(terms);

    std::vector<double> values(3);
    //x = y = z = 0.
    CHECK_CLOSE(2., func.GetValue(values), 1e-6);

    values[0] = 2.0; //x
    values[1] = 3.0; //y
    values[2] = 4.0; //z
    CHECK_CLOSE(140., func.GetValue(values), 1e-6);
}

TEST(PolynomialFunctionDerivativeValue)
{
    //f(x,y,z) = 2 + 3*x + 4*y + 5*z + x^2*y + x*y*z + z^3
    std::vector<PetitPas::PolynomialTerm> terms;

    std::vector<int> degrees(3);
    terms.push_back(PetitPas::PolynomialTerm(degrees, 2.0));

    degrees[0] = 1;
    degrees[1] = 0;
    degrees[2] = 0;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 3.0));

    degrees[0] = 0;
    degrees[1] = 1;
    degrees[2] = 0;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 4.0));

    degrees[0] = 0;
    degrees[1] = 0;
    degrees[2] = 1;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 5.0));

    degrees[0] = 2;
    degrees[1] = 1;
    degrees[2] = 0;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 1.0));

    degrees[0] = 1;
    degrees[1] = 1;
    degrees[2] = 1;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 1.0));

    degrees[0] = 0;
    degrees[1] = 0;
    degrees[2] = 3;
    terms.push_back(PetitPas::PolynomialTerm(degrees, 1.0));

    PetitPas::PolynomialFunction func(terms);

    //df/dx
    std::vector<double> values(3);
    values[0] = 2.0; //x
    values[1] = 3.0; //y
    values[2] = 4.0; //z
    CHECK_CLOSE(27., func.GetDerivativeValue(0, values), 1e-6);

    //df/dy
    CHECK_CLOSE(16., func.GetDerivativeValue(1, values), 1e-6);

    //df/dz
    CHECK_CLOSE(59., func.GetDerivativeValue(2, values), 1e-6);

    //Check bad index
    CHECK_THROW(func.GetDerivativeValue(3, values), std::runtime_error);

    //Check bad dimension
    std::vector<double> badValues(5);
    CHECK_THROW(func.GetDerivativeValue(0, badValues), std::runtime_error);
}

} //SUITE Polynomial Function
