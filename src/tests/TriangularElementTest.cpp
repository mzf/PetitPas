/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/Element.h"
#include "../core/MaterialBehavior.h"
#include "../core/Matrix.h"
#include "../core/Mesh.h"
#include "../core/Analysis.h"
#include "../core/Solution.h"
#include "../core/StrainResult.h"
#include "../core/StressResult.h"
#include <stdexcept>

SUITE(TriangularElement)
{

TEST(TriangularElements2DPlaneStress)
{
    // This test case is from oofem documentation: tests/sm/patch102.in

    // Create the Mesh.
    PetitPas::Mesh mesh;

    //Add nodes.
    std::vector<double> coords(2);

    coords[0] = 0.0;
    coords[1] = 0.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 1);
    }

    coords[0] = 6.0;
    coords[1] = 0.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 2);
    }

    coords[0] = 6.0;
    coords[1] = 3.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 3);
    }

    coords[0] = 0.0;
    coords[1] = 3.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 4);
    }

    coords[0] = 2.0;
    coords[1] = 1.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 5);
    }

    // Create the material.
    PetitPas::MaterialBehavior material(10.0, 0.1, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    // Add elements.
    std::vector<unsigned int> elementNodeIds(3);

    elementNodeIds[0] = 1;
    elementNodeIds[1] = 2;
    elementNodeIds[2] = 5;
    mesh.CreateElement(elementNodeIds, material, 1);

    elementNodeIds[0] = 2;
    elementNodeIds[1] = 3;
    elementNodeIds[2] = 5;
    mesh.CreateElement(elementNodeIds, material, 2);

    elementNodeIds[0] = 3;
    elementNodeIds[1] = 4;
    elementNodeIds[2] = 5;
    mesh.CreateElement(elementNodeIds, material, 3);

    elementNodeIds[0] = 4;
    elementNodeIds[1] = 1;
    elementNodeIds[2] = 5;
    mesh.CreateElement(elementNodeIds, material, 4);

    // Create the analysis.
    PetitPas::Analysis analysis(mesh);

    // Add boundary conditions.

    // nodes 1 and 4 fully fixed.
    analysis.SetZeroDisplacement(1, 0);
    analysis.SetZeroDisplacement(1, 1);
    analysis.SetZeroDisplacement(4, 0);
    analysis.SetZeroDisplacement(4, 1);

    // nodes 2 and 3 fixed in X
    analysis.SetZeroDisplacement(2, 0);
    analysis.SetZeroDisplacement(3, 0);

    // apply a vertical force of 1.5 at nodes 2 and 3
    analysis.SetForce(2, 1, 1.5);
    analysis.SetForce(3, 1, 1.5);

    // Solve and get solution.
    PetitPas::Solution solution = analysis.GetSolution();

    // Check the displacements.

    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 1), 1e-6);

    CHECK_CLOSE(0.0, solution.GetNodalValue(2, 0), 1e-6);
    CHECK_CLOSE(1.32, solution.GetNodalValue(2, 1), 1e-6);

    CHECK_CLOSE(0.0, solution.GetNodalValue(3, 0), 1e-6);
    CHECK_CLOSE(1.32, solution.GetNodalValue(3, 1), 1e-6);

    CHECK_CLOSE(0.0, solution.GetNodalValue(4, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(4, 1), 1e-6);

    CHECK_CLOSE(0.0, solution.GetNodalValue(5, 0), 1e-6);
    CHECK_CLOSE(0.44, solution.GetNodalValue(5, 1), 1e-6);

    // Check strains.
    PetitPas::StrainResult strainResult(solution);

    const PetitPas::Matrix& element1StrainMatrix = strainResult.GetElementalStrainAtNode(1, 1);
    CHECK_CLOSE(0.0,  element1StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,  element1StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(0.22, element1StrainMatrix.GetValueAt(2,0), 1e-6);

    const PetitPas::Matrix& element2StrainMatrix = strainResult.GetElementalStrainAtNode(2, 2);
    CHECK_CLOSE(0.0,  element2StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,  element2StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(0.22, element2StrainMatrix.GetValueAt(2,0), 1e-6);

    const PetitPas::Matrix& element3StrainMatrix = strainResult.GetElementalStrainAtNode(3, 3);
    CHECK_CLOSE(0.0,  element3StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,  element3StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(0.22, element3StrainMatrix.GetValueAt(2,0), 1e-6);

    const PetitPas::Matrix& element4StrainMatrix = strainResult.GetElementalStrainAtNode(3, 3);
    CHECK_CLOSE(0.0,  element4StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,  element4StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(0.22, element4StrainMatrix.GetValueAt(2,0), 1e-6);

    // Check stresses.
    PetitPas::StressResult stressResult(strainResult);

    const PetitPas::Matrix& element1StressMatrix = stressResult.GetElementalStressAtNode(1, 1);
    CHECK_CLOSE(0.0, element1StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(0.0, element1StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(1.0, element1StressMatrix.GetValueAt(2,0), 1e-2);

    const PetitPas::Matrix& element2StressMatrix = stressResult.GetElementalStressAtNode(2, 2);
    CHECK_CLOSE(0.0, element2StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(0.0, element2StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(1.0, element2StressMatrix.GetValueAt(2,0), 1e-2);

    const PetitPas::Matrix& element3StressMatrix = stressResult.GetElementalStressAtNode(3, 3);
    CHECK_CLOSE(0.0, element3StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(0.0, element3StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(1.0, element3StressMatrix.GetValueAt(2,0), 1e-2);

    const PetitPas::Matrix& element4StressMatrix = stressResult.GetElementalStressAtNode(3, 3);
    CHECK_CLOSE(0.0, element4StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(0.0, element4StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(1.0, element4StressMatrix.GetValueAt(2,0), 1e-2);

}

TEST(TriangularElements2DPlaneStrain)
{
    // This test case is from oofem documentation: tests/sm/patch107.in

    // Create the Mesh.
    PetitPas::Mesh mesh;

    //Add nodes.
    std::vector<double> coords(2);

    coords[0] = 0.0;
    coords[1] = 0.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 1);
    }

    coords[0] = 6.0;
    coords[1] = 0.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 2);
    }

    coords[0] = 6.0;
    coords[1] = 3.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 3);
    }

    coords[0] = 0.0;
    coords[1] = 3.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 4);
    }

    coords[0] = 2.0;
    coords[1] = 1.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 5);
    }

    // Create the material.
    PetitPas::MaterialBehavior material(10.0, 0.2, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStrain);

    // Add elements.
    std::vector<unsigned int> elementNodeIds(3);

    elementNodeIds[0] = 1;
    elementNodeIds[1] = 2;
    elementNodeIds[2] = 5;
    mesh.CreateElement(elementNodeIds, material, 1);

    elementNodeIds[0] = 2;
    elementNodeIds[1] = 3;
    elementNodeIds[2] = 5;
    mesh.CreateElement(elementNodeIds, material, 2);

    elementNodeIds[0] = 3;
    elementNodeIds[1] = 4;
    elementNodeIds[2] = 5;
    mesh.CreateElement(elementNodeIds, material, 3);

    elementNodeIds[0] = 4;
    elementNodeIds[1] = 1;
    elementNodeIds[2] = 5;
    mesh.CreateElement(elementNodeIds, material, 4);

    // Create the analysis.
    PetitPas::Analysis analysis(mesh);

    // Add boundary conditions.

    // nodes 1 and 4 fully fixed.
    analysis.SetZeroDisplacement(1, 0);
    analysis.SetZeroDisplacement(1, 1);
    analysis.SetZeroDisplacement(4, 0);
    analysis.SetZeroDisplacement(4, 1);

    // nodes 2 and 3 fixed in Y
    analysis.SetZeroDisplacement(2, 1);
    analysis.SetZeroDisplacement(3, 1);

    // apply an horizontal force of 1.5 at nodes 2 and 3
    analysis.SetForce(2, 0, 1.5);
    analysis.SetForce(3, 0, 1.5);

    // Solve and get solution.
    PetitPas::Solution solution = analysis.GetSolution();

    // Check the displacements.

    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 1), 1e-6);

    CHECK_CLOSE(0.54, solution.GetNodalValue(2, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(2, 1), 1e-6);

    CHECK_CLOSE(0.54, solution.GetNodalValue(3, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(3, 1), 1e-6);

    CHECK_CLOSE(0.0, solution.GetNodalValue(4, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(4, 1), 1e-6);

    CHECK_CLOSE(0.18, solution.GetNodalValue(5, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(5, 1), 1e-6);

    // Check strains.
    PetitPas::StrainResult strainResult(solution);

    const PetitPas::Matrix& element1StrainMatrix = strainResult.GetElementalStrainAtNode(1, 1);
    CHECK_CLOSE(9.0e-02, element1StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,     element1StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(0.0,     element1StrainMatrix.GetValueAt(2,0), 1e-6);

    const PetitPas::Matrix& element2StrainMatrix = strainResult.GetElementalStrainAtNode(2, 2);
    CHECK_CLOSE(9.0e-02, element2StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,     element2StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(0.0,     element2StrainMatrix.GetValueAt(2,0), 1e-6);

    const PetitPas::Matrix& element3StrainMatrix = strainResult.GetElementalStrainAtNode(3, 3);
    CHECK_CLOSE(9.0e-02, element3StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,     element3StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(0.0,     element3StrainMatrix.GetValueAt(2,0), 1e-6);

    const PetitPas::Matrix& element4StrainMatrix = strainResult.GetElementalStrainAtNode(3, 3);
    CHECK_CLOSE(9.0e-02, element4StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,     element4StrainMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(0.0,     element4StrainMatrix.GetValueAt(2,0), 1e-6);

    // Check stresses.
    PetitPas::StressResult stressResult(strainResult);

    const PetitPas::Matrix& element1StressMatrix = stressResult.GetElementalStressAtNode(1, 1);
    CHECK_CLOSE(1.0,  element1StressMatrix.GetValueAt(0,0), 1e-3);
    CHECK_CLOSE(0.25, element1StressMatrix.GetValueAt(1,0), 1e-3);
    CHECK_CLOSE(0.0,  element1StressMatrix.GetValueAt(2,0), 1e-3);

    const PetitPas::Matrix& element2StressMatrix = stressResult.GetElementalStressAtNode(2, 2);
    CHECK_CLOSE(1.0,  element2StressMatrix.GetValueAt(0,0), 1e-3);
    CHECK_CLOSE(0.25, element2StressMatrix.GetValueAt(1,0), 1e-3);
    CHECK_CLOSE(0.0,  element2StressMatrix.GetValueAt(2,0), 1e-3);

    const PetitPas::Matrix& element3StressMatrix = stressResult.GetElementalStressAtNode(3, 3);
    CHECK_CLOSE(1.0,  element3StressMatrix.GetValueAt(0,0), 1e-3);
    CHECK_CLOSE(0.25, element3StressMatrix.GetValueAt(1,0), 1e-3);
    CHECK_CLOSE(0.0,  element3StressMatrix.GetValueAt(2,0), 1e-3);

    const PetitPas::Matrix& element4StressMatrix = stressResult.GetElementalStressAtNode(3, 3);
    CHECK_CLOSE(1.0,  element4StressMatrix.GetValueAt(0,0), 1e-3);
    CHECK_CLOSE(0.25, element4StressMatrix.GetValueAt(1,0), 1e-3);
    CHECK_CLOSE(0.0,  element4StressMatrix.GetValueAt(2,0), 1e-3);

}

} //SUITE TriangularElement


