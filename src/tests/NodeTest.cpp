/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/Node.h"

SUITE(Node)
{

TEST(NodeConstructor)
{
    std::vector<double> coords(3);
    coords[0] = 4.0;
    coords[1] = 5.0;
    coords[2] = 6.0;
    PetitPas::Point point(coords);

    unsigned int nodeId = 123;

    PetitPas::Node node(point, nodeId);

    const std::vector<double>& readCoords = node.GetCoordinates();

    CHECK_EQUAL((unsigned int)3, readCoords.size());
    for(int i=0; i<3; i++)
    {
        CHECK_CLOSE(coords[i], readCoords[i], 1e-6);
    }

    CHECK_EQUAL(nodeId, node.GetId());

}

TEST(NodeDimension)
{
    std::vector<double> coords(3);
    coords[0] = 4.0;
    coords[1] = 5.0;
    coords[2] = 6.0;

    PetitPas::Point point(coords);
    PetitPas::Node node(point, 123);

    CHECK_EQUAL((unsigned int)3, node.GetDimension());
}

} // SUITE Node

