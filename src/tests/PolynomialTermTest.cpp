/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/PolynomialTerm.h"
#include <stdexcept>

SUITE(PolynomialTerm)
{

TEST(PolynomialTermConstructor)
{
    std::vector<int> degrees;
    CHECK_THROW(PetitPas::PolynomialTerm term(degrees), std::runtime_error);
}

TEST(PolynomialTermValue)
{
    //x*y^2
    std::vector<int> degrees(2);
    degrees[0] = 1;
    degrees[1] = 2;
    PetitPas::PolynomialTerm term(degrees);

    std::vector<double> values(2);
    values[0] = 5.0;
    values[1] = 2.0;
    CHECK_CLOSE(20.0, term.GetValue(values), 1e-6);

    //Check bad dimension
    std::vector<double> badValues(5);
    CHECK_THROW(term.GetValue(badValues), std::runtime_error);
}

TEST(PolynomialTermWithCoefficient)
{
    //3*x*y^2
    std::vector<int> degrees(2);
    degrees[0] = 1;
    degrees[1] = 2;
    PetitPas::PolynomialTerm term(degrees, 3.0);

    std::vector<double> values(2);
    values[0] = 5.0;
    values[1] = 2.0;
    CHECK_CLOSE(60.0, term.GetValue(values), 1e-6);
}

TEST(PolynomialTermSetCoefficient)
{
    //3*x*y^2
    std::vector<int> degrees(2);
    degrees[0] = 1;
    degrees[1] = 2;
    PetitPas::PolynomialTerm term(degrees, 3.0);

    std::vector<double> values(2);
    values[0] = 5.0;
    values[1] = 2.0;
    CHECK_CLOSE(60.0, term.GetValue(values), 1e-6);

    //2*x*y^2
    term.SetCoefficient(2.0);
    CHECK_CLOSE(40.0, term.GetValue(values), 1e-6);
}

TEST(PolynomialTermDimension)
{
    std::vector<int> degrees(3);
    PetitPas::PolynomialTerm term(degrees);

    CHECK_EQUAL((unsigned int)3, term.GetDimension());
}

TEST(PolynomialTermDerivative)
{
    //f(x,y) = 3*x*y^2
    std::vector<int> degrees(2);
    degrees[0] = 1;
    degrees[1] = 2;
    PetitPas::PolynomialTerm term(degrees, 3.0);

    //df/dx
    std::vector<double> values(2);
    values[0] = 5.0;
    values[1] = 2.0;
    CHECK_CLOSE(12.0, term.GetDerivativeValue(0, values), 1e-6);

    //df/dy
    CHECK_CLOSE(60.0, term.GetDerivativeValue(1, values), 1e-6);

    //Check bad index
    CHECK_THROW(term.GetDerivativeValue(2, values), std::runtime_error);

    //Check bad dimension
    std::vector<double> badValues(5);
    CHECK_THROW(term.GetValue(badValues), std::runtime_error);
}

TEST(PolynomialTermDerivativeAtZero)
{
    //f(x,y) = 3*x
    std::vector<int> degrees(2);
    degrees[0] = 1;
    degrees[1] = 0;
    PetitPas::PolynomialTerm term(degrees, 3.0);

    //df/dx
    std::vector<double> values(2);
    values[0] = 1.0;
    values[1] = 0.0;
    CHECK_CLOSE(3.0, term.GetDerivativeValue(0, values), 1e-6);

    //df/dy
    // Bug with std::pow when base=0 and exponent=-1
    CHECK_CLOSE(0.0, term.GetDerivativeValue(1, values), 1e-6);
}

} // SUITE PolynomialTerm
