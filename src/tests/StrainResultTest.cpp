/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/StrainResult.h"
#include "../core/Solution.h"
#include "../core/Mesh.h"
#include "../core/ResultNotFoundException.h"
#include <stdexcept>

// Implemented in MeshTest.cpp
void FillSmallMesh(PetitPas::Mesh& mesh);

SUITE(StrainResult)
{

void CheckLinearTriangleElementStrainValueAreConstant(PetitPas::StrainResult& strainResult, PetitPas::Element* element)
{
    unsigned int elementId = element->GetId();
    const std::vector<PetitPas::Node*> elementNodes = element->GetNodes();

    unsigned int firstNodeId = elementNodes[0]->GetId();
    const PetitPas::Matrix& firstNodeStrainMatrix = strainResult.GetElementalStrainAtNode(elementId, firstNodeId);

    for(std::vector<PetitPas::Node*>::const_iterator nodeIterator = elementNodes.begin();
        nodeIterator != elementNodes.end();
        ++nodeIterator)
    {
        unsigned int nodeId = (*nodeIterator)->GetId();
        const PetitPas::Matrix& strainMatrixAtNode = strainResult.GetElementalStrainAtNode(elementId, nodeId);
        CHECK_EQUAL(true, strainMatrixAtNode == firstNodeStrainMatrix);
    }
}


TEST(StrainResultConstructor)
{
    // Create a solution object for the result
    PetitPas::Mesh mesh;
    FillSmallMesh(mesh);
    std::vector<PetitPas::Node*> goodNodes = mesh.GetNodes();
    PetitPas::NodalValuesMatrix nodalValues(goodNodes);

    // Fill displacement values.
    double value = 1.0;
    for(unsigned int nodeId = 1; nodeId < 6; nodeId++)
    {
        for(unsigned int dofIndex = 0; dofIndex < 2; dofIndex++)
        {
            nodalValues.SetValue(nodeId, dofIndex, value);
            value += 1.0;
        }
    }

    // Create the solution.
    PetitPas::Solution solution(mesh, nodalValues);

    //Create the Strain result.
    PetitPas::StrainResult strainResult(solution);

    // Check bad element id and bad node id.
    CHECK_THROW(strainResult.GetElementalStrainAtNode(3, 1), PetitPas::ResultNotFoundException);
    CHECK_THROW(strainResult.GetElementalStrainAtNode(1, 4), PetitPas::ResultNotFoundException);

    // Check no error on valid input.
    const PetitPas::Matrix& strainMatrix = strainResult.GetElementalStrainAtNode(1, 2);
    CHECK_EQUAL((unsigned int)3, strainMatrix.GetRowCount());
    CHECK_EQUAL((unsigned int)1, strainMatrix.GetColumnCount());

    // We are on linear triangles so the valid must stay the same for each node of an element.
    PetitPas::Element* currentElement = mesh.GetElement(1);
    CheckLinearTriangleElementStrainValueAreConstant(strainResult, currentElement);
    currentElement = mesh.GetElement(2);
    CheckLinearTriangleElementStrainValueAreConstant(strainResult, currentElement);

}

} //SUITE StrainResult
