/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/MaterialBehavior.h"
#include "../core/Matrix.h"
#include <stdexcept>

SUITE(MaterialBehavior)
{

TEST(MaterialBehaviorConstructor)
{
    //Check bad poisson coefficients
    CHECK_THROW(PetitPas::MaterialBehavior mat(100.0, -5.0, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress), std::runtime_error);
    CHECK_THROW(PetitPas::MaterialBehavior mat(100.0, 5.0, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress), std::runtime_error);
}

TEST(MaterialBehavior2DPlaneStressMatrix)
{
    PetitPas::MaterialBehavior material(100.0, 0.3, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    const PetitPas::Matrix& hookeMatrix = material.GetHookeMatrix();

    CHECK_EQUAL((unsigned int)3, hookeMatrix.GetRowCount());
    CHECK_EQUAL((unsigned int)3, hookeMatrix.GetColumnCount());

    CHECK_CLOSE(109.8901099, hookeMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(32.96703297, hookeMatrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(0,2), 1e-6);
    CHECK_CLOSE(32.96703297, hookeMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(109.8901099, hookeMatrix.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(1,2), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(38.46153846, hookeMatrix.GetValueAt(2,2), 1e-6);
}

TEST(MaterialBehavior2DPlaneStrainMatrix)
{
    PetitPas::MaterialBehavior material(100.0, 0.3, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStrain);

    const PetitPas::Matrix& hookeMatrix = material.GetHookeMatrix();

    CHECK_EQUAL((unsigned int)3, hookeMatrix.GetRowCount());
    CHECK_EQUAL((unsigned int)3, hookeMatrix.GetColumnCount());

    CHECK_CLOSE(134.6153846, hookeMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(57.69230769, hookeMatrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(0,2), 1e-6);
    CHECK_CLOSE(57.69230769, hookeMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(134.6153846, hookeMatrix.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(1,2), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(38.46153846, hookeMatrix.GetValueAt(2,2), 1e-6);
}

TEST(MaterialBehavior3DMatrix)
{
    PetitPas::MaterialBehavior material(100.0, 0.3, PetitPas::MaterialBehavior::MaterialHypothesis_3D);

    const PetitPas::Matrix& hookeMatrix = material.GetHookeMatrix();

    CHECK_EQUAL((unsigned int)6, hookeMatrix.GetRowCount());
    CHECK_EQUAL((unsigned int)6, hookeMatrix.GetColumnCount());

    CHECK_CLOSE(134.6153846, hookeMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(57.69230769, hookeMatrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(57.69230769, hookeMatrix.GetValueAt(0,2), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(0,3), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(0,4), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(0,5), 1e-6);

    CHECK_CLOSE(57.69230769, hookeMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(134.6153846, hookeMatrix.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(57.69230769, hookeMatrix.GetValueAt(1,2), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(1,3), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(1,4), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(1,5), 1e-6);

    CHECK_CLOSE(57.69230769, hookeMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(57.69230769, hookeMatrix.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(134.6153846, hookeMatrix.GetValueAt(2,2), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(2,3), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(2,4), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(2,5), 1e-6);

    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(3,0), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(3,1), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(3,2), 1e-6);
    CHECK_CLOSE(38.46153846, hookeMatrix.GetValueAt(3,3), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(3,4), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(3,5), 1e-6);

    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(4,0), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(4,1), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(4,2), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(4,3), 1e-6);
    CHECK_CLOSE(38.46153846, hookeMatrix.GetValueAt(4,4), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(4,5), 1e-6);

    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(5,0), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(5,1), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(5,2), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(5,3), 1e-6);
    CHECK_CLOSE(0.0,         hookeMatrix.GetValueAt(5,4), 1e-6);
    CHECK_CLOSE(38.46153846, hookeMatrix.GetValueAt(5,5), 1e-6);

}

} // SUITE MaterialBehavior
