/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/Analysis.h"
#include "../core/Mesh.h"
#include "../core/MaterialBehavior.h"
#include "../core/StrainResult.h"
#include "../core/StressResult.h"
#include <stdexcept>

// Implemented in MeshTest.cpp
void FillSmallMesh(PetitPas::Mesh& mesh);

SUITE(Analysis)
{

TEST(AnalysisConstructor)
{
    PetitPas::Mesh mesh;
    FillSmallMesh(mesh);
    PetitPas::Analysis analysis(mesh);
}

TEST(AnalysisSetZeroDisplacement)
{
    PetitPas::Mesh mesh;
    FillSmallMesh(mesh);
    PetitPas::Analysis analysis(mesh);

    // Check bad node id
    CHECK_THROW(analysis.SetZeroDisplacement(42, 0), std::runtime_error);

    // Check bad DOF index
    CHECK_THROW(analysis.SetZeroDisplacement(2, 2), std::runtime_error);

    analysis.SetZeroDisplacement(2, 0);
}

TEST(AnalysisSetZeroDisplacementAll)
{
    PetitPas::Mesh mesh;
    FillSmallMesh(mesh);
    PetitPas::Analysis analysis(mesh);

    // Check bad node id
    CHECK_THROW(analysis.SetZeroDisplacement(42), std::runtime_error);

    analysis.SetZeroDisplacement(2);
}


TEST(AnalysisSetDisplacement)
{
    PetitPas::Mesh mesh;
    FillSmallMesh(mesh);
    PetitPas::Analysis analysis(mesh);

    // Check bad node id
    CHECK_THROW(analysis.SetDisplacement(42, 0, 1.0), std::runtime_error);

    // Check bad DOF index
    CHECK_THROW(analysis.SetDisplacement(2, 2, 1.0), std::runtime_error);

    analysis.SetDisplacement(2, 0, 10.0);
}

TEST(AnalysisSetForce)
{
    PetitPas::Mesh mesh;
    FillSmallMesh(mesh);
    PetitPas::Analysis analysis(mesh);

    // Check bad node id
    CHECK_THROW(analysis.SetForce(42, 0, 1.0), std::runtime_error);

    // Check bad DOF index
    CHECK_THROW(analysis.SetForce(2, 2, 1.0), std::runtime_error);

    analysis.SetForce(2, 0, 10.0);
}

TEST(AnalysisGetResultSimpleMesh)
{
    // Create a small analysis with 2 triangle elements
    //  3   4
    //  +---+
    //  |\  |
    //  | \ |
    //  |  \|
    //  +---+
    //  1   2

    PetitPas::Mesh mesh;
    PetitPas::MaterialBehavior material(100.0, 0.3, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    std::vector<double> location(2);

    {
        location[0] = 1.0;
        location[1] = 1.0;
        PetitPas::Point point(location);
        mesh.CreateNode(point,1);
    }
    {
        location[0] = 2.0;
        location[1] = 1.0;
        PetitPas::Point point(location);
        mesh.CreateNode(point,2);
    }
    {
        location[0] = 1.0;
        location[1] = 3.0;
        PetitPas::Point point(location);
        mesh.CreateNode(point,3);
    }
    {
        location[0] = 2.0;
        location[1] = 3.0;
        PetitPas::Point point(location);
        mesh.CreateNode(point,4);
    }

    std::vector<unsigned int> nodeIds(3);
    nodeIds[0] = 1;
    nodeIds[1] = 2;
    nodeIds[2] = 3;
    mesh.CreateElement(nodeIds, material);

    nodeIds[0] = 2;
    nodeIds[1] = 3;
    nodeIds[2] = 4;
    mesh.CreateElement(nodeIds, material);

    PetitPas::Analysis analysis(mesh);

    // Block the base of the system
    analysis.SetZeroDisplacement(1, 0);
    analysis.SetZeroDisplacement(1, 1);
    analysis.SetZeroDisplacement(2, 0);
    analysis.SetZeroDisplacement(2, 1);

    // Set a force at node 3
    analysis.SetForce(3, 0, 10.0);

    PetitPas::Solution solution = analysis.GetSolution();

    // Check blocked DOFs.
    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 1), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(2, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(2, 1), 1e-6);

    // Check that the node with force has moved.
    CHECK_EQUAL(true, solution.GetNodalValue(3, 0) > 0.0);

}

TEST(Analysis2DTriangleMesh)
{
    // This test solves a problem on a triangle mesh:
    //
    /*    y
        ^
        |
     N6 +
        |\
        | \
        |  \
        |E4 \
     N4 +----+ N5
        |\ E2|\
        | \  | \
        |  \ |  \
        |E1 \|E3 \
        +----+----+--> x
        N1   N2   N3
    */
    // Nodes coordinates are (in m):
    // N1 (0,0)
    // N2 (3,0)
    // N3 (6,0)
    // N4 (0,3)
    // N5 (3,3)
    // N6 (0,6)
    //
    // Element's nodes:
    // E1: N1, N2, N4
    // E2: N2, N5, N4
    // E3: N2, N3, N5
    // E4: N4, N5, N6
    //
    // Boundary conditions (forces in kN):
    // N1, N2 and N3 fixed on x and y.
    // Force on N4 (90.0, -112.5)
    // Force on N5 (0.0, -112.5)
    // Force on N6 (15.0, -37.5)
    //
    // Material in 2D plane strain:
    // E = 30*10^3 (MPa)
    // v = 0.2


    // Create the Mesh.
    PetitPas::Mesh mesh;

    //Add nodes.
    std::vector<double> coords(2);

    coords[0] = 0.0;
    coords[1] = 0.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 1);
    }

    coords[0] = 3.0;
    coords[1] = 0.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 2);
    }

    coords[0] = 6.0;
    coords[1] = 0.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 3);
    }

    coords[0] = 0.0;
    coords[1] = 3.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 4);
    }

    coords[0] = 3.0;
    coords[1] = 3.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 5);
    }

    coords[0] = 0.0;
    coords[1] = 6.0;
    {
        PetitPas::Point point(coords);
        mesh.CreateNode(point, 6);
    }

    // Create the material.
    PetitPas::MaterialBehavior material(30e3, 0.2, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStrain);

    // Add elements.
    std::vector<unsigned int> elementNodeIds(3);

    elementNodeIds[0] = 1;
    elementNodeIds[1] = 2;
    elementNodeIds[2] = 4;
    mesh.CreateElement(elementNodeIds, material, 1);

    elementNodeIds[0] = 2;
    elementNodeIds[1] = 5;
    elementNodeIds[2] = 4;
    mesh.CreateElement(elementNodeIds, material, 2);

    elementNodeIds[0] = 2;
    elementNodeIds[1] = 3;
    elementNodeIds[2] = 5;
    mesh.CreateElement(elementNodeIds, material, 3);

    elementNodeIds[0] = 4;
    elementNodeIds[1] = 5;
    elementNodeIds[2] = 6;
    mesh.CreateElement(elementNodeIds, material, 4);

    // Create the analysis.
    PetitPas::Analysis analysis(mesh);

    // Add boundary conditions.
    analysis.SetZeroDisplacement(1, 0);
    analysis.SetZeroDisplacement(1, 1);
    analysis.SetZeroDisplacement(2, 0);
    analysis.SetZeroDisplacement(2, 1);
    analysis.SetZeroDisplacement(3, 0);
    analysis.SetZeroDisplacement(3, 1);

    analysis.SetForce(4, 0, 90.0);
    analysis.SetForce(4, 1, -112.5);
    analysis.SetForce(5, 1, -112.5);
    analysis.SetForce(6, 0, 15.0);
    analysis.SetForce(6, 1, -37.5);

    // Solve and get solution.
    PetitPas::Solution solution = analysis.GetSolution();

    // Check the displacements.
    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(1, 1), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(2, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(2, 1), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(3, 0), 1e-6);
    CHECK_CLOSE(0.0, solution.GetNodalValue(3, 1), 1e-6);

    CHECK_CLOSE( 6.0231e-3, solution.GetNodalValue(4, 0), 1e-6);
    CHECK_CLOSE(-5.8758e-3, solution.GetNodalValue(4, 1), 1e-6);
    CHECK_CLOSE( 4.8456e-3, solution.GetNodalValue(5, 0), 1e-6);
    CHECK_CLOSE(-4.7899e-3, solution.GetNodalValue(5, 1), 1e-6);
    CHECK_CLOSE( 7.3373e-3, solution.GetNodalValue(6, 0), 1e-6);
    CHECK_CLOSE(-7.8315e-3, solution.GetNodalValue(6, 1), 1e-6);

    // Check strains.
    PetitPas::StrainResult strainResult(solution);

    const PetitPas::Matrix& element1StrainMatrix = strainResult.GetElementalStrainAtNode(1, 1);
    CHECK_CLOSE(0.0, element1StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(-0.0019585, element1StrainMatrix.GetValueAt(1,0), 1e-7);
    CHECK_CLOSE(0.0020076, element1StrainMatrix.GetValueAt(2,0), 1e-6);

    const PetitPas::Matrix& element2StrainMatrix = strainResult.GetElementalStrainAtNode(2, 2);
    CHECK_CLOSE(-0.0003925, element2StrainMatrix.GetValueAt(0,0), 1e-7);
    CHECK_CLOSE(-0.001596633, element2StrainMatrix.GetValueAt(1,0), 1e-7);
    CHECK_CLOSE(0.001977167, element2StrainMatrix.GetValueAt(2,0), 1e-7);

    const PetitPas::Matrix& element3StrainMatrix = strainResult.GetElementalStrainAtNode(3, 2);
    CHECK_CLOSE(0.0, element3StrainMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(-0.001596667, element3StrainMatrix.GetValueAt(1,0), 1e-7);
    CHECK_CLOSE(0.001615222, element3StrainMatrix.GetValueAt(2,0), 1e-7);

    const PetitPas::Matrix& element4StrainMatrix = strainResult.GetElementalStrainAtNode(4, 6);
    CHECK_CLOSE(-0.0003925, element4StrainMatrix.GetValueAt(0,0), 1e-7);
    CHECK_CLOSE(-0.0006519, element4StrainMatrix.GetValueAt(1,0), 1e-7);
    CHECK_CLOSE(0.000800033, element4StrainMatrix.GetValueAt(2,0), 1e-6);

    // Check stresses.
    PetitPas::StressResult stressResult(strainResult);

    const PetitPas::Matrix& element1StressMatrix = stressResult.GetElementalStressAtNode(1, 1);
    CHECK_CLOSE(-16.321, element1StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(-65.285, element1StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(25.095, element1StressMatrix.GetValueAt(2,0), 1e-2);

    const PetitPas::Matrix& element2StressMatrix = stressResult.GetElementalStressAtNode(2, 2);
    CHECK_CLOSE(-26.388, element2StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(-56.492, element2StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(24.7145, element2StressMatrix.GetValueAt(2,0), 1e-2);

    const PetitPas::Matrix& element3StressMatrix = stressResult.GetElementalStressAtNode(3, 2);
    CHECK_CLOSE(-13.305, element3StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(-53.222, element3StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(20.19, element3StressMatrix.GetValueAt(2,0), 1e-2);

    const PetitPas::Matrix& element4StressMatrix = stressResult.GetElementalStressAtNode(4, 6);
    CHECK_CLOSE(-18.515, element4StressMatrix.GetValueAt(0,0), 1e-2);
    CHECK_CLOSE(-25.001, element4StressMatrix.GetValueAt(1,0), 1e-2);
    CHECK_CLOSE(10.0, element4StressMatrix.GetValueAt(2,0), 1e-2);
}

} //SUITE Analysis
