/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/StiffnessMatrix.h"
#include "../core/Matrix.h"
#include <stdexcept>

SUITE(StiffnessMatrix)
{

TEST(StiffnessMatrixConstructorNotSquare)
{
    //Check matrix not square
    PetitPas::Matrix notSquareMatrix(3,2);
    std::vector<PetitPas::Node*> emptyNodes;
    CHECK_THROW(PetitPas::StiffnessMatrix(notSquareMatrix, emptyNodes), std::runtime_error);
}

TEST(StiffnessMatrixConstructorNodeListEmpty)
{
    //Check node list empty
    PetitPas::Matrix squareMatrix(4,4);
    std::vector<PetitPas::Node*> emptyNodes;
    CHECK_THROW(PetitPas::StiffnessMatrix(squareMatrix, emptyNodes), std::runtime_error);
}

TEST(StiffnessMatrixConstructorBadDimensions)
{
    PetitPas::Matrix squareMatrix(4,4);
    std::vector<double> coords(3);
    coords[0] = 4.0;
    coords[1] = 5.0;
    coords[2] = 6.0;
    PetitPas::Point point(coords);
    PetitPas::Node node(point, 123);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node);

    CHECK_THROW(PetitPas::StiffnessMatrix(squareMatrix, nodeList), std::runtime_error);
}

TEST(StiffnessMatrixConstructorNoMatrix)
{
    std::vector<double> coords(3);
    coords[0] = 4.0;
    coords[1] = 5.0;
    coords[2] = 6.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);

    PetitPas::StiffnessMatrix stiffnessMatrix(nodeList);

    // Check dimension of the auto-generated matrix.
    const PetitPas::Matrix& generatedMatrix = stiffnessMatrix.GetRawMatrix();
    CHECK_EQUAL(true, generatedMatrix.IsSquare());
    CHECK_EQUAL((unsigned int)6, generatedMatrix.GetRowCount());
}

TEST(StiffnessMatrixCopyConstructor)
{
    PetitPas::Matrix matrix(1,1);
    matrix.SetValueAt(0,0, 42.0);
    std::vector<double> coords(1);
    coords[0] = 4.0;
    PetitPas::Point point(coords);
    PetitPas::Node node(point, 1);
    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node);
    PetitPas::StiffnessMatrix stiffnessMatrix(matrix, nodeList);

    //Copy the stiffness matrix and check the values
    PetitPas::StiffnessMatrix stiffnessMatrixCopy(stiffnessMatrix);

    const PetitPas::Matrix* matrixPointer = &(stiffnessMatrix.GetRawMatrix());
    const PetitPas::Matrix* copyMatrixPointer = &(stiffnessMatrixCopy.GetRawMatrix());

    CHECK(matrixPointer != copyMatrixPointer);
    CHECK_EQUAL(42.0, copyMatrixPointer->GetValueAt(0,0));
}

TEST(StiffnessMatrixAssignmentOperator)
{
    PetitPas::Matrix matrix(1,1);
    matrix.SetValueAt(0,0, 42.0);
    std::vector<double> coords(1);
    coords[0] = 4.0;
    PetitPas::Point point(coords);
    PetitPas::Node node(point, 1);
    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node);
    PetitPas::StiffnessMatrix stiffnessMatrix(matrix, nodeList);

    // Use the assignment operator (=) and check the values.
    PetitPas::StiffnessMatrix stiffnessMatrixCopy = stiffnessMatrix;

    const PetitPas::Matrix* matrixPointer = &(stiffnessMatrix.GetRawMatrix());
    const PetitPas::Matrix* copyMatrixPointer = &(stiffnessMatrixCopy.GetRawMatrix());

    CHECK(matrixPointer != copyMatrixPointer);
    CHECK_EQUAL(42.0, copyMatrixPointer->GetValueAt(0,0));
}

TEST(StiffnessMatrixGetRawMatrix)
{
    PetitPas::Matrix squareMatrix(2,2);
    squareMatrix.SetValueAt(0,0, 1.0);
    squareMatrix.SetValueAt(0,1, 2.0);
    squareMatrix.SetValueAt(1,0, 3.0);
    squareMatrix.SetValueAt(1,1, 4.0);

    std::vector<double> coords(1);
    coords[0] = 4.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);

    PetitPas::StiffnessMatrix stiffnessMatrix(squareMatrix, nodeList);

    PetitPas::Matrix rawMatrix = stiffnessMatrix.GetRawMatrix();

    CHECK_CLOSE(squareMatrix.GetValueAt(0,0), rawMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(squareMatrix.GetValueAt(0,1), rawMatrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(squareMatrix.GetValueAt(1,0), rawMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(squareMatrix.GetValueAt(1,1), rawMatrix.GetValueAt(1,1), 1e-6);
}

TEST(StiffnessMatrixContainsNode)
{
    PetitPas::Matrix squareMatrix(2,2);

    std::vector<double> coords(1);
    coords[0] = 4.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);

    // Constructor with matrix.
    PetitPas::StiffnessMatrix stiffnessMatrix(squareMatrix, nodeList);
    CHECK_EQUAL(true, stiffnessMatrix.ContainsNode(1));
    CHECK_EQUAL(true, stiffnessMatrix.ContainsNode(2));
    CHECK_EQUAL(false, stiffnessMatrix.ContainsNode(3));

    // Constructor without matrix.
    PetitPas::StiffnessMatrix stiffnessMatrixWithout(nodeList);
    CHECK_EQUAL(true, stiffnessMatrixWithout.ContainsNode(1));
    CHECK_EQUAL(true, stiffnessMatrixWithout.ContainsNode(2));
    CHECK_EQUAL(false, stiffnessMatrixWithout.ContainsNode(3));
}

TEST(StiffnessMatrixGetNodeIndex)
{
    PetitPas::Matrix squareMatrix(4,4);

    std::vector<double> coords(2);
    coords[0] = 1.0;
    coords[1] = 2.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);

    PetitPas::StiffnessMatrix stiffnessMatrix(squareMatrix, nodeList);

    CHECK_EQUAL((unsigned int)0, stiffnessMatrix.GetNodeIndex(1));
    CHECK_EQUAL((unsigned int)2, stiffnessMatrix.GetNodeIndex(2));

    CHECK_THROW(stiffnessMatrix.GetNodeIndex(3), std::runtime_error);
}

TEST(StiffnessMatrixGetNodeIds)
{
    PetitPas::Matrix squareMatrix(4,4);

    std::vector<double> coords(2);
    coords[0] = 1.0;
    coords[1] = 2.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);

    PetitPas::StiffnessMatrix stiffnessMatrix(squareMatrix, nodeList);

    std::vector<unsigned int> nodeIds = stiffnessMatrix.GetNodeIds();

    CHECK_EQUAL((unsigned int)2, nodeIds.size());
    CHECK_EQUAL((unsigned int)1, nodeIds[0]);
    CHECK_EQUAL((unsigned int)2, nodeIds[1]);
}

TEST(StiffnessMatrixAddIncompatibleMatrix)
{
    // First matrix with nodes 1 and 2.
    PetitPas::Matrix squareMatrix(4,4);
    std::vector<double> coords(2);
    coords[0] = 1.0;
    coords[1] = 2.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);

    PetitPas::StiffnessMatrix stiffnessMatrix1(squareMatrix, nodeList);

    // Second matrix with nodes 1 and 3
    PetitPas::Node node3(point, 3);
    nodeList.clear();
    nodeList.push_back(&node1);
    nodeList.push_back(&node3);

    PetitPas::StiffnessMatrix stiffnessMatrix2(squareMatrix, nodeList);

    //Add matrix
    CHECK_THROW(stiffnessMatrix1.Assemble(stiffnessMatrix2), std::runtime_error);
}

TEST(StiffnessMatrixAdd1D)
{
    //System with 4 nodes and 4 elements in 1D:
    // element 1: nodes 1, 2
    // element 2: nodes 1, 3
    // element 3: nodes 3, 2
    // element 4: nodes 3, 4

    // Create nodes.
    std::vector<double> coords(1);
    coords[0] = 1.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);
    PetitPas::Node node3(point, 3);
    PetitPas::Node node4(point, 4);
    std::vector<PetitPas::Node*> nodeList;

    // element 1
    PetitPas::Matrix matrix1(2,2);
    matrix1.SetValueAt(0,0, 1.0);
    matrix1.SetValueAt(0,1, 2.0);
    matrix1.SetValueAt(1,0, 3.0);
    matrix1.SetValueAt(1,1, 4.0);
    nodeList.clear();
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);
    PetitPas::StiffnessMatrix stiffnessMatrix1(matrix1, nodeList);

    // element 2
    PetitPas::Matrix matrix2(2,2);
    matrix2.SetValueAt(0,0,5.0);
    matrix2.SetValueAt(0,1,6.0);
    matrix2.SetValueAt(1,0,7.0);
    matrix2.SetValueAt(1,1,8.0);
    nodeList.clear();
    nodeList.push_back(&node1);
    nodeList.push_back(&node3);
    PetitPas::StiffnessMatrix stiffnessMatrix2(matrix2, nodeList);

    // element 3
    PetitPas::Matrix matrix3(2,2);
    matrix3.SetValueAt(0,0,9.0);
    matrix3.SetValueAt(0,1,10.0);
    matrix3.SetValueAt(1,0,11.0);
    matrix3.SetValueAt(1,1,12.0);
    nodeList.clear();
    nodeList.push_back(&node3);
    nodeList.push_back(&node2);
    PetitPas::StiffnessMatrix stiffnessMatrix3(matrix3, nodeList);

    // element 4
    PetitPas::Matrix matrix4(2,2);
    matrix4.SetValueAt(0,0,13.0);
    matrix4.SetValueAt(0,1,14.0);
    matrix4.SetValueAt(1,0,15.0);
    matrix4.SetValueAt(1,1,16.0);
    nodeList.clear();
    nodeList.push_back(&node3);
    nodeList.push_back(&node4);
    PetitPas::StiffnessMatrix stiffnessMatrix4(matrix4, nodeList);

    // The global matrix.
    PetitPas::Matrix globalMatrix(4,4);
    nodeList.clear();
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);
    nodeList.push_back(&node3);
    nodeList.push_back(&node4);
    PetitPas::StiffnessMatrix globalStiffnessMatrix(globalMatrix, nodeList);

    // Assemble all matrix.
    globalStiffnessMatrix.Assemble(stiffnessMatrix1);
    globalStiffnessMatrix.Assemble(stiffnessMatrix2);
    globalStiffnessMatrix.Assemble(stiffnessMatrix3);
    globalStiffnessMatrix.Assemble(stiffnessMatrix4);

    // Check values.
    const PetitPas::Matrix& rawMatrix = globalStiffnessMatrix.GetRawMatrix();
    CHECK_CLOSE(6.0,  rawMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(2.0,  rawMatrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(6.0,  rawMatrix.GetValueAt(0,2), 1e-6);
    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(0,3), 1e-6);
    CHECK_CLOSE(3.0,  rawMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(16.0, rawMatrix.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(11.0, rawMatrix.GetValueAt(1,2), 1e-6);
    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(1,3), 1e-6);
    CHECK_CLOSE(7.0,  rawMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(10.0, rawMatrix.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(30.0, rawMatrix.GetValueAt(2,2), 1e-6);
    CHECK_CLOSE(14.0, rawMatrix.GetValueAt(2,3), 1e-6);
    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(3,0), 1e-6);
    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(3,1), 1e-6);
    CHECK_CLOSE(15.0, rawMatrix.GetValueAt(3,2), 1e-6);
    CHECK_CLOSE(16.0, rawMatrix.GetValueAt(3,3), 1e-6);
}

TEST(StiffnessMatrixAdd2D)
{
    //System with 3 nodes and 2 elements in 2D:
    // element 1: nodes 1, 2
    // element 2: nodes 2, 3

    // Create nodes.
    std::vector<double> coords(2);
    coords[0] = 1.0;
    coords[1] = 2.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);
    PetitPas::Node node3(point, 3);
    std::vector<PetitPas::Node*> nodeList;

    // element 1
    PetitPas::Matrix matrix1(4,4);
    matrix1.SetValueAt(0,0, 1.0);
    matrix1.SetValueAt(0,1, 2.0);
    matrix1.SetValueAt(0,2, 3.0);
    matrix1.SetValueAt(0,3, 4.0);
    matrix1.SetValueAt(1,0, 5.0);
    matrix1.SetValueAt(1,1, 6.0);
    matrix1.SetValueAt(1,2, 7.0);
    matrix1.SetValueAt(1,3, 8.0);
    matrix1.SetValueAt(2,0, 9.0);
    matrix1.SetValueAt(2,1, 10.0);
    matrix1.SetValueAt(2,2, 11.0);
    matrix1.SetValueAt(2,3, 12.0);
    matrix1.SetValueAt(3,0, 13.0);
    matrix1.SetValueAt(3,1, 14.0);
    matrix1.SetValueAt(3,2, 15.0);
    matrix1.SetValueAt(3,3, 16.0);
    nodeList.clear();
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);
    PetitPas::StiffnessMatrix stiffnessMatrix1(matrix1, nodeList);

    // element 2
    PetitPas::Matrix matrix2(4,4);
    matrix2.SetValueAt(0,0, 17.0);
    matrix2.SetValueAt(0,1, 18.0);
    matrix2.SetValueAt(0,2, 19.0);
    matrix2.SetValueAt(0,3, 20.0);
    matrix2.SetValueAt(1,0, 21.0);
    matrix2.SetValueAt(1,1, 22.0);
    matrix2.SetValueAt(1,2, 23.0);
    matrix2.SetValueAt(1,3, 24.0);
    matrix2.SetValueAt(2,0, 25.0);
    matrix2.SetValueAt(2,1, 26.0);
    matrix2.SetValueAt(2,2, 27.0);
    matrix2.SetValueAt(2,3, 28.0);
    matrix2.SetValueAt(3,0, 29.0);
    matrix2.SetValueAt(3,1, 30.0);
    matrix2.SetValueAt(3,2, 31.0);
    matrix2.SetValueAt(3,3, 32.0);
    nodeList.clear();
    nodeList.push_back(&node2);
    nodeList.push_back(&node3);
    PetitPas::StiffnessMatrix stiffnessMatrix2(matrix2, nodeList);

    // The global matrix.
    PetitPas::Matrix globalMatrix(6,6);
    nodeList.clear();
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);
    nodeList.push_back(&node3);
    PetitPas::StiffnessMatrix globalStiffnessMatrix(globalMatrix, nodeList);

    // Assemble all matrix.
    globalStiffnessMatrix.Assemble(stiffnessMatrix1);
    globalStiffnessMatrix.Assemble(stiffnessMatrix2);

    // Check values.
    const PetitPas::Matrix& rawMatrix = globalStiffnessMatrix.GetRawMatrix();
    CHECK_CLOSE(1.0,  rawMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(2.0,  rawMatrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(3.0,  rawMatrix.GetValueAt(0,2), 1e-6);
    CHECK_CLOSE(4.0,  rawMatrix.GetValueAt(0,3), 1e-6);
    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(0,4), 1e-6);
    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(0,5), 1e-6);

    CHECK_CLOSE(5.0,  rawMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(6.0,  rawMatrix.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(7.0,  rawMatrix.GetValueAt(1,2), 1e-6);
    CHECK_CLOSE(8.0,  rawMatrix.GetValueAt(1,3), 1e-6);
    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(1,4), 1e-6);
    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(1,5), 1e-6);

    CHECK_CLOSE(9.0,  rawMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(10.0, rawMatrix.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(28.0, rawMatrix.GetValueAt(2,2), 1e-6);
    CHECK_CLOSE(30.0, rawMatrix.GetValueAt(2,3), 1e-6);
    CHECK_CLOSE(19.0, rawMatrix.GetValueAt(2,4), 1e-6);
    CHECK_CLOSE(20.0, rawMatrix.GetValueAt(2,5), 1e-6);

    CHECK_CLOSE(13.0, rawMatrix.GetValueAt(3,0), 1e-6);
    CHECK_CLOSE(14.0, rawMatrix.GetValueAt(3,1), 1e-6);
    CHECK_CLOSE(36.0, rawMatrix.GetValueAt(3,2), 1e-6);
    CHECK_CLOSE(38.0, rawMatrix.GetValueAt(3,3), 1e-6);
    CHECK_CLOSE(23.0, rawMatrix.GetValueAt(3,4), 1e-6);
    CHECK_CLOSE(24.0, rawMatrix.GetValueAt(3,5), 1e-6);

    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(4,0), 1e-6);
    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(4,1), 1e-6);
    CHECK_CLOSE(25.0, rawMatrix.GetValueAt(4,2), 1e-6);
    CHECK_CLOSE(26.0, rawMatrix.GetValueAt(4,3), 1e-6);
    CHECK_CLOSE(27.0, rawMatrix.GetValueAt(4,4), 1e-6);
    CHECK_CLOSE(28.0, rawMatrix.GetValueAt(4,5), 1e-6);

    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(5,0), 1e-6);
    CHECK_CLOSE(0.0,  rawMatrix.GetValueAt(5,1), 1e-6);
    CHECK_CLOSE(29.0, rawMatrix.GetValueAt(5,2), 1e-6);
    CHECK_CLOSE(30.0, rawMatrix.GetValueAt(5,3), 1e-6);
    CHECK_CLOSE(31.0, rawMatrix.GetValueAt(5,4), 1e-6);
    CHECK_CLOSE(32.0, rawMatrix.GetValueAt(5,5), 1e-6);

}

TEST(StiffnessMatrixSetZeroDisplacement)
{
    std::vector<double> coords(3);
    coords[0] = 4.0;
    coords[1] = 5.0;
    coords[2] = 6.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);

    PetitPas::Matrix matrix(6, 6);

    //Fill the matrix with non zero values.
    double value = 10.0;
    for(unsigned int rowindex=0; rowindex<6; rowindex++)
    {
        for(unsigned int columnIndex=0; columnIndex<6; columnIndex++)
        {
            matrix.SetValueAt(rowindex, columnIndex, value);
            value += 1.0;
        }
    }

    PetitPas::StiffnessMatrix stiffnessMatrix(matrix, nodeList);

    // Check bad node id.
    CHECK_THROW(stiffnessMatrix.SetZeroDisplacement(42, 0), std::runtime_error);

    // Check bad DOF index.
    CHECK_THROW(stiffnessMatrix.SetZeroDisplacement(2, 3), std::runtime_error);

    // Check normal behavior.
    stiffnessMatrix.SetZeroDisplacement(2, 1);
    const PetitPas::Matrix& zeroMatrix = stiffnessMatrix.GetRawMatrix();
    unsigned int positionIndex = 4; //node index 1 and DOF index 1 => 1*3 + 1 = 4

    // Check column.
    for(unsigned int rowIndex=0; rowIndex<6; rowIndex++)
    {
        if(rowIndex != positionIndex)
        {
            CHECK_CLOSE(0.0, zeroMatrix.GetValueAt(rowIndex, positionIndex), 1e-6);
        }
    }

    // Check row.
    for(unsigned int columnIndex=0; columnIndex<6; columnIndex++)
    {
        if(columnIndex != positionIndex)
        {
            CHECK_CLOSE(0.0, zeroMatrix.GetValueAt(positionIndex, columnIndex), 1e-6);
        }
    }

    // Finally the "1" value.
    CHECK_CLOSE(1.0, zeroMatrix.GetValueAt(positionIndex, positionIndex), 1e-6);

}

TEST(StiffnessMatrixGetSetDiagonalValue)
{
    std::vector<double> coords(3);
    coords[0] = 4.0;
    coords[1] = 5.0;
    coords[2] = 6.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);

    PetitPas::StiffnessMatrix stiffnessMatrix(nodeList);

    // Check bad node id.
    CHECK_THROW(stiffnessMatrix.SetDiagonalValue(42, 0, 0.0), std::runtime_error);
    CHECK_THROW(stiffnessMatrix.GetDiagonalValue(42, 0), std::runtime_error);

    // Check bad DOF index.
    CHECK_THROW(stiffnessMatrix.SetDiagonalValue(2, 3, 0.0), std::runtime_error);
    CHECK_THROW(stiffnessMatrix.GetDiagonalValue(2, 3), std::runtime_error);

    CHECK_CLOSE(0.0, stiffnessMatrix.GetDiagonalValue(2,0), 1e-6);
    stiffnessMatrix.SetDiagonalValue(2, 0, 10.0);
    CHECK_CLOSE(10.0, stiffnessMatrix.GetDiagonalValue(2,0), 1e-6);
}

} //SUITE StiffnessMatrix
