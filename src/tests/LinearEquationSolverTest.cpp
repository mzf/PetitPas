/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/LinearEquationSolver.h"
#include <stdexcept>

SUITE(LinearEquationSolver)
{

TEST(LinearEquationSolverConstructor)
{
    PetitPas::Matrix notSquareMatrix(3,2);
    PetitPas::Matrix squareMatrix(3,3);
    PetitPas::Matrix verticalVector(3,1);
    PetitPas::Matrix incompatibleVerticalVector(2,1);
    CHECK_THROW(PetitPas::LinearEquationSolver(notSquareMatrix, verticalVector), std::runtime_error);
    CHECK_THROW(PetitPas::LinearEquationSolver(squareMatrix, notSquareMatrix), std::runtime_error);
    CHECK_THROW(PetitPas::LinearEquationSolver(squareMatrix, incompatibleVerticalVector), std::runtime_error);
}

TEST(LinearEquationSolverBadSolve)
{
    PetitPas::Matrix squareMatrix(3,3, PetitPas::Matrix::MatrixContainerType_Sparse);
    PetitPas::Matrix verticalVector(3,1);

    // Test a system when line 1 and 2 are linear combination.
    // The solver must not be able to solve it.

    //x+2y+3z=5
    squareMatrix.SetValueAt(0, 0, 1.0);
    squareMatrix.SetValueAt(0, 1, 2.0);
    squareMatrix.SetValueAt(0, 2, 3.0);
    verticalVector.SetValueAt(0, 0, 5.0);

    //2x+4y+6z=10
    squareMatrix.SetValueAt(1, 0, 2.0);
    squareMatrix.SetValueAt(1, 1, 4.0);
    squareMatrix.SetValueAt(1, 2, 6.0);
    verticalVector.SetValueAt(1, 0, 10.0);

    //2x+2y+10z=42
    squareMatrix.SetValueAt(2, 0, 2.0);
    squareMatrix.SetValueAt(2, 1, 2.0);
    squareMatrix.SetValueAt(2, 2, 10.0);
    verticalVector.SetValueAt(2, 0, 42.0);

    PetitPas::LinearEquationSolver solver(squareMatrix, verticalVector);

    CHECK_THROW(solver.GetSolution(), std::runtime_error);

}

TEST(LinearEquationSolverSimpleSolve)
{
    PetitPas::Matrix squareMatrix(3,3, PetitPas::Matrix::MatrixContainerType_Sparse);
    PetitPas::Matrix verticalVector(3,1);

    // Solve a simple 3*3 equation system.

    //x-1y+2z=5
    squareMatrix.SetValueAt(0, 0, 1.0);
    squareMatrix.SetValueAt(0, 1, -1.0);
    squareMatrix.SetValueAt(0, 2, 2.0);
    verticalVector.SetValueAt(0, 0, 5.0);

    //3x+2y+z=10
    squareMatrix.SetValueAt(1, 0, 3.0);
    squareMatrix.SetValueAt(1, 1, 2.0);
    squareMatrix.SetValueAt(1, 2, 1.0);
    verticalVector.SetValueAt(1, 0, 10.0);

    //2x-3y-2z=-10
    squareMatrix.SetValueAt(2, 0, 2.0);
    squareMatrix.SetValueAt(2, 1, -3.0);
    squareMatrix.SetValueAt(2, 2, -2.0);
    verticalVector.SetValueAt(2, 0, -10.0);

    PetitPas::LinearEquationSolver solver(squareMatrix, verticalVector);

    PetitPas::Matrix solution = solver.GetSolution();

    CHECK_EQUAL((unsigned int)3, solution.GetRowCount());
    CHECK_EQUAL((unsigned int)1, solution.GetColumnCount());
    CHECK_CLOSE(1.0, solution.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(2.0, solution.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(3.0, solution.GetValueAt(2,0), 1e-6);

}

} // SUITE LinearEquationSolver
