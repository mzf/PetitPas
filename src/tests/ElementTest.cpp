/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/Element.h"
#include "../core/MaterialBehavior.h"
#include "../core/Matrix.h"
#include "../core/MatrixNotInvertibleException.h"

#include <stdexcept>

SUITE(Element)
{

TEST(ElementConstructor)
{
    //Create a dummy material
    PetitPas::MaterialBehavior material(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    //Check bad number of nodes
    std::vector<PetitPas::Node*> emptyNodes;
    CHECK_THROW(PetitPas::Element badElement(emptyNodes, 0, material), std::runtime_error);

    //Check incompatible node dimensions
    std::vector<double> coords(2);
    PetitPas::Point point1(coords);
    PetitPas::Node node1(point1, 1);

    coords.resize(3);
    PetitPas::Point point2(coords);
    PetitPas::Node node2(point2, 2);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);

    CHECK_THROW(PetitPas::Element badElement(nodes, 0, material), std::runtime_error);

}

TEST(ElementNodeCount)
{
    std::vector<double> coords(2);
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);
    PetitPas::Node node3(point, 3);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);

    //Create a dummy material
    PetitPas::MaterialBehavior material(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    PetitPas::Element element(nodes, 0, material);

    CHECK_EQUAL((unsigned int)3, element.GetNodeCount());
}

TEST(ElementDimension)
{
    std::vector<double> coords(2);
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 1);
    PetitPas::Node node2(point, 2);
    PetitPas::Node node3(point, 3);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);

    //Create a dummy material
    PetitPas::MaterialBehavior material(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    PetitPas::Element element(nodes, 0, material);

    CHECK_EQUAL((unsigned int)2, element.GetDimension());
}

TEST(ElementGetNodes)
{
    std::vector<double> coords(2);
    coords[0] = 1.0;
    coords[1] = 2.0;
    PetitPas::Point point1(coords);
    PetitPas::Node node1(point1, 1);
    coords[0] = 3.0;
    coords[1] = 4.0;
    PetitPas::Point point2(coords);
    PetitPas::Node node2(point2, 2);
    coords[0] = -5.0;
    coords[1] = -6.0;
    PetitPas::Point point3(coords);
    PetitPas::Node node3(point3, 3);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);

    //Create a dummy material
    PetitPas::MaterialBehavior material(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    PetitPas::Element element(nodes, 0, material);

    std::vector<PetitPas::Node*> elementNodes = element.GetNodes();

    CHECK_EQUAL((unsigned int)3, elementNodes.size());

    const std::vector<double>& coordinates1 = elementNodes[0]->GetCoordinates();
    CHECK_CLOSE(1.0, coordinates1[0], 1e-6);
    CHECK_CLOSE(2.0, coordinates1[1], 1e-6);

    const std::vector<double>& coordinates2 = elementNodes[1]->GetCoordinates();
    CHECK_CLOSE(3.0, coordinates2[0], 1e-6);
    CHECK_CLOSE(4.0, coordinates2[1], 1e-6);

    const std::vector<double>& coordinates3 = elementNodes[2]->GetCoordinates();
    CHECK_CLOSE(-5.0, coordinates3[0], 1e-6);
    CHECK_CLOSE(-6.0, coordinates3[1], 1e-6);
}

TEST(ElementStiffnessMatrix2DReferenceTriangle)
{
    //Create the reference triangle in 2D
    std::vector<double> coords1(2);
    coords1[0] = 0.0;
    coords1[1] = 0.0;
    PetitPas::Point point1(coords1);
    PetitPas::Node node1(point1, 1);

    std::vector<double> coords2(2);
    coords2[0] = 1.0;
    coords2[1] = 0.0;
    PetitPas::Point point2(coords2);
    PetitPas::Node node2(point2, 2);

    std::vector<double> coords3(2);
    coords3[0] = 0.0;
    coords3[1] = 1.0;
    PetitPas::Point point3(coords3);
    PetitPas::Node node3(point3, 3);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);

    //Create a simple material with:
    // Young Modulus E = 1
    // Poisson Coefficient v = 0
    //
    // In 2D Plane Stress, the Hooke matrix will be:
    // [ E 0 0       [ 1 0 0
    //   0 E 0     =   0 1 0
    //   0 0 E/2 ]     0 0 1/2 ]
    PetitPas::MaterialBehavior material(1., 0., PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    PetitPas::Element element(nodes, 0, material);

    //Get stiffness matrix for 2D plane stress hypothesis
    PetitPas::Matrix stiffnessMatrix = element.GetRawStiffnessMatrix();

    CHECK_CLOSE(3.0/4.0,  stiffnessMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(1.0/4.0,  stiffnessMatrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(-2.0/4.0, stiffnessMatrix.GetValueAt(0,2), 1e-6);
    CHECK_CLOSE(-1.0/4.0, stiffnessMatrix.GetValueAt(0,3), 1e-6);
    CHECK_CLOSE(-1.0/4.0, stiffnessMatrix.GetValueAt(0,4), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(0,5), 1e-6);

    CHECK_CLOSE(1.0/4.0,  stiffnessMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(3.0/4.0,  stiffnessMatrix.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(1,2), 1e-6);
    CHECK_CLOSE(-1.0/4.0, stiffnessMatrix.GetValueAt(1,3), 1e-6);
    CHECK_CLOSE(-1.0/4.0, stiffnessMatrix.GetValueAt(1,4), 1e-6);
    CHECK_CLOSE(-2.0/4.0, stiffnessMatrix.GetValueAt(1,5), 1e-6);

    CHECK_CLOSE(-2.0/4.0, stiffnessMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(2.0/4.0,  stiffnessMatrix.GetValueAt(2,2), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(2,3), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(2,4), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(2,5), 1e-6);

    CHECK_CLOSE(-1.0/4.0, stiffnessMatrix.GetValueAt(3,0), 1e-6);
    CHECK_CLOSE(-1.0/4.0, stiffnessMatrix.GetValueAt(3,1), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(3,2), 1e-6);
    CHECK_CLOSE(1.0/4.0,  stiffnessMatrix.GetValueAt(3,3), 1e-6);
    CHECK_CLOSE(1.0/4.0,  stiffnessMatrix.GetValueAt(3,4), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(3,5), 1e-6);

    CHECK_CLOSE(-1.0/4.0, stiffnessMatrix.GetValueAt(4,0), 1e-6);
    CHECK_CLOSE(-1.0/4.0, stiffnessMatrix.GetValueAt(4,1), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(4,2), 1e-6);
    CHECK_CLOSE(1.0/4.0,  stiffnessMatrix.GetValueAt(4,3), 1e-6);
    CHECK_CLOSE(1.0/4.0,  stiffnessMatrix.GetValueAt(4,4), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(4,5), 1e-6);

    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(5,0), 1e-6);
    CHECK_CLOSE(-2.0/4.0, stiffnessMatrix.GetValueAt(5,1), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(5,2), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(5,3), 1e-6);
    CHECK_CLOSE(0.0,      stiffnessMatrix.GetValueAt(5,4), 1e-6);
    CHECK_CLOSE(2.0/4.0,  stiffnessMatrix.GetValueAt(5,5), 1e-6);
}

TEST(ElementStiffnessMatrix2DRealTriangle)
{
    //Create the real triangle in 2D
    std::vector<double> coords1(2);
    coords1[0] = 3.0;
    coords1[1] = 0.0;
    PetitPas::Point point1(coords1);
    PetitPas::Node node1(point1, 1);

    std::vector<double> coords2(2);
    coords2[0] = 3.0;
    coords2[1] = 3.0;
    PetitPas::Point point2(coords2);
    PetitPas::Node node2(point2, 2);

    std::vector<double> coords3(2);
    coords3[0] = 0.0;
    coords3[1] = 3.0;
    PetitPas::Point point3(coords3);
    PetitPas::Node node3(point3, 3);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);

    //Create a material: E = 72, v = 0.2 (and G = 30)
    PetitPas::MaterialBehavior material(72., 0.2, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStrain);

    PetitPas::Element element(nodes, 0, material);

    //Get stiffness matrix for 2D plane strain hypothesis
    PetitPas::Matrix stiffnessMatrix = element.GetRawStiffnessMatrix();

    CHECK_CLOSE(15.0,  stiffnessMatrix.GetValueAt(0,0), 1e-6);
    CHECK_CLOSE(0.0,   stiffnessMatrix.GetValueAt(0,1), 1e-6);
    CHECK_CLOSE(-15.0, stiffnessMatrix.GetValueAt(0,2), 1e-6);
    CHECK_CLOSE(-15.0, stiffnessMatrix.GetValueAt(0,3), 1e-6);
    CHECK_CLOSE(0.0,   stiffnessMatrix.GetValueAt(0,4), 1e-6);
    CHECK_CLOSE(15.0,  stiffnessMatrix.GetValueAt(0,5), 1e-6);

    CHECK_CLOSE(0.0,   stiffnessMatrix.GetValueAt(1,0), 1e-6);
    CHECK_CLOSE(40.0,  stiffnessMatrix.GetValueAt(1,1), 1e-6);
    CHECK_CLOSE(-10.0, stiffnessMatrix.GetValueAt(1,2), 1e-6);
    CHECK_CLOSE(-40.0, stiffnessMatrix.GetValueAt(1,3), 1e-6);
    CHECK_CLOSE(10.0,  stiffnessMatrix.GetValueAt(1,4), 1e-6);
    CHECK_CLOSE(0.0,   stiffnessMatrix.GetValueAt(1,5), 1e-6);

    CHECK_CLOSE(-15.0, stiffnessMatrix.GetValueAt(2,0), 1e-6);
    CHECK_CLOSE(-10.0, stiffnessMatrix.GetValueAt(2,1), 1e-6);
    CHECK_CLOSE(55.0,  stiffnessMatrix.GetValueAt(2,2), 1e-6);
    CHECK_CLOSE(25.0,  stiffnessMatrix.GetValueAt(2,3), 1e-6);
    CHECK_CLOSE(-40.0, stiffnessMatrix.GetValueAt(2,4), 1e-6);
    CHECK_CLOSE(-15.0, stiffnessMatrix.GetValueAt(2,5), 1e-6);

    CHECK_CLOSE(-15.0, stiffnessMatrix.GetValueAt(3,0), 1e-6);
    CHECK_CLOSE(-40.0, stiffnessMatrix.GetValueAt(3,1), 1e-6);
    CHECK_CLOSE(25.0,  stiffnessMatrix.GetValueAt(3,2), 1e-6);
    CHECK_CLOSE(55.0,  stiffnessMatrix.GetValueAt(3,3), 1e-6);
    CHECK_CLOSE(-10.0, stiffnessMatrix.GetValueAt(3,4), 1e-6);
    CHECK_CLOSE(-15.0, stiffnessMatrix.GetValueAt(3,5), 1e-6);

    CHECK_CLOSE(0.0,   stiffnessMatrix.GetValueAt(4,0), 1e-6);
    CHECK_CLOSE(10.0,  stiffnessMatrix.GetValueAt(4,1), 1e-6);
    CHECK_CLOSE(-40.0, stiffnessMatrix.GetValueAt(4,2), 1e-6);
    CHECK_CLOSE(-10.0, stiffnessMatrix.GetValueAt(4,3), 1e-6);
    CHECK_CLOSE(40.0,  stiffnessMatrix.GetValueAt(4,4), 1e-6);
    CHECK_CLOSE(0.0,   stiffnessMatrix.GetValueAt(4,5), 1e-6);

    CHECK_CLOSE(15.0,  stiffnessMatrix.GetValueAt(5,0), 1e-6);
    CHECK_CLOSE(0.0,   stiffnessMatrix.GetValueAt(5,1), 1e-6);
    CHECK_CLOSE(-15.0, stiffnessMatrix.GetValueAt(5,2), 1e-6);
    CHECK_CLOSE(-15.0, stiffnessMatrix.GetValueAt(5,3), 1e-6);
    CHECK_CLOSE(0.0,   stiffnessMatrix.GetValueAt(5,4), 1e-6);
    CHECK_CLOSE(15.0,  stiffnessMatrix.GetValueAt(5,5), 1e-6);

}

TEST(ElementGetFieldDerivativeValue)
{
    // Create an element
    std::vector<double> coords1(2);
    coords1[0] = 0.0;
    coords1[1] = 3.0;
    PetitPas::Point point1(coords1);
    PetitPas::Node node1(point1, 1);

    std::vector<double> coords2(2);
    coords2[0] = 3.0;
    coords2[1] = 3.0;
    PetitPas::Point point2(coords2);
    PetitPas::Node node2(point2, 2);

    std::vector<double> coords3(2);
    coords3[0] = 0.0;
    coords3[1] = 6.0;
    PetitPas::Point point3(coords3);
    PetitPas::Node node3(point3, 3);

    std::vector<PetitPas::Node*> nodes;
    nodes.push_back(&node1);
    nodes.push_back(&node2);
    nodes.push_back(&node3);

    //Create a material: E = 72, v = 0.2 (and G = 30)
    PetitPas::MaterialBehavior material(72., 0.2, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStrain);

    PetitPas::Element element(nodes, 0, material);

    //Create a displacement vector
    PetitPas::Matrix displacementVector(6, 1);
    displacementVector.SetValueAt(0, 0, 6.0231);
    displacementVector.SetValueAt(1, 0, -5.8758);
    displacementVector.SetValueAt(2, 0, 4.8456);
    displacementVector.SetValueAt(3, 0, -4.7899);
    displacementVector.SetValueAt(4, 0, 7.3373);
    displacementVector.SetValueAt(5, 0, -7.8315);

    // Get the derivative field for the three nodes.
    for(unsigned int nodeIndex = 0; nodeIndex < 3; nodeIndex++)
    {
        PetitPas::Matrix derivativeMatrix = element.GetFieldDerivativeValue(displacementVector, nodeIndex);

        CHECK_EQUAL((unsigned int)1, derivativeMatrix.GetColumnCount());
        CHECK_EQUAL((unsigned int)3, derivativeMatrix.GetRowCount());

        CHECK_CLOSE(-0.3925, derivativeMatrix.GetValueAt(0, 0), 1e-6);
        CHECK_CLOSE(-0.6519, derivativeMatrix.GetValueAt(1, 0), 1e-6);
        CHECK_CLOSE(0.8000333, derivativeMatrix.GetValueAt(2, 0), 1e-6);
    }

    // Check exceptions.
    CHECK_THROW(element.GetFieldDerivativeValue(displacementVector, 3), std::runtime_error);
    CHECK_THROW(element.GetFieldDerivativeValue(PetitPas::Matrix(6,2), 0), std::runtime_error);
    CHECK_THROW(element.GetFieldDerivativeValue(PetitPas::Matrix(5,1), 0), std::runtime_error);
}

TEST(ElementBadMaterialHypothesis)
{
    // 3D Materials in 2D
    std::vector<double> coords2D(2);
    PetitPas::Point point2D(coords2D);
    PetitPas::Node node1(point2D, 1);
    PetitPas::Node node2(point2D, 2);
    PetitPas::Node node3(point2D, 3);

    std::vector<PetitPas::Node*> nodes2D;
    nodes2D.push_back(&node1);
    nodes2D.push_back(&node2);
    nodes2D.push_back(&node3);

    PetitPas::MaterialBehavior material3D(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_3D);
    CHECK_THROW(PetitPas::Element(nodes2D, 0, material3D), std::runtime_error);

    // 2D Materials in 3D
    std::vector<double> coords3D(3);
    PetitPas::Point point3D(coords3D);
    PetitPas::Node node4(point3D, 4);
    PetitPas::Node node5(point3D, 5);
    PetitPas::Node node6(point3D, 6);
    PetitPas::Node node7(point3D, 7);

    std::vector<PetitPas::Node*> nodes3D;
    nodes3D.push_back(&node4);
    nodes3D.push_back(&node5);
    nodes3D.push_back(&node6);
    nodes3D.push_back(&node7);

    PetitPas::MaterialBehavior material2DPlaneStress(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);
    CHECK_THROW(PetitPas::Element(nodes3D, 0, material2DPlaneStress), std::runtime_error);

    PetitPas::MaterialBehavior material2DPlaneStrain(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStrain);
    CHECK_THROW(PetitPas::Element(nodes3D, 0, material2DPlaneStrain), std::runtime_error);

}

TEST(ElementBadJacobianMatrix)
{
    std::vector<double> coords2D(2);
    PetitPas::Point point2D(coords2D);
    PetitPas::Node node1(point2D, 1);
    PetitPas::Node node2(point2D, 2);
    PetitPas::Node node3(point2D, 3);

    std::vector<PetitPas::Node*> nodes2D;
    nodes2D.push_back(&node1);
    nodes2D.push_back(&node2);
    nodes2D.push_back(&node3);

    PetitPas::MaterialBehavior material(0., 0., PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    unsigned int elementId = 123456;
    std::stringstream elementIdStringStream;
    elementIdStringStream << elementId;
    std::string elementIdString = elementIdStringStream.str();

    PetitPas::Element element(nodes2D, elementId, material);

    // Check that exception is thrown.
    CHECK_THROW(element.GetStiffnessMatrix(), PetitPas::MatrixNotInvertibleException);

    // Check exception content.
    bool isExceptionCatched = false;
    try
    {
        element.GetStiffnessMatrix();
    }
    catch(PetitPas::MatrixNotInvertibleException& e)
    {
        isExceptionCatched = true;
        std::string exceptionMessage(e.what());
        CHECK_EQUAL(true, exceptionMessage.find(elementIdString) != exceptionMessage.npos);
    }

    CHECK_EQUAL(true, isExceptionCatched);

    // Check that exception is thrown.
    PetitPas::Matrix nodalValues(6,1);
    CHECK_THROW(element.GetFieldDerivativeValue(nodalValues, 0), PetitPas::MatrixNotInvertibleException);

    // Check exception content.
    isExceptionCatched = false;
    try
    {
        element.GetFieldDerivativeValue(nodalValues, 0);
    }
    catch(PetitPas::MatrixNotInvertibleException& e)
    {
        isExceptionCatched = true;
        std::string exceptionMessage(e.what());
        CHECK_EQUAL(true, exceptionMessage.find(elementIdString) != exceptionMessage.npos);
    }

    CHECK_EQUAL(true, isExceptionCatched);
}

} // SUITE Element
