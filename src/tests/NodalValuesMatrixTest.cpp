/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/NodalValuesMatrix.h"
#include "../core/Matrix.h"
#include "../core/Node.h"
#include <stdexcept>
#include <algorithm>

SUITE(NodalValuesMatrix)
{

// Fill the given list with 2 nodes in 3D.
void FillNodeList(std::vector<PetitPas::Node*>& nodeList)
{
    std::vector<double> coords(3);
    coords[0] = 4.0;
    coords[1] = 5.0;
    coords[2] = 6.0;
    PetitPas::Point point(coords);
    PetitPas::Node* node1 = new PetitPas::Node(point, 12);
    PetitPas::Node* node2 = new PetitPas::Node(point, 13);
    nodeList.push_back(node1);
    nodeList.push_back(node2);
}

// Free the memory allocated for the nodes in the list.
void CleanNodeList(std::vector<PetitPas::Node*>& nodeList)
{
    for(std::vector<PetitPas::Node*>::iterator nodeIterator = nodeList.begin();
        nodeIterator != nodeList.end();
        ++nodeIterator)
    {
        delete (*nodeIterator);
    }
}

TEST(NodalValuesMatrixConstructor)
{
    std::vector<PetitPas::Node*> emptyNodeList;
    CHECK_THROW(PetitPas::NodalValuesMatrix badMatrix(emptyNodeList), std::runtime_error);

    std::vector<PetitPas::Node*> nodeList;
    FillNodeList(nodeList);

    PetitPas::NodalValuesMatrix nodalValuesMatrix(nodeList);
    PetitPas::Matrix matrix = nodalValuesMatrix.GetRawMatrix();

    CHECK_EQUAL((unsigned int)6, matrix.GetRowCount());
    CHECK_EQUAL((unsigned int)1, matrix.GetColumnCount());

    for(unsigned int rowIndex = 0; rowIndex < 6; rowIndex++)
    {
        CHECK_CLOSE(0.0, matrix.GetValueAt(rowIndex, 0), 1e-6);
    }

    CleanNodeList(nodeList);
}

TEST(NodalValuesMatrixContainsNode)
{
    std::vector<PetitPas::Node*> nodeList;
    FillNodeList(nodeList);

    PetitPas::NodalValuesMatrix nodalValuesMatrix(nodeList);

    CHECK_EQUAL(true, nodalValuesMatrix.ContainsNode(12));
    CHECK_EQUAL(true, nodalValuesMatrix.ContainsNode(13));
    CHECK_EQUAL(false, nodalValuesMatrix.ContainsNode(14));

    CleanNodeList(nodeList);
}

TEST(NodalValuesMatrixGetNodeIds)
{
    std::vector<PetitPas::Node*> nodeList;
    FillNodeList(nodeList);

    PetitPas::NodalValuesMatrix nodalValuesMatrix(nodeList);
    const std::vector<unsigned int>& nodeIds = nodalValuesMatrix.GetNodeIds();

    CHECK_EQUAL((unsigned int)2, nodeIds.size());
    CHECK_EQUAL(true, std::find(nodeIds.begin(), nodeIds.end(), (unsigned int)12) != nodeIds.end());
    CHECK_EQUAL(true, std::find(nodeIds.begin(), nodeIds.end(), (unsigned int)13) != nodeIds.end());

    CleanNodeList(nodeList);
}

TEST(NodalValuesMatrixSetValueGetValue)
{
    std::vector<PetitPas::Node*> nodeList;
    FillNodeList(nodeList);

    PetitPas::NodalValuesMatrix nodalValuesMatrix(nodeList);

    // Check bad node id.
    CHECK_THROW(nodalValuesMatrix.SetValue(42, 0, 0.), std::runtime_error);
    CHECK_THROW(nodalValuesMatrix.GetValue(42, 0), std::runtime_error);

    // Check bad DOF index.
    CHECK_THROW(nodalValuesMatrix.SetValue(12, 3, 0.), std::runtime_error);
    CHECK_THROW(nodalValuesMatrix.GetValue(12, 3), std::runtime_error);

    // Check normal usage.
    nodalValuesMatrix.SetValue(12, 0, 1.0);
    nodalValuesMatrix.SetValue(12, 1, 2.0);
    nodalValuesMatrix.SetValue(12, 2, 3.0);
    nodalValuesMatrix.SetValue(13, 0, 4.0);
    nodalValuesMatrix.SetValue(13, 1, 5.0);
    nodalValuesMatrix.SetValue(13, 2, 6.0);
    CHECK_CLOSE(1.0, nodalValuesMatrix.GetValue(12, 0), 1e-6);
    CHECK_CLOSE(2.0, nodalValuesMatrix.GetValue(12, 1), 1e-6);
    CHECK_CLOSE(3.0, nodalValuesMatrix.GetValue(12, 2), 1e-6);
    CHECK_CLOSE(4.0, nodalValuesMatrix.GetValue(13, 0), 1e-6);
    CHECK_CLOSE(5.0, nodalValuesMatrix.GetValue(13, 1), 1e-6);
    CHECK_CLOSE(6.0, nodalValuesMatrix.GetValue(13, 2), 1e-6);

    CleanNodeList(nodeList);
}

TEST(NodalValuesMatrixConstructorNotVertical)
{
    //Check matrix not square
    PetitPas::Matrix notVerticalMatrix(3,2);
    std::vector<PetitPas::Node*> emptyNodes;
    CHECK_THROW(PetitPas::NodalValuesMatrix(notVerticalMatrix, emptyNodes), std::runtime_error);
}

TEST(NodalValuesMatrixConstructorNodeListEmpty)
{
    //Check node list empty
    PetitPas::Matrix verticalMatrix(4,1);
    std::vector<PetitPas::Node*> emptyNodes;
    CHECK_THROW(PetitPas::NodalValuesMatrix(verticalMatrix, emptyNodes), std::runtime_error);
}

TEST(NodalValuesMatrixConstructorBadDimensions)
{
    PetitPas::Matrix verticalMatrix(4,1);
    std::vector<double> coords(3);
    coords[0] = 4.0;
    coords[1] = 5.0;
    coords[2] = 6.0;
    PetitPas::Point point(coords);
    PetitPas::Node node(point, 123);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node);

    CHECK_THROW(PetitPas::NodalValuesMatrix(verticalMatrix, nodeList), std::runtime_error);
}

TEST(NodalValuesMatrixConstructorWithMatrix)
{
    // 2 nodes in 3D = 6 components
    PetitPas::Matrix verticalMatrix(6,1);
    double value = 10.0;
    for(unsigned int index = 0; index < 6; index++)
    {
        verticalMatrix.SetValueAt(index, 0, value);
        value += 1.0;
    }

    std::vector<double> coords(3);
    coords[0] = 4.0;
    coords[1] = 5.0;
    coords[2] = 6.0;
    PetitPas::Point point(coords);
    PetitPas::Node node1(point, 123);
    PetitPas::Node node2(point, 456);

    std::vector<PetitPas::Node*> nodeList;
    nodeList.push_back(&node1);
    nodeList.push_back(&node2);

    // Create the NodalValuesMatrix.
    PetitPas::NodalValuesMatrix nodalValuesMatrix(verticalMatrix, nodeList);

    // Check the values stored at the nodes.
    CHECK_CLOSE(10.0, nodalValuesMatrix.GetValue(123, 0), 1e-6);
    CHECK_CLOSE(11.0, nodalValuesMatrix.GetValue(123, 1), 1e-6);
    CHECK_CLOSE(12.0, nodalValuesMatrix.GetValue(123, 2), 1e-6);
    CHECK_CLOSE(13.0, nodalValuesMatrix.GetValue(456, 0), 1e-6);
    CHECK_CLOSE(14.0, nodalValuesMatrix.GetValue(456, 1), 1e-6);
    CHECK_CLOSE(15.0, nodalValuesMatrix.GetValue(456, 2), 1e-6);
}

} // SUITE NodalValuesMatrix
