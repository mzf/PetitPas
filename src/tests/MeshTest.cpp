/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "unittest++/UnitTest++.h"

#include "../core/Mesh.h"
#include "../core/MaterialBehavior.h"
#include "../core/Matrix.h"
#include <stdexcept>

//Useful functions


/** \brief Fill the given mesh with 5 nodes and 2 triangle elements.
 *
 * element 1: nodes [1, 2, 3]
 * element 2: nodes [3, 4, 5]
 *
 * \param mesh The Mesh object to fill
 *
 */
void FillSmallMesh(PetitPas::Mesh& mesh)
{
    // Small memory leak in the following line, but it's only in unit tests.
    PetitPas::MaterialBehavior* material = new PetitPas::MaterialBehavior(100.0, 0.3, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    std::vector<double> location(2);

    {
        location[0] = 1.0;
        location[1] = 2.0;
        PetitPas::Point point(location);
        mesh.CreateNode(point,1);
    }
    {
        location[0] = 1.5;
        location[1] = 3.0;
        PetitPas::Point point(location);
        mesh.CreateNode(point,2);
    }
    {
        location[0] = 2.0;
        location[1] = 2.0;
        PetitPas::Point point(location);
        mesh.CreateNode(point,3);
    }
    {
        location[0] = 2.0;
        location[1] = 5.0;
        PetitPas::Point point(location);
        mesh.CreateNode(point,4);
    }
    {
        location[0] = 4.0;
        location[1] = 3.0;
        PetitPas::Point point(location);
        mesh.CreateNode(point,5);
    }

    std::vector<unsigned int> nodeIds(3);
    nodeIds[0] = 1;
    nodeIds[1] = 2;
    nodeIds[2] = 3;
    mesh.CreateElement(nodeIds, *material);

    nodeIds[0] = 3;
    nodeIds[1] = 4;
    nodeIds[2] = 5;
    mesh.CreateElement(nodeIds, *material);
}

SUITE(Mesh)
{

TEST(MeshDimension)
{
    PetitPas::Mesh mesh;

    // Empty mesh have dimension 0.
    CHECK_EQUAL((unsigned int)0, mesh.GetDimension());

    // Check random dimension.
    unsigned int dimension = 5;
    std::vector<double> location(dimension);
    PetitPas::Point point(location);
    mesh.CreateNode(point);
    CHECK_EQUAL(dimension, mesh.GetDimension());
}

TEST(MeshCreateNode)
{
    PetitPas::Mesh mesh;

    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    PetitPas::Node* newNode = mesh.CreateNode(point);

    // New node id must be 1.
    CHECK_EQUAL((unsigned int)1, newNode->GetId());

    //Check new node coordinates.
    std::vector<double> coordinates = newNode->GetCoordinates();
    CHECK_EQUAL((unsigned int)2, coordinates.size());
    CHECK_CLOSE(location[0], coordinates[0], 1e-6);
    CHECK_CLOSE(location[1], coordinates[1], 1e-6);

    // Check error in case of redundant id.
    CHECK_THROW(mesh.CreateNode(point, 1), std::runtime_error);

    // Check error if nodes don't have the same dimension.
    std::vector<double> badLocation(3);
    PetitPas::Point badPoint(badLocation);
    CHECK_THROW(mesh.CreateNode(badPoint), std::runtime_error);

    // Check a node with a given id.
    unsigned int givenId = 42;
    PetitPas::Node* nodeWithId = mesh.CreateNode(point, givenId);
    CHECK_EQUAL(givenId, nodeWithId->GetId());

    // Check add a new node with automatic id
    PetitPas::Node* newNodeAutomaticId = mesh.CreateNode(point);
    CHECK_EQUAL(givenId+1, newNodeAutomaticId->GetId());

    CHECK_EQUAL((unsigned int)3, mesh.GetNodes().size());
}

TEST(MeshGetNode)
{
    PetitPas::Mesh mesh;

    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    mesh.CreateNode(point, 1);
    mesh.CreateNode(point, 2);
    mesh.CreateNode(point, 3);

    CHECK_THROW(mesh.GetNode(4), std::runtime_error);
    CHECK_THROW(mesh.GetNode(0), std::runtime_error);

    PetitPas::Node* node = mesh.GetNode(2);
    CHECK_EQUAL((unsigned int)2, node->GetId());
}

TEST(MeshContainsNode)
{
    PetitPas::Mesh mesh;

    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    mesh.CreateNode(point, 1);
    mesh.CreateNode(point, 2);
    mesh.CreateNode(point, 3);

    CHECK_EQUAL(false, mesh.ContainsNode(0));
    CHECK_EQUAL(true,  mesh.ContainsNode(1));
    CHECK_EQUAL(true,  mesh.ContainsNode(2));
    CHECK_EQUAL(true,  mesh.ContainsNode(3));
    CHECK_EQUAL(false, mesh.ContainsNode(4));

    // Empty mesh
    PetitPas::Mesh emptyMesh;
    CHECK_EQUAL(false, emptyMesh.ContainsNode(0));
}

TEST(MeshGetNodes)
{
    PetitPas::Mesh mesh;

    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    mesh.CreateNode(point, 111);
    mesh.CreateNode(point, 222);
    mesh.CreateNode(point, 333);

    std::vector<PetitPas::Node*> nodes = mesh.GetNodes();

    CHECK_EQUAL((unsigned int)3, nodes.size());
    CHECK_EQUAL((unsigned int)111, nodes[0]->GetId());
    CHECK_EQUAL((unsigned int)222, nodes[1]->GetId());
    CHECK_EQUAL((unsigned int)333, nodes[2]->GetId());
}

TEST(MeshGetNodeIds)
{
    PetitPas::Mesh mesh;

    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    mesh.CreateNode(point, 111);
    mesh.CreateNode(point, 222);
    mesh.CreateNode(point, 333);

    const std::set<unsigned int>& nodeIds = mesh.GetNodeIds();

    CHECK_EQUAL((unsigned int)3, nodeIds.size());
    CHECK_EQUAL(true, nodeIds.find(111) != nodeIds.end());
    CHECK_EQUAL(true, nodeIds.find(222) != nodeIds.end());
    CHECK_EQUAL(true, nodeIds.find(333) != nodeIds.end());
}

TEST(MeshGetMaxNodeId)
{
    PetitPas::Mesh mesh;

    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    mesh.CreateNode(point, 1);
    mesh.CreateNode(point, 2);
    mesh.CreateNode(point, 3);

    CHECK_EQUAL((unsigned int)3, mesh.GetNodeIdMax());

    mesh.CreateNode(point, 10);
    CHECK_EQUAL((unsigned int)10, mesh.GetNodeIdMax());

    mesh.CreateNode(point);
    CHECK_EQUAL((unsigned int)11, mesh.GetNodeIdMax());
}

TEST(MeshCreateElement)
{
    PetitPas::Mesh mesh;
    PetitPas::MaterialBehavior material(100.0, 0.3, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    //Add nodes 1 2 3 to the mesh
    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    mesh.CreateNode(point, 1);
    mesh.CreateNode(point, 2);
    mesh.CreateNode(point, 3);

    //Add triangular element
    std::vector<unsigned int> nodeIds(3);
    nodeIds[0] = 1;
    nodeIds[1] = 2;
    nodeIds[2] = 3;
    PetitPas::Element* newElement = mesh.CreateElement(nodeIds, material);

    CHECK_EQUAL((unsigned int)3, newElement->GetNodeCount());
    CHECK_EQUAL((unsigned int)2, newElement->GetDimension());

    std::vector<PetitPas::Node*> nodes = newElement->GetNodes();
    CHECK_EQUAL((unsigned int)3, nodes.size());
    CHECK_EQUAL((unsigned int)1, nodes[0]->GetId());
    CHECK_EQUAL((unsigned int)2, nodes[1]->GetId());
    CHECK_EQUAL((unsigned int)3, nodes[2]->GetId());

    //Add element with invalid node ids.
    nodeIds[0] = 10;
    nodeIds[1] = 20;
    nodeIds[2] = 30;
    CHECK_THROW(mesh.CreateElement(nodeIds, material), std::runtime_error);
}

TEST(MeshCreateElementWithId)
{
    PetitPas::Mesh mesh;
    PetitPas::MaterialBehavior material(100.0, 0.3, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    //Add nodes 1 2 3 to the mesh
    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    mesh.CreateNode(point, 1);
    mesh.CreateNode(point, 2);
    mesh.CreateNode(point, 3);

    //Add triangular element
    std::vector<unsigned int> nodeIds(3);
    nodeIds[0] = 1;
    nodeIds[1] = 2;
    nodeIds[2] = 3;
    PetitPas::Element* newElement = mesh.CreateElement(nodeIds, material, 42);
    CHECK_EQUAL((unsigned int)42, newElement->GetId());

    //Add another element with automatic numbering.
    PetitPas::Element* newElementAutomatic = mesh.CreateElement(nodeIds, material);
    CHECK_EQUAL((unsigned int)43, newElementAutomatic->GetId());

    //Check error when the id is already in the mesh.
    CHECK_THROW(mesh.CreateElement(nodeIds, material, 43), std::runtime_error);
}

TEST(MeshContainsElement)
{
    PetitPas::Mesh mesh;
    PetitPas::MaterialBehavior material(100.0, 0.3, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    //Add nodes 1 2 3 to the mesh
    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    mesh.CreateNode(point, 1);
    mesh.CreateNode(point, 2);
    mesh.CreateNode(point, 3);

    //Add triangular elements
    std::vector<unsigned int> nodeIds(3);
    nodeIds[0] = 1;
    nodeIds[1] = 2;
    nodeIds[2] = 3;
    mesh.CreateElement(nodeIds, material, 5);
    mesh.CreateElement(nodeIds, material, 6);
    mesh.CreateElement(nodeIds, material, 7);

    CHECK_EQUAL(true, mesh.ContainsElement(5));
    CHECK_EQUAL(true, mesh.ContainsElement(6));
    CHECK_EQUAL(true, mesh.ContainsElement(7));
    CHECK_EQUAL(false, mesh.ContainsElement(8));
    CHECK_EQUAL(false, mesh.ContainsElement(0));
    CHECK_EQUAL(false, mesh.ContainsElement(42));
}

TEST(MeshGetElementIdMax)
{
    PetitPas::Mesh mesh;
    PetitPas::MaterialBehavior material(100.0, 0.3, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    //On empty mesh, the max is 0.
    CHECK_EQUAL((unsigned int)0, mesh.GetElementIdMax());

    //Add nodes 1 2 3 to the mesh
    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    mesh.CreateNode(point, 1);
    mesh.CreateNode(point, 2);
    mesh.CreateNode(point, 3);

    //Add triangular elements
    std::vector<unsigned int> nodeIds(3);
    nodeIds[0] = 1;
    nodeIds[1] = 2;
    nodeIds[2] = 3;
    mesh.CreateElement(nodeIds, material, 5);
    CHECK_EQUAL((unsigned int)5, mesh.GetElementIdMax());

    mesh.CreateElement(nodeIds, material);
    CHECK_EQUAL((unsigned int)6, mesh.GetElementIdMax());

    mesh.CreateElement(nodeIds, material, 42);
    CHECK_EQUAL((unsigned int)42, mesh.GetElementIdMax());

    mesh.CreateElement(nodeIds, material, 11);
    CHECK_EQUAL((unsigned int)42, mesh.GetElementIdMax());
}

TEST(MeshGetElement)
{
    PetitPas::Mesh mesh;
    PetitPas::MaterialBehavior material(100.0, 0.3, PetitPas::MaterialBehavior::MaterialHypothesis_2DPlaneStress);

    //Add nodes 1 2 3 to the mesh
    std::vector<double> location(2);
    location[0] = 1.0;
    location[1] = 2.0;
    PetitPas::Point point(location);
    mesh.CreateNode(point, 1);
    mesh.CreateNode(point, 2);
    mesh.CreateNode(point, 3);

    //Add triangular elements
    std::vector<unsigned int> nodeIds(3);
    nodeIds[0] = 1;
    nodeIds[1] = 2;
    nodeIds[2] = 3;
    mesh.CreateElement(nodeIds, material, 5);
    mesh.CreateElement(nodeIds, material, 6);

    CHECK_EQUAL((unsigned int)5, mesh.GetElement(5)->GetId());
    CHECK_EQUAL((unsigned int)6, mesh.GetElement(6)->GetId());

    CHECK_THROW(mesh.GetElement(42), std::runtime_error);

}

TEST(MeshStiffnessMatrixDimension)
{
    PetitPas::Mesh mesh;
    FillSmallMesh(mesh);

    //Check that stiffness matrix has the right dimension (5 nodes in 2d, 5*2=10).
    PetitPas::Matrix stiffnessMatrix = mesh.GetStiffnessMatrix().GetRawMatrix();
    CHECK_EQUAL(true, stiffnessMatrix.IsSquare());
    CHECK_EQUAL((unsigned int)10, stiffnessMatrix.GetRowCount());
}

}// SUITE Mesh
