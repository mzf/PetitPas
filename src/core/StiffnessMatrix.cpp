/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StiffnessMatrix.h"
#include "Matrix.h"
#include <stdexcept>

namespace PetitPas
{

StiffnessMatrix::StiffnessMatrix(const Matrix& givenRawMatrix, const std::vector<Node*>& nodes)
{
    if(givenRawMatrix.IsSquare() == false)
    {
        throw std::runtime_error("The given matrix is not square.");
    }

    if(nodes.empty())
    {
        throw std::runtime_error("The given node id list is empty.");
    }

    // For now, let's assume that all nodes have the same number of degrees of freedom,
    // and that it is the dimension of the node.
    this->degreesOfFreedomPerNode = nodes[0]->GetDimension();
    if(givenRawMatrix.GetRowCount() != nodes.size() * this->degreesOfFreedomPerNode)
    {
        throw std::runtime_error("The given matrix dimension is incorrect, it must equals the number of freedom degrees of the nodes.");
    }

    // Everything is ok, so we can build our stiffness matrix object.

    this->rawMatrix = new Matrix(givenRawMatrix);

    this->InitData(nodes);
}

StiffnessMatrix::StiffnessMatrix(const std::vector<Node*>& nodes)
{
    if(nodes.empty())
    {
        throw std::runtime_error("The given node id list is empty.");
    }

    // For now, let's assume that all nodes have the same number of degrees of freedom,
    // and that it is the dimension of the node.
    this->degreesOfFreedomPerNode = nodes[0]->GetDimension();

    //Create an empty matrix object.
    unsigned int matrixDimension = this->degreesOfFreedomPerNode * nodes.size();
    this->rawMatrix = new Matrix(matrixDimension, matrixDimension, Matrix::MatrixContainerType_Sparse);

    this->InitData(nodes);
}

void StiffnessMatrix::InitData(const std::vector<Node*>& nodes)
{
    unsigned int index = 0;
    for(std::vector<Node*>::const_iterator nodeIterator = nodes.begin();
        nodeIterator != nodes.end();
        ++nodeIterator)
    {
        unsigned int nodeId = (*nodeIterator)->GetId();

        // Save node ids order.
        this->nodeIds.push_back(nodeId);

        // Compute node index.
        this->nodeIndexes[nodeId] = index * this->degreesOfFreedomPerNode;
        index++;
    }
}

StiffnessMatrix::~StiffnessMatrix()
{
    delete this->rawMatrix;
}

StiffnessMatrix::StiffnessMatrix(const StiffnessMatrix& other):
    nodeIds(other.nodeIds),
    nodeIndexes(other.nodeIndexes),
    degreesOfFreedomPerNode(other.degreesOfFreedomPerNode)
{
    this->rawMatrix = new Matrix(*(other.rawMatrix));
}

StiffnessMatrix& StiffnessMatrix::operator=(const StiffnessMatrix& other)
{
    if (this != &other)
    {
        delete this->rawMatrix;
        this->rawMatrix = new Matrix(*(other.rawMatrix));

        this->nodeIds = other.nodeIds;
        this->nodeIndexes = other.nodeIndexes;
        this->degreesOfFreedomPerNode = other.degreesOfFreedomPerNode;
    }
    return *this;
}

Matrix& StiffnessMatrix::GetRawMatrix() const
{
    return *(this->rawMatrix);
}

bool StiffnessMatrix::ContainsNode(unsigned int nodeId) const
{
    return this->nodeIndexes.count(nodeId) > 0;
}

unsigned int StiffnessMatrix::GetNodeIndex(unsigned int nodeId) const
{
    std::map<unsigned int, unsigned int>::const_iterator nodeIndexIterator = this->nodeIndexes.find(nodeId);
    if(nodeIndexIterator == this->nodeIndexes.end())
    {
        throw std::runtime_error("The stiffness matrix does not contain the given node id.");
    }

    return nodeIndexIterator->second;
}

const std::vector<unsigned int>& StiffnessMatrix::GetNodeIds() const
{
    return this->nodeIds;
}

void StiffnessMatrix::Assemble(const StiffnessMatrix& otherStiffnessMatrix)
{
    const std::vector<unsigned int>& otherNodeIds = otherStiffnessMatrix.GetNodeIds();
    const Matrix& otherMatrix = otherStiffnessMatrix.GetRawMatrix();

    // Double loop on row and column of the "other" matrix.
    for(std::vector<unsigned int>::const_iterator rowNodeIdIterator = otherNodeIds.begin();
        rowNodeIdIterator != otherNodeIds.end();
        ++rowNodeIdIterator)
    {
        if(this->ContainsNode(*rowNodeIdIterator) == false)
        {
            throw std::runtime_error("The stiffness matrix does not contains one of the given node ids.");
        }

        // Get the index in the other matrix and in our matrix.
        unsigned int otherRowNodeIndex = otherStiffnessMatrix.GetNodeIndex(*rowNodeIdIterator);
        unsigned int ourRowNodeIndex = this->GetNodeIndex(*rowNodeIdIterator);

        // Second loop for column.
        for(std::vector<unsigned int>::const_iterator columnNodeIdIterator = otherNodeIds.begin();
            columnNodeIdIterator != otherNodeIds.end();
            ++columnNodeIdIterator)
        {
            unsigned int otherColumnNodeIndex = otherStiffnessMatrix.GetNodeIndex(*columnNodeIdIterator);
            unsigned int ourColumnNodeIndex = this->GetNodeIndex(*columnNodeIdIterator);

            // Add the value to our matrix for each node's degree of freedom.
            for(unsigned int rowDOFOffset = 0; rowDOFOffset < this->degreesOfFreedomPerNode; rowDOFOffset++)
            {
                for(unsigned int columnDOFOffset = 0; columnDOFOffset < this->degreesOfFreedomPerNode; columnDOFOffset++)
                {
                    // Get other stiffness matrix coefficient.
                    unsigned int otherRowIndex = otherRowNodeIndex + rowDOFOffset;
                    unsigned int otherColumnIndex = otherColumnNodeIndex + columnDOFOffset;
                    double otherValue = otherMatrix.GetValueAt(otherRowIndex, otherColumnIndex);

                    // Get our stiffness matrix coefficient.
                    unsigned int ourRowIndex = ourRowNodeIndex + rowDOFOffset;
                    unsigned int ourColumnIndex = ourColumnNodeIndex + columnDOFOffset;

                    // Update our matrix.
                    this->rawMatrix->AddValueAt(ourRowIndex, ourColumnIndex, otherValue);
                }
            }
        }
    }
}

void StiffnessMatrix::SetZeroDisplacement(unsigned int nodeId, unsigned int degreeOfFreedomIndex)
{
    unsigned int positionIndex = this->GetPositionIndex(nodeId, degreeOfFreedomIndex);
    unsigned int matrixDimension = this->rawMatrix->GetRowCount();

    // Zero the column.
    for(unsigned int rowIndex=0; rowIndex < matrixDimension; rowIndex++)
    {
        this->rawMatrix->SetValueAt(rowIndex, positionIndex, 0.0);
    }

    // Zero the row.
    for(unsigned int columnIndex=0; columnIndex < matrixDimension; columnIndex++)
    {
        this->rawMatrix->SetValueAt(positionIndex, columnIndex, 0.0);
    }

    // Set the "1" coefficient.
    this->rawMatrix->SetValueAt(positionIndex, positionIndex, 1.0);

}

void StiffnessMatrix::SetDiagonalValue(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double value)
{
    unsigned int positionIndex = this->GetPositionIndex(nodeId, degreeOfFreedomIndex);

    this->rawMatrix->SetValueAt(positionIndex, positionIndex, value);
}

double StiffnessMatrix::GetDiagonalValue(unsigned int nodeId, unsigned int degreeOfFreedomIndex)
{
    unsigned int positionIndex = this->GetPositionIndex(nodeId, degreeOfFreedomIndex);

    return this->rawMatrix->GetValueAt(positionIndex, positionIndex);
}

void StiffnessMatrix::CheckInput(unsigned int nodeId, unsigned int degreeOfFreedomIndex)
{
    if(this->ContainsNode(nodeId) == false)
    {
        throw std::runtime_error("Unknown node id.");
    }

    if(degreeOfFreedomIndex >= this->degreesOfFreedomPerNode)
    {
        throw std::runtime_error("The given degree of freedom index is out of the bounds.");
    }
}

unsigned int StiffnessMatrix::GetPositionIndex(unsigned int nodeId, unsigned int degreeOfFreedomIndex)
{
    this->CheckInput(nodeId, degreeOfFreedomIndex);
    return GetNodeIndex(nodeId) + degreeOfFreedomIndex;
}

} //namespace PetitPas
