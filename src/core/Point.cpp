/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Point.h"

#include <stdexcept>

namespace PetitPas
{

Point::Point(const std::vector<double> &coords)
{
    if(coords.empty())
    {
        throw std::runtime_error("The coordinates number of the point can not be 0.");
    }

    this->coordinates = coords;
}

const std::vector<double>& Point::GetCoordinates() const
{
    return this->coordinates;
}

unsigned int Point::GetDimension() const
{
    return this->coordinates.size();
}

} //namespace PetitPas
