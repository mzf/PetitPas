/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REFERENCE_ELEMENT_H
#define REFERENCE_ELEMENT_H

#include "Point.h"
#include "Matrix.h"
#include "PolynomialFunction.h"

namespace PetitPas
{
    class Node;

    /** \brief This class represents a reference element.
     *        A mesh element needs a reference element to integrate data.
     */
    class ReferenceElement
    {
    public:
        /** \brief ReferenceElement Constructor.
         *         The basis functions and matrix of basis functions are built in the constructor.
         *
         * \param nodes List of node object of this reference element.
         * \param integrationPoints List of integration points.
         * \param integrationWeights List of weights for each integration points.
         *                           The order is the same as in the \a integrationPoints list.
         *
         * \throw std::runtime_error if \a nodes or \a integrationPoints or \a integrationWeights are empty,
         *                           or if \a integrationPoints and \a integrationWeights don't have the same size.
         */
        ReferenceElement(const std::vector<Node>& nodes, const std::vector<Point>& integrationPoints, const std::vector<double>& integrationWeights);

        /** \brief Copy constructor.
         *
         * \param other The ReferenceElement object that we want to copy the data from.
         *
         */
        ReferenceElement(const ReferenceElement& other);

        /** \brief The assignment operator.
         *
         * \param other The other ReferenceElement object to copy.
         * \return A ReferenceElement reference to this object.
         *
         */
        ReferenceElement& operator= (const ReferenceElement& other);

        /** \brief Get the nodes of the reference element.
         *
         * \return A list of node objects.
         *
         */
        const std::vector<Node>& GetNodes() const;

        /** \brief Get the integration points of this reference element.
         *
         * \return A list of Point objects that represents integration points.
         *         The associated weights are returned by \a GetIntegrationWeighs
         *         in the same order as the integration points.
         *
         */
        const std::vector<Point>& GetIntegrationPoints() const;

        /** \brief Get the integration weight.
         *
         * \return A list of double that represents the weight of the integration points.
         *         The order of the weights are the same as the integration points
         *         returned by \a GetIntegrationPoints.
         *
         */
        const std::vector<double>& GetIntegrationWeights() const;

        /** \brief Get the basis functions of the reference element.
         *
         * \return A list of PolynomialFunction the represents basis functions for each node
         *         of this reference element.
         *
         */
        const std::vector<PolynomialFunction>& GetBasisFunctions() const;

        /** \brief Get the matrix of the basis function derivatives. Useful for Jacobian matrix calculation.
         *
         * \param integrationPointIndex The index of the integration point.
         *        It must be between 0 and integration point number minus one.
         *
         * \return A Matrix that contains the derivatives of the basis functions at the given
         *         integration point index.
         *         The matrix order is:
         *         \code
         *         [ dN1/dx dN2/dx ...
         *           dN1/dy dN2/dy ... ]
         *         \endcode
         *
         * \throw std::runtime_error if \a integrationPointIndex is invalid.
         */
        const Matrix& GetBasisFunctionDerivativeMatrix(unsigned int integrationPointIndex) const;

        /** \brief Get the matrix of the basis function derivatives at a given node.
         *
         * \param nodeIndex The index of the node.
         *        It must be between 0 and node number minus one.
         *
         * \return A Matrix that contains the derivatives of the basis functions at the given
         *         node index.
         *         The matrix order is:
         *         \code
         *         [ dN1/dx dN2/dx ...
         *           dN1/dy dN2/dy ... ]
         *         \endcode
         *
         * \throw std::runtime_error if \a nodeIndex is invalid.
         */
        const Matrix& GetBasisFunctionDerivativeMatrixAtNode(unsigned int nodeIndex) const;

    private:

        // Internal helper functions.
        void CreateBasisFunctions();
        void CreateBasisFunctionMatrices();
        Matrix GetBasisFunctionMatrix(const std::vector<double>& pointCoords) const;
        unsigned int GetDimension() const;

        // Members.
        std::vector<Node> nodes;
        std::vector<Point> integrationPoints;
        std::vector<double> integrationWeights;
        std::vector<PolynomialFunction> basisFunctions;
        std::vector<Matrix> integrationPointBasisFunctionDerivativeMatrices;
        std::vector<Matrix> nodeBasisFunctionDerivativeMatrices;
    };

}

#endif // REFERENCE_ELEMENT_H
