/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ElementIdNodeIdPair.h"

#include <stdexcept>

namespace PetitPas
{
ElementIdNodeIdPair::ElementIdNodeIdPair(unsigned int elementId, unsigned int nodeId):
    elementId(elementId),
    nodeId(nodeId)
{
}

// This operator will be used inside the map 'strainAtElementNode'.
bool ElementIdNodeIdPair::operator< (const ElementIdNodeIdPair& other) const
{
    if(this->elementId < other.elementId)
        return true;

    if(this->elementId > other.elementId)
        return false;

    if(this->nodeId < other.nodeId)
        return true;

    return false;
}

} //namespace PetitPas
