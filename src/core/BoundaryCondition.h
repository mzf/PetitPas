/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BOUNDARY_CONDITION_H
#define BOUNDARY_CONDITION_H

#include <vector>

namespace PetitPas
{

    class StiffnessMatrix;
    class NodalValuesMatrix;

    /** \brief This is an abstract class that represents a boundary condition.
     *         This condition will be applied to a stiffness matrix and a nodal load vector.
     */
    class BoundaryCondition
    {
    public:

        /** \brief BoundaryCondition Constructor.
         *
         */
        BoundaryCondition() {};

        /** \brief BoundaryCondition Destructor. Made virtual because derived class will be deleted by this interface.
         *
         */
        virtual ~BoundaryCondition() {};

        /** \brief Apply this boundary condition to a finite element system.
         *
         * A finite element system is defined by a stiffness matrix (K) and a force vector (F).
         * The boundary condition may modify these two components in this method.
         *
         * \param stiffnessMatrix The StiffnessMatrix object that represents the stiffness matrix of the system.
         * \param nodalLoadVector A NodalValuesMatrix object that represents a vertical vector.
         *
         */
        virtual void ApplyTo(StiffnessMatrix& stiffnessMatrix, NodalValuesMatrix& nodalLoadVector) = 0;

    };

}//namespace PetitPas


#endif // BOUNDARY_CONDITION_H
