/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ANALYSIS_H
#define ANALYSIS_H

#include "StiffnessMatrix.h"
#include "NodalValuesMatrix.h"
#include "Solution.h"
#include <vector>

namespace PetitPas
{
    // Forward declarations
    class Mesh;
    class BoundaryCondition;

    /** \brief The Analysis class that contains Mesh and nodal loads, and that could solve the system.
     */
    class Analysis
    {
    public:
        /** \brief Analysis constructor
         *
         * \param theMesh the associated Mesh for the analysis.
         *
         */
        explicit Analysis(const Mesh& theMesh);

        /** \brief Analysis destructor.
         *
         */
        ~Analysis();

        /** \brief Fix a node's degree of freedom.
         *
         * \param nodeId The id of the concerned node.
         * \param degreeOfFreedomIndex The degree of freedom index of the node, starts at 0.
         *
         * \throw std::runtime_error if the analysis does not contains a node with the given \a nodeId or
         *                           if \a degreeOfFreedomIndex is out of bounds [0, maxDOF-1].
         */
        void SetZeroDisplacement(unsigned int nodeId, unsigned int degreeOfFreedomIndex);

        /** \brief Fix a all degree of freedom of a node.
         *
         * \param nodeId The id of the concerned node.
         *
         * \throw std::runtime_error if the analysis does not contains a node with the given \a nodeId.
         */
        void SetZeroDisplacement(unsigned int nodeId);

        /** \brief Set a displacement at a node's degree of freedom.
         *
         * \param nodeId The id of the concerned node.
         * \param degreeOfFreedomIndex The degree of freedom index of the node, starts at 0.
         * \param displacementValue The value of the displacement.
         *
         * \throw std::runtime_error if the analysis does not contains a node with the given \a nodeId or
         *                           if \a degreeOfFreedomIndex is out of bounds [0, maxDOF-1].
         */
        void SetDisplacement(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double displacementValue);

        /** \brief Set a node's degree of freedom's force.
         *
         * \param nodeId The id of the concerned node.
         * \param degreeOfFreedomIndex The degree of freedom index of the node, starts at 0.
         * \param forceValue The value of the force.
         *
         * \throw std::runtime_error if the analysis does not contains a node with the given \a nodeId or
         *                           if \a degreeOfFreedomIndex is out of bounds [0, maxDOF-1].
         */
        void SetForce(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double forceValue);

        /** \brief Get the result of the simulation.
         * This method launches the resolution of the analysis.
         *
         * \return A Solution object that contains the output of the solve of the analysis.
         *
         */
        Solution GetSolution() const;

    private:

        /**<  throws exceptions if the mesh does not contains the node id or if the dofIndex is invalid.*/
        void CheckNodeDOF(unsigned int nodeId, unsigned int dofIndex);

        const Mesh& mesh;
        std::vector<BoundaryCondition*> boundaryConditions;
    };

}//namespace PetitPas


#endif // ANALYSIS_H

