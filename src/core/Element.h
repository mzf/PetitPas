/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ELEMENT_H
#define ELEMENT_H

#include "StiffnessMatrix.h"
#include "ReferenceElement.h"

namespace PetitPas
{
    class MaterialBehavior;

    /** \brief The element class.
     */
    class Element
    {
    public:
        /** \brief Element constructor.
         *
         * \param elementNodes The list of pointers to the nodes that define this element.
         * \param elementId The id of the element.
         * \param materialBehavior The material associated to this element.
         *
         * \throw std::runtime_error if the \a elementNodes list is empty or
         *        if the dimensions of the nodes are not the same or
         *        if the material behavior hypothesis is not compatible with node's dimensions.
         */
        Element(const std::vector<Node*>& elementNodes, unsigned int elementId, const MaterialBehavior& materialBehavior);

        /** \brief Destructor.
         *
         */
        ~Element();

        /** \brief Get the number of nodes.
         *
         * \return The number of node of this element.
         *
         */
        unsigned int GetNodeCount() const;

        /** \brief Get the nodes of this element.
         *
         * \return The list of pointers to Node objects of the element.
         *
         */
        const std::vector<Node*>& GetNodes() const;

        /** \brief Get the dimension of the element.
         *
         * \return The dimension of the element.
         *
         */
        unsigned int GetDimension() const;

        /** \brief Get the id of the element.
         *
         * \return The associated id of this element.
         *
         */
        unsigned int GetId() const;

        /** \brief Get the material behavior associated with this element.
         *
         * \return A pointer to a MaterialBehavior object.
         *
         */
        const MaterialBehavior* GetMaterialBehavior() const;

        /** \brief Get the Matrix object of the stiffness matrix of this element.
         *
         * \return Matrix object that is the stiffness matrix.
         *
         */
        Matrix GetRawStiffnessMatrix() const;

        /** \brief Get the StiffnessMatrix object of this element.
         *
         * \return The StiffnessMatrix object that represents the stiffness matrix of this element.
         *
         */
        StiffnessMatrix GetStiffnessMatrix() const;

        /** \brief Compute the interpolation of the derivative function according to the given nodal field.
         *         It is useful to compute the deformations of the element from the nodal displacements.
         *         In this case, the input field will be the nodal displacements and the output
         *         will be the deformation at the given node index.
         *
         * \param nodalValues Vertical Matrix that contains the values for each degree of freedom of each node.
         *                    The node order must the same as the list returned by GetNodes().
         * \param targetNodeIndex The index of the node for the calculation. This index must not be greater than
         *                        the number of nodes minus one.
         * \return A vertical Matrix object that represents the interpolation of the derivative basis function with
         *         the given node field.
         *
         * \throw std::runtime_error if \a targetNodeIndex is out of bound or
         *                           if \a nodalValues is not vertical or
         *                           if \a nodalValues dimension is not the total number of degrees of freedom of the element.
         */
        Matrix GetFieldDerivativeValue(const Matrix& nodalValues, unsigned int targetNodeIndex);


    private:

        void CheckMaterialHypothesis();

        Matrix GetNodeCoordinatesMatrix() const;
        Matrix GetJacobianMatrixAtIntegrationPoint(unsigned int integrationPointIndex) const;
        Matrix GetJacobianMatrixAtNode(unsigned int nodeIndex) const;
        Matrix GetInverseJacobianMatrix(const Matrix& jacobianMatrix) const;
        void Fill2DBasisDerivativeFunctionMatrixAtNode(unsigned int nodeIndex,
                                                       const std::vector<PolynomialFunction>& basisFunctions,
                                                       const std::vector<double>& computationPoint,
                                                       const Matrix& inverseJacobianMatrix,
                                                       Matrix& B) const;
        void Fill3DBasisDerivativeFunctionMatrixAtNode(unsigned int nodeIndex,
                                                       const std::vector<PolynomialFunction>& basisFunctions,
                                                       const std::vector<double>& computationPoint,
                                                       const Matrix& inverseJacobianMatrix,
                                                       Matrix& B) const;

        /** Returns Bt*C*B, useful in stiffness matrix computation.
         *
         */
        Matrix GetBTransposedByCByB(const Matrix& B, const Matrix& C) const;

        /** Returns the total number of degrees of freedom for this element.
         *  It is usually node count multiplied by number of unknowns per node.
         */
        unsigned int GetDegreesOfFreedomNumber() const;

        std::vector<Node*> nodes;
        unsigned int id;
        const MaterialBehavior* materialBehavior;
        const ReferenceElement* referenceElement;
    };

}

#endif // ELEMENT_H
