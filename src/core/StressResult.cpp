/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StressResult.h"
#include "Matrix.h"
#include "ResultNotFoundException.h"
#include "Element.h"
#include "StrainResult.h"
#include "MaterialBehavior.h"

namespace PetitPas
{

StressResult::StressResult(const StrainResult& strainResult):
    strainResult(strainResult)
{
    // Compute the stress at elements from the given strain result.

    const std::vector<Element*>& elements = this->strainResult.GetElements();

    for(std::vector<Element*>::const_iterator elementIterator = elements.begin();
        elementIterator != elements.end();
        ++elementIterator)
    {
        Element* currentElement = (*elementIterator);

        // Get the material behavior matrix of this element.
        const MaterialBehavior* elementMaterialBehavior = currentElement->GetMaterialBehavior();

        // For each node, compute the stress as a multiplication of the behavior matrix and the strain matrix.
        const std::vector<Node*>& elementNodes = currentElement->GetNodes();
        for(std::vector<Node*>::const_iterator nodeIterator = elementNodes.begin();
            nodeIterator != elementNodes.end();
            ++nodeIterator)
        {
            Node* currentNode = (*nodeIterator);

            // Get the strain matrix at this element's node.
            const Matrix& strainMatrix = this->strainResult.GetElementalStrainAtNode(currentElement->GetId(), currentNode->GetId());

            // Compute the stress.
            Matrix stressMatrix = elementMaterialBehavior->GetHookeMatrix() * strainMatrix;

            // Store the result.
            ElementIdNodeIdPair elementNodePair(currentElement->GetId(), currentNode->GetId());
            this->stressAtElementNode.insert(std::pair<ElementIdNodeIdPair, Matrix>(elementNodePair, stressMatrix));
        }

        // Save the element.
        this->elements.push_back(currentElement);
    }


}

const Matrix& StressResult::GetElementalStressAtNode(unsigned int elementId, unsigned int nodeId) const
{
    // Create a key <element, node> to get the result.
    ElementIdNodeIdPair elementNodePair(elementId, nodeId);

    std::map<ElementIdNodeIdPair, Matrix>::const_iterator stressIterator = this->stressAtElementNode.find(elementNodePair);
    if(stressIterator == this->stressAtElementNode.end())
    {
        throw ResultNotFoundException(elementId, nodeId);
    }

    return stressIterator->second;
}

const std::vector<Element*>& StressResult::GetElements() const
{
    return this->elements;
}

} //namespace PetitPas
