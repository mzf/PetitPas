/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PolynomialTerm.h"
#include <stdexcept>
#include <cmath>

namespace PetitPas
{

PolynomialTerm::PolynomialTerm(const std::vector<int>& degrees, double coefficient)
{
    if(degrees.empty())
    {
        throw std::runtime_error("The number of degrees can not be 0.");
    }

    this->degrees = degrees;
    this->coefficient = coefficient;
}

double PolynomialTerm::GetValue(const std::vector<double>& variableValues) const
{
    if(this->degrees.size() != variableValues.size())
    {
        throw std::runtime_error("The size of the given variable values is not the same as the term degrees number.");
    }

    double result = this->coefficient;
    for(unsigned int index = 0; index < this->degrees.size(); index++)
    {
        result *= std::pow(variableValues[index], this->degrees[index]);
    }

    return result;

}

double PolynomialTerm::GetDerivativeValue(unsigned int byVariableIndex, const std::vector<double>& variableValues) const
{
    if(this->degrees.size() != variableValues.size())
    {
        throw std::runtime_error("The size of the given variable values is not the same as the term degrees number.");
    }

    if(byVariableIndex >= this->GetDimension())
    {
        throw std::runtime_error("The variable index is out of range.");
    }

    double result = this->coefficient;

    for(unsigned int index = 0; index < this->degrees.size(); index++)
    {
        int degree = this->degrees[index];
        if(index == byVariableIndex)
        {
            //the degree becomes a multiplicative constant (x^2 => 2*x)
            result *= degree;

            // special case: the current variable is a constant (ex: 3*x^0 = 3),
            // so the derivative is zero and we don't need to compute others
            // coefficients. It also avoid the problematic case with pow function
            // when base is 0 and exponent a negative number (ex: pow(0, -1)).
            if(degree == 0)
                continue;

            //the current degree must be decreased
            degree--;
        }
        result *= std::pow(variableValues[index], degree);
    }

    return result;
}

unsigned int PolynomialTerm::GetDimension() const
{
    return this->degrees.size();
}

void PolynomialTerm::SetCoefficient(double value)
{
    this->coefficient = value;
}

} //namespace PetitPas
