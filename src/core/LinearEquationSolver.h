/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LINEAR_EQUATION_SOLVER_H
#define LINEAR_EQUATION_SOLVER_H

#include "Matrix.h"

#include "Eigen/Sparse"

namespace PetitPas
{
    /** \brief This class can solve A*X=B equation system, with X a vector of unknowns.
     *
     * If you want to solve the following system:
     * \code
     * 3*x + 5*y + 10*z = 10
     * 4*x + 6*y + 11*z = 42
     * 5*x + 7*y + 12*z = 0
     * \endcode
     *
     * Then the matrix form will be:
     * \code
     * [ 3 5 10     [ x      [ 10
     *   4 6 11   *   y    =   42
     *   5 7 12 ]     z ]      0  ]
     * \endcode
     *
     * It is equivalent to:
     * \code
     * [A] * [X] = [B]
     * \endcode
     *
     * So you can pass the square Matrix [A] and the vertical Matrix [B] to the constructor of this class,
     * and when you will call the GetSolution() method it will solve the system and return the unknown [X] Matrix.
     */
    class LinearEquationSolver
    {
    public:
        /** \brief Construct a solver for a linear equation system A*X=B,
         *         with A a square matrix and B a vertical vector.
         *
         *         The matrix A and B will be modified during the solve.
         *
         * \param A A square Matrix object that contains the coefficients of the system.
         * \param B A vertical Matrix object that contains the right member of the system.
         *
         * \throw std::runtime_error if A is not square or
         *                           if B is not a vertical vector or
         *                           if A and B dimensions are not the same.
         */
        LinearEquationSolver(Matrix& A, Matrix& B);

        /** \brief Destructor.
         *
         */
        ~LinearEquationSolver();

        /** \brief Solve the linear system and return the solution.
         *
         * \return The solution of the system. It is a vertical Matrix with the same dimension as the input matrices.
         *
         * \throw std::runtime_error if the solver can not resolve the given system.
         */
        const Matrix& GetSolution();

    private:
        Eigen::SparseMatrix<double> systemCoefficients;
        Eigen::VectorXd systemVector;
        Matrix* solution;
        bool isSolved;
    };

}//namespace PetitPas


#endif // LINEAR_EQUATION_SOLVER_H
