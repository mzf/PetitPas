/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Solution.h"
#include "Mesh.h"

#include <stdexcept>

namespace PetitPas
{

Solution::Solution(const Mesh& mesh, const NodalValuesMatrix& nodalValueVector):
    mesh(mesh),
    nodalValueVector(nodalValueVector)
{
    // Check that node values are contained in the mesh.
    const std::vector<unsigned int> vectorNodeIds = this->nodalValueVector.GetNodeIds();
    for(std::vector<unsigned int>::const_iterator nodeIdIterator = vectorNodeIds.begin();
        nodeIdIterator != vectorNodeIds.end();
        ++nodeIdIterator)
    {
        if(this->mesh.ContainsNode(*nodeIdIterator) == false)
        {
            throw std::runtime_error("Some nodes of the nodal displacement vector are not in the given mesh.");
        }
    }
}

double Solution::GetNodalValue(unsigned int nodeId, unsigned int degreeOfFreedomIndex) const
{
    return this->nodalValueVector.GetValue(nodeId, degreeOfFreedomIndex);
}

std::vector<double> Solution::GetNodalValues(unsigned int nodeId) const
{
    Node* node = this->mesh.GetNode(nodeId);

    // For now, the total number of degree of freedom is the dimension of the node.
    unsigned int degreesOfFreedomNumber = node->GetDimension();
    std::vector<double> values(degreesOfFreedomNumber);
    for(unsigned int dofIndex = 0; dofIndex < degreesOfFreedomNumber; dofIndex++)
    {
        values[dofIndex] = this->nodalValueVector.GetValue(nodeId, dofIndex);
    }

    return values;
}

const Mesh& Solution::GetMesh() const
{
    return this->mesh;
}

} //namespace PetitPas
