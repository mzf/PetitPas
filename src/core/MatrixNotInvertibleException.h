/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MATRIX_NOT_INVERTIBLE_EXCEPTION_H
#define MATRIX_NOT_INVERTIBLE_EXCEPTION_H

#include <stdexcept>
#include <string>

namespace PetitPas
{

    /** \brief An exception raised when a matrix is not invertible.
     */
    class MatrixNotInvertibleException : public std::runtime_error
    {
    public:

        /** \brief Constructor
         */
        MatrixNotInvertibleException(const std::string& message) : std::runtime_error(message)
        { }

    };

}//namespace PetitPas


#endif // MATRIX_NOT_INVERTIBLE_EXCEPTION_H
