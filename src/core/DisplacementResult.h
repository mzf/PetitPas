/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DISPLACEMENT_RESULT_H
#define DISPLACEMENT_RESULT_H

#include <vector>
#include <set>

namespace PetitPas
{
    class Solution;

    /** \brief The displacement result of the analysis solution.
     */
    class DisplacementResult
    {
    public:

        /** \brief Constructor from a solution object.
         *
         * \param solution A Solution object from a solved analysis.
         *
         */
        explicit DisplacementResult(const Solution& solution);

        /** \brief Get the nodal displacements.
         *
         * \param nodeId Id of the node that we want the displacement
         * \return A list of double values, where each component is the displacement value of the node for a degree of freedom.
         *         For instance, in structural 2D simulation the list will contains 2 values for the displacements in directions X and Y.
         *
         * \throw std::runtime_error if the result does not contains the given node Id.
         */
        std::vector<double> GetNodalDisplacements(unsigned int nodeId) const;

        /** \brief Get the ids of the node that have a displacement result.
         *
         * \return A list of node id. You can use theses ids with GetNodalDisplacements() to get associated displacement.
         *
         */
        const std::set<unsigned int>& GetNodeIds() const;

    private:
        const Solution& solution;

    };

}//namespace PetitPas


#endif // DISPLACEMENT_RESULT_H
