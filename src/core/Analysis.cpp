/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Analysis.h"
#include "Mesh.h"
#include "MathTools.h"
#include "LinearEquationSolver.h"
#include "DisplacementBoundaryCondition.h"
#include "ZeroDisplacementBoundaryCondition.h"
#include "NodalForceBoundaryCondition.h"

#include <stdexcept>
#include <iostream>

namespace PetitPas
{

Analysis::Analysis(const Mesh& theMesh):
    mesh(theMesh)
{

}

Analysis::~Analysis()
{
    //Free the allocated boundary conditions
    for(std::vector<BoundaryCondition*>::iterator boundaryConditionIterator = this->boundaryConditions.begin();
        boundaryConditionIterator != this->boundaryConditions.end();
        ++boundaryConditionIterator)
    {
        delete (*boundaryConditionIterator);
    }
}

void Analysis::SetZeroDisplacement(unsigned int nodeId, unsigned int degreeOfFreedomIndex)
{
    this->CheckNodeDOF(nodeId, degreeOfFreedomIndex);
    BoundaryCondition* zeroDisplacement = new ZeroDisplacementBoundaryCondition(nodeId, degreeOfFreedomIndex);
    this->boundaryConditions.push_back(zeroDisplacement);
}

void Analysis::SetZeroDisplacement(unsigned int nodeId)
{
    // Loop on degree of freedom's indexes and fix all of them.
    unsigned int dimension = this->mesh.GetDimension();
    for(unsigned int index = 0; index < dimension; index++)
    {
        this->SetZeroDisplacement(nodeId, index);
    }
}

void Analysis::SetDisplacement(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double displacementValue)
{
    // If displacement is too small, use the zero displacement method.
    if(MathTools::IsDoubleClose(0.0, displacementValue, 1e-10))
    {
        this->SetZeroDisplacement(nodeId, degreeOfFreedomIndex);
    }
    else
    {
        this->CheckNodeDOF(nodeId, degreeOfFreedomIndex);
        BoundaryCondition* displacement = new DisplacementBoundaryCondition(nodeId, degreeOfFreedomIndex, displacementValue);
        this->boundaryConditions.push_back(displacement);
    }
}

void Analysis::SetForce(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double forceValue)
{
    this->CheckNodeDOF(nodeId, degreeOfFreedomIndex);
    BoundaryCondition* nodalForce = new NodalForceBoundaryCondition(nodeId, degreeOfFreedomIndex, forceValue);
    this->boundaryConditions.push_back(nodalForce);
}

void Analysis::CheckNodeDOF(unsigned int nodeId, unsigned int DOFIndex)
{
    if(this->mesh.ContainsNode(nodeId) == false)
    {
        throw std::runtime_error("The given node id does not exists in the mesh.");
    }

    //For now, assume that degrees of freedom are node's dimensions.
    if(DOFIndex >= this->mesh.GetNode(nodeId)->GetDimension())
    {
        throw std::runtime_error("Invalid degree of freedom index.");
    }
}

Solution Analysis::GetSolution() const
{
    // Get the stiffness matrix of the mesh and create an empty nodal load vector.
    StiffnessMatrix stiffnessMatrix = this->mesh.GetStiffnessMatrix();
    NodalValuesMatrix nodalLoadVector(this->mesh.GetNodes());

    // Apply boundary condition to these matrices.
    for(std::vector<BoundaryCondition*>::const_iterator boundaryConditionIterator = this->boundaryConditions.begin();
        boundaryConditionIterator != this->boundaryConditions.end();
        ++boundaryConditionIterator)
    {
        (*boundaryConditionIterator)->ApplyTo(stiffnessMatrix, nodalLoadVector);
    }

    // Solve the system.
    // The system is K*U=F with
    //  - K: the stiffness matrix
    //  - F: the nodal force vector
    //  - U: the displacement vector
    // We are looking for U, so U=(K^-1)*F

    PetitPas::LinearEquationSolver solver(stiffnessMatrix.GetRawMatrix(), nodalLoadVector.GetRawMatrix());
    const PetitPas::Matrix& solutionMatrix = solver.GetSolution();

    // Creates the Solution object
    NodalValuesMatrix nodalDisplacementMatrix(solutionMatrix, this->mesh.GetNodes());
    return Solution(this->mesh, nodalDisplacementMatrix);
}

} //namespace PetitPas
