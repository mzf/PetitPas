/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPARSE_MATRIX_ITERATOR_H
#define SPARSE_MATRIX_ITERATOR_H

#include <map>

namespace PetitPas
{
    /** \brief An iterator on the non zero values of a sparse matrix.
     */
    class SparseMatrixIterator
    {
    public:

        /** \brief SparseMatrixIterator constructor
         *
         * \param wrappedIterator An iterator of an internal container of a SparseMatrixContainer.
         *
         */
        explicit SparseMatrixIterator(const std::map<std::pair<unsigned int, unsigned int>, double>::const_iterator& wrappedIterator);


        /** \brief Get the row index of the element pointed by this iterator.
         *         The lowercase name is because of Eigen compatibility.
         *
         * \return The row index of the element pointed by this iterator
         *
         */
        int row() const;

        /** \brief Get the column index of the element pointed by this iterator.
         *         The lowercase name is because of Eigen compatibility.
         *
         * \return The column index of the element pointed by this iterator
         *
         */
        int col() const;

        /** \brief Get the value of the element pointed by this iterator.
         *         The lowercase name is because of Eigen compatibility.
         *
         * \return The value of the element pointed by this iterator
         *
         */
        double value() const;

        /** \brief The not-equal operator that compare the inner iterator.
         *
         * \return true if the two SparseMatrixIterators don't contain the same inner iterator.
         *
         */
        bool operator!=(const SparseMatrixIterator& other) const;

        /** \brief The pre-increment operator.
         *
         * \return true if the two SparseMatrixIterators don't contain the same inner iterator.
         *
         */
        SparseMatrixIterator operator++();

        /** \brief The pointer operator.
         *
         * \return A pointer on this object.
         *
         */
        const SparseMatrixIterator* operator->() const;

    private:

        // Post-increment operator not implemented.
        SparseMatrixIterator operator++(int);

        // Equal operator not implemented.
        bool operator==(const SparseMatrixIterator& other);

        std::map<std::pair<unsigned int, unsigned int>, double>::const_iterator containerIterator;
    };

}//namespace PetitPas


#endif // SPARSE_MATRIX_ITERATOR_H
