/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Matrix.h"
#include "MathTools.h"
#include "MatrixNotInvertibleException.h"
#include "DenseMatrixContainer.h"
#include "SparseMatrixContainer.h"

#include <sstream>

namespace PetitPas
{

Matrix::Matrix(unsigned int rowCount, unsigned int columnCount, MatrixContainerType containerType):
    matrixContainer(NULL)
{
    if((rowCount == 0) || (columnCount == 0))
    {
        throw std::runtime_error("Matrix dimension can not be 0.");
    }

    switch(containerType)
    {
    case MatrixContainerType_Dense:
        this->matrixContainer = new DenseMatrixContainer(rowCount, columnCount);
        break;
    case MatrixContainerType_Sparse:
        this->matrixContainer = new SparseMatrixContainer(rowCount, columnCount);
        break;
    default:
        throw std::runtime_error("Unknown matrix container type.");
    }
}

Matrix::~Matrix()
{
    delete this->matrixContainer;
}

Matrix::Matrix(const Matrix& other)
{
    this->matrixContainer = NULL;
    *this = other;
}

Matrix::Matrix(const MatrixContainer& newMatrixContainer)
{
    if((newMatrixContainer.GetRowCount() == 0) || (newMatrixContainer.GetColumnCount() == 0))
    {
        throw std::runtime_error("Matrix dimension can not be 0.");
    }

    MatrixContainer* matrixContainerUnConst = const_cast<MatrixContainer*>(&newMatrixContainer);
    DenseMatrixContainer* denseMatrixContainer = dynamic_cast<DenseMatrixContainer*>(matrixContainerUnConst);
    if(denseMatrixContainer != NULL)
    {
        this->matrixContainer = new DenseMatrixContainer(*denseMatrixContainer);
    }
    else
    {
        throw std::runtime_error("Matrix constructor with sparse container not implemented!");
    }
}

std::string Matrix::GetStringRepresentation() const
{
    std::stringstream s;

    unsigned int columnCount = this->GetColumnCount();
    unsigned int rowCount = this->GetRowCount();

    s << "[ ";

    for(unsigned int rowIndex=0; rowIndex < rowCount; rowIndex++)
    {
        for(unsigned int columnIndex=0; columnIndex < columnCount; columnIndex++)
        {
            if(columnIndex != 0)
            {
                s << "\t";
            }
            else if (rowIndex != 0)
            {
                s << "  ";
            }
            s << this->GetValueAt(rowIndex, columnIndex);
        }

        if(rowIndex < rowCount-1)
        {
            s << std::endl;
        }
    }

    s << " ]" << std::endl;

    return s.str();
}

Matrix& Matrix::operator=(const Matrix& other)
{
    if (this != &other) // protect against invalid self-assignment
    {
        delete this->matrixContainer;

        DenseMatrixContainer* denseMatrixContainer = dynamic_cast<DenseMatrixContainer*>(other.matrixContainer);
        if(denseMatrixContainer != NULL)
        {
            this->matrixContainer = new DenseMatrixContainer(*(denseMatrixContainer));
        }
        else
        {
            SparseMatrixContainer* sparseMatrixContainer = dynamic_cast<SparseMatrixContainer*>(other.matrixContainer);
            if(sparseMatrixContainer != NULL)
            {
                this->matrixContainer = new SparseMatrixContainer(*(sparseMatrixContainer));
            }
            else
            {
                throw std::runtime_error("Can not find the type of the matrix container.");
            }
        }

    }

    return *this;
}

Matrix Matrix::operator+(const Matrix &other) const
{
    unsigned int columnCount = this->GetColumnCount();
    unsigned int rowCount = this->GetRowCount();

    if( (other.GetRowCount() != rowCount) ||
        (other.GetColumnCount() != columnCount) )
    {
        throw std::runtime_error("Incompatible dimensions in + operator");
    }

    Matrix resultMatrix(rowCount, columnCount);
    for(unsigned int rowIndex=0; rowIndex<rowCount; rowIndex++)
    {
        for(unsigned int columnIndex=0; columnIndex<columnCount; columnIndex++)
        {
            double value = this->GetValueAt(rowIndex, columnIndex) + other.GetValueAt(rowIndex, columnIndex);
            resultMatrix.SetValueAt(rowIndex, columnIndex, value);
        }
    }

    return resultMatrix;
}

Matrix Matrix::operator-(const Matrix &other) const
{
    if( (other.GetRowCount() != this->GetRowCount()) ||
        (other.GetColumnCount() != this->GetColumnCount()) )
    {
        throw std::runtime_error("Incompatible dimensions in - operator");
    }

    Matrix resultMatrix = other;
    resultMatrix.MultiplyBy(-1.0);
    resultMatrix = resultMatrix + *this;

    return resultMatrix;
}

Matrix Matrix::operator*(const Matrix &other) const
{
    unsigned int columnCount = this->GetColumnCount();
    unsigned int rowCount = this->GetRowCount();

    if( (columnCount != other.GetRowCount()) )
    {
        throw std::runtime_error("Incompatible dimensions in * operator");
    }

    Matrix resultMatrix(rowCount, other.GetColumnCount());

    for(unsigned int rowIndex = 0; rowIndex < rowCount; rowIndex++)
    {
        for(unsigned int columnIndex = 0; columnIndex < other.GetColumnCount(); columnIndex++)
        {
            double value = 0.0;
            for(unsigned int index = 0; index < columnCount; index++)
            {
                value += this->GetValueAt(rowIndex, index) * other.GetValueAt(index, columnIndex);
            }
            resultMatrix.SetValueAt(rowIndex, columnIndex, value);
        }
    }

    return resultMatrix;
}

bool Matrix::operator==(const Matrix& other) const
{
    // Check dimensions.
    if((this->GetRowCount() != other.GetRowCount()) ||
       (this->GetColumnCount() != other.GetColumnCount()) )
    {
        return false;
    }

    // Check data.
    for(unsigned int rowIndex = 0; rowIndex < this->GetRowCount(); rowIndex++)
    {
        for(unsigned int columnIndex = 0; columnIndex < this->GetColumnCount(); columnIndex++)
        {
            if(MathTools::IsDoubleClose(this->GetValueAt(rowIndex, columnIndex), other.GetValueAt(rowIndex, columnIndex), 1e-10) == false)
                return false;
        }
    }

    // Everything is identical.
    return true;
}


unsigned int Matrix::GetRowCount() const
{
    return this->matrixContainer->GetRowCount();
}

unsigned int Matrix::GetColumnCount() const
{
    return this->matrixContainer->GetColumnCount();
}

double Matrix::GetValueAt(unsigned int rowIndex, unsigned int columnIndex) const
{
    return this->matrixContainer->GetValueAt(rowIndex, columnIndex);
}

void Matrix::SetValueAt(unsigned int rowIndex, unsigned int columnIndex, double value)
{
    this->matrixContainer->SetValueAt(rowIndex, columnIndex, value);
}

Matrix Matrix::GetTranspose() const
{
    Matrix transposeMatrix(this->GetColumnCount(), this->GetRowCount());

    for(unsigned int rowIndex = 0; rowIndex < this->GetRowCount(); rowIndex++)
    {
        for(unsigned int columnIndex = 0; columnIndex < this->GetColumnCount(); columnIndex++)
        {
            transposeMatrix.SetValueAt(columnIndex, rowIndex, this->GetValueAt(rowIndex, columnIndex));
        }
    }

    return transposeMatrix;
}

double Matrix::GetDeterminant() const
{
    if(this->IsSquare() == false)
    {
        throw std::runtime_error("The matrix must be square to compute the determinant.");
    }

    unsigned int columnCount = this->GetColumnCount();
    unsigned int rowCount = this->GetRowCount();

    if(rowCount == 1)
    {
        return this->GetValueAt(0,0);
    }
    else if(rowCount == 2)
    {
        return (this->GetValueAt(0,0) * this->GetValueAt(1,1)) - (this->GetValueAt(0,1) * this->GetValueAt(1,0));
    }
    else
    {
        double result = 0.;
        for(unsigned int columnIndex = 0; columnIndex < columnCount; columnIndex++)
        {
            //Compute the column factor.
            double factor = columnIndex%2 == 0 ? 1.0 : -1.0;

            //Build the sub-matrix.
            Matrix subMatrix(rowCount-1, columnCount-1);
            for(unsigned int originalColumnIndex = 0; originalColumnIndex < columnCount; originalColumnIndex++)
            {
                //row index starts at 1 because we use the first row as coefficients.
                for(unsigned int originalRowIndex = 1; originalRowIndex < rowCount; originalRowIndex++)
                {
                    unsigned int subRowIndex = originalRowIndex - 1;
                    unsigned int subColumnIndex;
                    if(originalColumnIndex < columnIndex )
                    {
                        subColumnIndex = originalColumnIndex;
                    }
                    else if(originalColumnIndex == columnIndex)
                    {
                        //discard the current column
                        continue;
                    }
                    else //originalColumnIndex > columnIndex
                    {
                        subColumnIndex = originalColumnIndex - 1;
                    }

                    subMatrix.SetValueAt(subRowIndex, subColumnIndex, this->GetValueAt(originalRowIndex, originalColumnIndex));
                }
            }

            //Add the sub result.
            result += factor * this->GetValueAt(0, columnIndex) * subMatrix.GetDeterminant();
        }
        return result;
    }

    return 0.;
}

Matrix Matrix::GetCofactorMatrix() const
{
    if(this->IsSquare() == false)
    {
        throw std::runtime_error("The matrix must be square to compute the cofactors.");
    }

    unsigned int columnCount = this->GetColumnCount();
    unsigned int rowCount = this->GetRowCount();
    Matrix cofactorMatrix(rowCount, columnCount);

    for(unsigned int rowIndex = 0; rowIndex < rowCount; rowIndex++)
    {
        for(unsigned int columnIndex = 0; columnIndex < columnCount; columnIndex++)
        {
            //Build the sub-matrix.
            Matrix subMatrix(rowCount-1, columnCount-1);
            for(unsigned int originalColumnIndex = 0; originalColumnIndex<columnCount; originalColumnIndex++)
            {
                //row index starts at 1 because we use the first row as coefficients.
                for(unsigned int originalRowIndex = 0; originalRowIndex<rowCount; originalRowIndex++)
                {
                    if((originalColumnIndex == columnIndex) ||(originalRowIndex == rowIndex))
                    {
                        //Discard the current row and column
                        continue;
                    }

                    unsigned int subColumnIndex;
                    if(originalColumnIndex < columnIndex)
                    {
                        subColumnIndex = originalColumnIndex;
                    }
                    else //originalColumnIndex > columnIndex
                    {
                        subColumnIndex = originalColumnIndex - 1;
                    }

                    unsigned int subRowIndex;
                    if(originalRowIndex < rowIndex)
                    {
                        subRowIndex = originalRowIndex;
                    }
                    else //originalRowIndex > rowIndex
                    {
                        subRowIndex = originalRowIndex - 1;
                    }

                    subMatrix.SetValueAt(subRowIndex, subColumnIndex, this->GetValueAt(originalRowIndex, originalColumnIndex));
                }
            }

            //Get the factor value
            double factor = (columnIndex + rowIndex) % 2 == 0 ? 1.0 : -1.0;

            //Compute the cofactor
            cofactorMatrix.SetValueAt(rowIndex, columnIndex, factor * subMatrix.GetDeterminant());
        }
    }

    return cofactorMatrix;
}

bool Matrix::IsSquare() const
{
    return (this->GetRowCount() == this->GetColumnCount());
}

Matrix Matrix::GetInverse() const
{
    if(this->IsSquare() == false)
    {
        throw std::runtime_error("The matrix must be square to compute the inverse.");
    }

    double determinant = this->GetDeterminant();
    if(MathTools::IsDoubleClose(0.0, determinant, 1e-12))
    {
        throw MatrixNotInvertibleException("The matrix is not invertible.");
    }

    DenseMatrixContainer* denseMatrixContainer = dynamic_cast<DenseMatrixContainer*>(this->matrixContainer);
    if(denseMatrixContainer != NULL)
    {
        return Matrix(denseMatrixContainer->GetInverse());
    }
    else
    {
        //Inverse is cofactors matrix transposed and divided by determinant

        Matrix cofactorsMatrix = this->GetCofactorMatrix();

        Matrix inverseMatrix = cofactorsMatrix.GetTranspose();
        inverseMatrix.MultiplyBy(1.0/determinant);
        return inverseMatrix;
    }

}

void Matrix::MultiplyBy(double value)
{
    unsigned int columnCount = this->GetColumnCount();
    unsigned int rowCount = this->GetRowCount();

    for(unsigned int rowIndex = 0; rowIndex < rowCount; rowIndex++)
    {
        for(unsigned int columnIndex = 0; columnIndex < columnCount; columnIndex++)
        {
            double originalValue = this->matrixContainer->GetValueAt(rowIndex, columnIndex);
            this->matrixContainer->SetValueAt(rowIndex, columnIndex, originalValue * value);
        }
    }
}

void Matrix::AddValueAt(unsigned int rowIndex, unsigned int columnIndex, double value)
{
    this->matrixContainer->AddValueAt(rowIndex, columnIndex, value);
}

void Matrix::MultiplyValueAt(unsigned int rowIndex, unsigned int columnIndex, double value)
{
    this->matrixContainer->MultiplyValueAt(rowIndex, columnIndex, value);
}

void Matrix::SwapRows(unsigned int firstRowIndex, unsigned int secondRowIndex)
{
    unsigned int columnCount = this->GetColumnCount();
    unsigned int rowCount = this->GetRowCount();

    if((firstRowIndex >= rowCount) ||
       (secondRowIndex >= rowCount) )
    {
        throw std::runtime_error("Invalid row index.");
    }

    if (firstRowIndex == secondRowIndex)
    {
        //same rows, nothing to do.
        return;
    }

    // Swap the data.
    for(unsigned int columnIndex=0; columnIndex < columnCount; columnIndex++)
    {
        double temp = this->GetValueAt(firstRowIndex, columnIndex);
        this->SetValueAt(firstRowIndex, columnIndex, this->GetValueAt(secondRowIndex, columnIndex));
        this->SetValueAt(secondRowIndex, columnIndex, temp);
    }
}

void Matrix::SwapColumns(unsigned int firstColumnIndex, unsigned int secondColumnIndex)
{
    unsigned int columnCount = this->GetColumnCount();
    unsigned int rowCount = this->GetRowCount();

    if((firstColumnIndex >= columnCount) ||
       (secondColumnIndex >= columnCount) )
    {
        throw std::runtime_error("Invalid column index.");
    }

    if (firstColumnIndex == secondColumnIndex)
    {
        //same columns, nothing to do.
        return;
    }

    // Swap the data.
    for(unsigned int rowIndex=0; rowIndex < rowCount; rowIndex++)
    {
        double temp = this->GetValueAt(rowIndex, firstColumnIndex);
        this->SetValueAt(rowIndex, firstColumnIndex, this->GetValueAt(rowIndex, secondColumnIndex));
        this->SetValueAt(rowIndex, secondColumnIndex, temp);
    }
}

MatrixContainer* Matrix::GetContainer() const
{
    return this->matrixContainer;
}

} //namespace PetitPas
