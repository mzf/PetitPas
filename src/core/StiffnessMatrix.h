/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STIFFNESS_MATRIX_H
#define STIFFNESS_MATRIX_H

#include "Node.h"
#include <map>

namespace PetitPas
{
    class Mesh;
    class Matrix;

    /** \brief Represents the stiffness matrix of a group of nodes.
     *
     * This class handles the connectivity between the "raw" matrix and the node's degree of freedom.
     */
    class StiffnessMatrix
    {
    public:

        /** \brief StiffnessMatrix constructor.
         *
         * \param rawMatrix The raw matrix. It must be square and the dimension must be the total of nodes' degrees of freedom.
         *                  This class will keep a copy of this matrix so the original could be safely deleted.
         * \param nodes List of pointers to the nodes of the matrix.
         *              The order of the list must be identical to the node coefficients.
         *
         * \throw std::runtime_error if \a rawMatrix is not square or
         *                           if its dimension is not equal to the total nodes' degrees of freedom count or
         *                           if the list \a nodes is empty.
         */
        StiffnessMatrix(const Matrix& rawMatrix, const std::vector<Node*>& nodes);

        /** \brief Light constructor that initialize a Matrix object according to the node list.
         *
         * \param nodes List of pointers to the nodes of the matrix.
         *              The order of the list must be identical to the node coefficients.
         *
         * \throw std::runtime_error if the list \a nodes is empty.
         */
        explicit StiffnessMatrix(const std::vector<Node*>& nodes);

        /** \brief Copy constructor.
         *
         * \param other The other StiffnessMatrix to copy data from.
         *
         */
        StiffnessMatrix(const StiffnessMatrix& other);

        /** \brief Assignment operator
         *
         * \param other The other StiffnessMatrix to copy data from.
         * \return A reference to this object.
         *
         */
        StiffnessMatrix& operator=(const StiffnessMatrix& other);

        /** \brief StiffnessMatrix destructor.
         *
         *
         */
        ~StiffnessMatrix();

        /** \brief Assemble an other StiffnessMatrix to this StiffnessMatrix.
         *
         * This method is useful to assemble a Mesh stiffness matrix.
         *
         * \param otherStiffnessMatrix The other StiffnessMatrix object that will be added to this StiffnessMatrix.
         *                             The node ids of \a otherStiffnessMatrix must be contained by this StiffnessMatrix.
         *
         * \throw std::runtime_error if one of the node ids of \a otherStiffnessMatrix are not contained in this StiffnessMatrix.
         */
        void Assemble(const StiffnessMatrix& otherStiffnessMatrix);

        /** \brief Get the raw matrix of this StiffnessMatrix.
         *
         * \return The raw Matrix object of this StiffnessMatrix.
         *
         */
        Matrix& GetRawMatrix() const;

        /** \brief Get the index of the node in this StiffnessMatrix.
         *
         * \param nodeId The id of the node that we want the index.
         * \return The index of the first row/column of the freedom degrees of the node with the given \a nodeId.
         *
         * \throw std::runtime_error if the StiffnessMatrix does not contains the given \a nodeId.
         */
        unsigned int GetNodeIndex(unsigned int nodeId) const;

        /** \brief Get the list of node ids.
         *
         * \return The list of the node ids that this stiffness matrix contains.
         *
         */
        const std::vector<unsigned int>& GetNodeIds() const;

        /** \brief Check if the StiffnessMatrix contains a specific node.
         *
         * \param nodeId The id of the node we want to check.
         * \return true if one of the coefficient of the stiffness matrix is relative to the given \a nodeId,
         *         false otherwise.
         *
         */
        bool ContainsNode(unsigned int nodeId) const;

        /** \brief Fix the stiffness value of a node degree of freedom.
         *
         * This method will put a 1 in the diagonal coefficient of the node and fill
         * the vertical and horizontal values with zeros.
         * Example for the coefficient at third column, second row:
         * \code
         * [ 1 2 3      [ 1 2 0
         *   4 5 6   =>   0 0 1
         *   7 8 9 ]      7 8 0 ]
         * \endcode
         *
         * \param nodeId the corresponding node id for the coefficient.
         * \param degreeOfFreedomIndex the index of the degree of freedom.
         *
         * \throw std::runtime_error if the \a nodeId is unknown or
         *                           if \a degreeOfFreedomIndex is out of bounds.
         */
        void SetZeroDisplacement(unsigned int nodeId, unsigned int degreeOfFreedomIndex);

        /** \brief Set a value at a specific diagonal coefficient.
         *
         * The diagonal coefficient is determined by the given \a nodeId and \a degreeOfFreedomIndex.
         *
         * \param nodeId the corresponding node id for the coefficient.
         * \param degreeOfFreedomIndex the index of the degree of freedom.
         * \param value the new value of the diagonal coefficient.
         *
         * \throw std::runtime_error if the \a nodeId is unknown or
         *                           if \a degreeOfFreedomIndex is out of bounds.
         */
        void SetDiagonalValue(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double value);

        /** \brief Get the value of a specific diagonal coefficient.
         *
         * The diagonal coefficient is determined by the given \a nodeId and \a degreeOfFreedomIndex.
         *
         * \param nodeId the corresponding node id for the coefficient.
         * \param degreeOfFreedomIndex the index of the degree of freedom.
         * \return the value of the associated diagonal coefficient corresponding of the given node's degree of freedom.
         *
         * \throw std::runtime_error if the \a nodeId is unknown or
         *                           if \a degreeOfFreedomIndex is out of bounds.
         */
        double GetDiagonalValue(unsigned int nodeId, unsigned int degreeOfFreedomIndex);

    private:

        void InitData(const std::vector<Node*>& nodes);

        /**< Throws exception if the matrix does not contains the node or the DOF.*/
        void CheckInput(unsigned int nodeId, unsigned int degreeOfFreedomIndex);
        unsigned int GetPositionIndex(unsigned int nodeId, unsigned int degreeOfFreedomIndex);

        Matrix* rawMatrix;
        std::vector<unsigned int> nodeIds;
        std::map<unsigned int, unsigned int> nodeIndexes;
        unsigned int degreesOfFreedomPerNode;

    };

}//namespace PetitPas


#endif // STIFFNESS_MATRIX_H
