/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Mesh.h"
#include "StiffnessMatrix.h"

#include <stdexcept>
#include <sstream>

namespace PetitPas
{

Mesh::Mesh()
{
}

Mesh::~Mesh()
{
    // Free the memory allocated for the nodes.
    for(std::vector<Node*>::iterator nodeIterator = this->nodes.begin();
        nodeIterator != this->nodes.end();
        ++nodeIterator)
    {
        delete (*nodeIterator);
    }

    // Free the memory allocated for the elements.
    for(std::vector<Element*>::iterator elementIterator = this->elements.begin();
        elementIterator != this->elements.end();
        ++elementIterator)
    {
        delete (*elementIterator);
    }
}

Node* Mesh::CreateNode(const Point& nodeLocation, unsigned int nodeId)
{
    // Check dimension.
    if(this->nodes.empty() == false)
    {
        if(this->GetDimension() != nodeLocation.GetDimension())
        {
            throw std::runtime_error("The dimension of the node is not the same of the mesh.");
        }
    }

    // Check node id uniqueness.
    if(this->ContainsNode(nodeId))
    {
        throw std::runtime_error("A node with the same id is already present in the mesh.");
    }

    // Generate new node Id if needed.
    if(nodeId == 0)
    {
        nodeId = this->GetNodeIdMax() + 1;
    }

    Node* newNode = new Node(nodeLocation, nodeId);
    this->nodes.push_back(newNode);
    this->nodeIds.insert(nodeId);
    this->nodeFromId.insert(std::pair<unsigned int, Node*>(nodeId, newNode));

    return this->nodes.back();
}

unsigned int Mesh::GetDimension() const
{
    if(this->nodes.empty())
    {
        return 0;
    }

    return this->nodes[0]->GetDimension();
}

bool Mesh::ContainsNode(unsigned int nodeId) const
{
    return this->nodeIds.find(nodeId) != this->nodeIds.end();
}

Node* Mesh::GetNode(unsigned int nodeId) const
{
    std::map<unsigned int, Node*>::const_iterator nodeIterator = this->nodeFromId.find(nodeId);
    if(nodeIterator == this->nodeFromId.end())
    {
        std::stringstream message;
        message << "The node with id=" << nodeId;
        message << " not found in the mesh.";
        throw std::runtime_error(message.str());
    }

    return nodeIterator->second;
}

const std::vector<Node*>& Mesh::GetNodes() const
{
    return this->nodes;
}

const std::set<unsigned int>& Mesh::GetNodeIds() const
{
    return this->nodeIds;
}

unsigned int Mesh::GetNodeIdMax() const
{
    if(this->nodeIds.empty())
        return 0;

    // the member "nodeIds" is a set so the max element is at the end.
    return *this->nodeIds.rbegin();
}

unsigned int Mesh::GetElementIdMax() const
{
    if(this->elementIds.empty())
        return 0;

    // the member "nodeIds" is a set so the max element is at the end.
    return *this->elementIds.rbegin();
}

bool Mesh::ContainsElement(unsigned int elementId) const
{
    return this->elementIds.find(elementId) != this->elementIds.end();
}

Element* Mesh::CreateElement(const std::vector<unsigned int>& elementNodeIds, const MaterialBehavior& materialBehavior, unsigned int elementId)
{
    std::vector<Node*> elementNodes;
    elementNodes.reserve(elementNodeIds.size());
    for(std::vector<unsigned int>::const_iterator nodeIdIterator = elementNodeIds.begin();
        nodeIdIterator != elementNodeIds.end();
        ++nodeIdIterator)
    {
        unsigned int nodeId = *nodeIdIterator;
        elementNodes.push_back(this->GetNode(nodeId));
    }

    if(this->ContainsElement(elementId))
    {
        throw std::runtime_error("The mesh already contains the given element Id.");
    }

    if(elementId == 0)
    {
        // Create a new element id.
        elementId = this->GetElementIdMax() + 1;
    }

    Element* newElement = new Element(elementNodes, elementId, materialBehavior);
    this->elements.push_back(newElement);
    this->elementIds.insert(elementId);
    this->elementFromId.insert(std::pair<unsigned int, Element*>(elementId, newElement));

    return newElement;
}

Element* Mesh::GetElement(unsigned int elementId) const
{
    std::map<unsigned int, Element*>::const_iterator elementIterator = this->elementFromId.find(elementId);
    if(elementIterator == this->elementFromId.end())
    {
        std::stringstream message;
        message << "The element with id=" << elementId;
        message << " not found in the mesh.";
        throw std::runtime_error(message.str());
    }

    return elementIterator->second;
}

StiffnessMatrix Mesh::GetStiffnessMatrix() const
{
    // Create a StiffnessMatrix object to handle the computation.
    StiffnessMatrix meshStiffnessMatrix(this->nodes);

    // For each element in the mesh, we add the associated StiffnessMatrix object
    // to our StiffnessMatrix.
    for(std::vector<Element*>::const_iterator elementIterator = this->elements.begin();
        elementIterator != this->elements.end();
        ++elementIterator)
    {
        meshStiffnessMatrix.Assemble((*elementIterator)->GetStiffnessMatrix());
    }

    return meshStiffnessMatrix;
}

const std::vector<Element*>& Mesh::GetElements() const
{
    return this->elements;
}

} //namespace PetitPas
