/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SparseMatrixContainer.h"
#include "MathTools.h"

#include <stdexcept>

namespace PetitPas
{

SparseMatrixContainer::SparseMatrixContainer(unsigned int rowCount, unsigned int columnCount):
    rowCount(rowCount),
    columnCount(columnCount)
{
    if((rowCount == 0) || (columnCount == 0))
    {
        throw std::runtime_error("Matrix dimension can not be 0.");
    }
}

SparseMatrixContainer::~SparseMatrixContainer()
{
}


SparseMatrixContainer::SparseMatrixContainer(const MatrixContainer& other)
{
    this->rowCount = other.GetRowCount();
    this->columnCount = other.GetColumnCount();

    for(unsigned int rowIndex = 0; rowIndex < this->rowCount; rowIndex++)
    {
        for(unsigned int columnIndex = 0; columnIndex < this->columnCount; columnIndex++)
        {
            this->SetValueAt(rowIndex, columnIndex, other.GetValueAt(rowIndex, columnIndex));
        }
    }
}

unsigned int SparseMatrixContainer::GetRowCount() const
{
    return this->rowCount;
}

unsigned int SparseMatrixContainer::GetColumnCount() const
{
    return this->columnCount;
}

double SparseMatrixContainer::GetValueAt(unsigned int rowIndex, unsigned int columnIndex) const
{
    if(rowIndex >= this->rowCount || columnIndex >= this->columnCount)
    {
        throw std::runtime_error("Invalid index in GetValueAt.");
    }

    std::map<std::pair<unsigned int, unsigned int>, double>::const_iterator valueIterator = this->values.find(std::make_pair(rowIndex, columnIndex));

    // This position is not stored, so it is a zero.
    if(valueIterator == this->values.end())
    {
        return 0.0;
    }

    // Non zero value.
    return valueIterator->second;
}

void SparseMatrixContainer::SetValueAt(unsigned int rowIndex, unsigned int columnIndex, double value)
{
    if(rowIndex >= this->rowCount || columnIndex >= this->columnCount)
    {
        throw std::runtime_error("Invalid index in SetValueAt");
    }

    // Look for the actual value.
    std::map<std::pair<unsigned int, unsigned int>, double>::iterator valueIterator = this->values.find(std::make_pair(rowIndex, columnIndex));

    // Store only non zero values.
    if(MathTools::IsDoubleClose(0.0, value, 1e-12) == false)
    {
        if(valueIterator == this->values.end())
        {
            // If the entry did not existed, create it.
            this->values.insert(std::make_pair(std::make_pair(rowIndex, columnIndex), value));
        }
        else
        {
            // Else modify existing entry.
            valueIterator->second = value;
        }
    }
    else
    {
        // value == 0.0, suppress the entry if it existed.
        if(valueIterator != this->values.end())
        {
            this->values.erase(valueIterator);
        }
    }

}

void SparseMatrixContainer::AddValueAt(unsigned int rowIndex, unsigned int columnIndex, double value)
{
    if(rowIndex >= this->rowCount || columnIndex >= this->columnCount)
    {
        throw std::runtime_error("Invalid index in AddValueAt");
    }

    // Do something only if the value to add is not zero.
    if(MathTools::IsDoubleClose(0.0, value, 1e-12) == false)
    {
        // Look for the actual value.
        std::map<std::pair<unsigned int, unsigned int>, double>::iterator valueIterator = this->values.find(std::make_pair(rowIndex, columnIndex));

        if(valueIterator == this->values.end())
        {
            // If the entry did not existed, create it.
            this->values.insert(std::make_pair(std::make_pair(rowIndex, columnIndex), value));
        }
        else
        {
            // Else modify existing entry.
            valueIterator->second += value;

            // If the result is zero, we have to remove it from the database.
            if(MathTools::IsDoubleClose(0.0, valueIterator->second, 1e-12))
            {
                this->values.erase(valueIterator);
            }
        }
    }
}

void SparseMatrixContainer::MultiplyValueAt(unsigned int rowIndex, unsigned int columnIndex, double value)
{
    if(rowIndex >= this->rowCount || columnIndex >= this->columnCount)
    {
        throw std::runtime_error("Invalid index in MultiplyValueAt");
    }

    // Look for the actual value.
    std::map<std::pair<unsigned int, unsigned int>, double>::iterator valueIterator = this->values.find(std::make_pair(rowIndex, columnIndex));

    // If actual value is zero, multiplication will be zero so we do nothing!
    if(valueIterator == this->values.end())
    {
        return;
    }

    valueIterator->second *= value;

    // If the result is zero, we have to remove it from the database.
    if(MathTools::IsDoubleClose(0.0, valueIterator->second, 1e-12))
    {
        this->values.erase(valueIterator);
    }
}

SparseMatrixIterator SparseMatrixContainer::GetBeginIterator() const
{
    return SparseMatrixIterator(this->values.begin());
}

SparseMatrixIterator SparseMatrixContainer::GetEndIterator() const
{
    return SparseMatrixIterator(this->values.end());
}

} //namespace PetitPas
