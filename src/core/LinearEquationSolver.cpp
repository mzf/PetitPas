/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "LinearEquationSolver.h"
#include "MathTools.h"
#include "SparseMatrixContainer.h"

#include <stdexcept>
#include <cmath>

namespace PetitPas
{

LinearEquationSolver::LinearEquationSolver(Matrix& A, Matrix& B):
    solution(NULL),
    isSolved(false)
{
    if(A.IsSquare() == false)
    {
        throw std::runtime_error("The given A Matrix is not square.");
    }

    if(B.GetColumnCount() != 1)
    {
        throw std::runtime_error("The given B Matrix is not a vertical vector.");
    }

    if(A.GetRowCount() != B.GetRowCount())
    {
        throw std::runtime_error("The given A and B matrices don't have the same dimension.");
    }

    MatrixContainer* matrixContainer = A.GetContainer();
    SparseMatrixContainer* sparseMatrixContainer = dynamic_cast<SparseMatrixContainer*>(matrixContainer);
    if(sparseMatrixContainer != NULL)
    {
        this->systemCoefficients.resize(sparseMatrixContainer->GetRowCount(), sparseMatrixContainer->GetColumnCount());
        this->systemCoefficients.setFromTriplets(sparseMatrixContainer->GetBeginIterator(), sparseMatrixContainer->GetEndIterator());

        this->systemVector.resize(B.GetRowCount());
        for(unsigned int rowIndex = 0; rowIndex < B.GetRowCount(); rowIndex++)
        {
            this->systemVector(rowIndex) = B.GetValueAt(rowIndex, 0);
        }
    }
    else
    {
        throw std::runtime_error("Linear Solver Not implemented for now with Dense Matrices...");
    }
}

LinearEquationSolver::~LinearEquationSolver()
{
    delete this->solution;
}

const Matrix& LinearEquationSolver::GetSolution()
{
    if(this->isSolved == false)
    {
        Eigen::SparseLU<Eigen::SparseMatrix<double> > solver;

        // Decomposition.
        solver.compute(this->systemCoefficients);
        if(solver.info() != Eigen::Success)
        {
            throw std::runtime_error("Decomposition of Matrix failed.");
        }

        // Solve.
        Eigen::VectorXd result = solver.solve(this->systemVector);
        if(solver.info() != Eigen::Success)
        {
            throw std::runtime_error("Solve failed.");
        }

        this->solution = new Matrix(result.rows(), 1);
        for(unsigned int index = 0; index < result.rows(); index++)
        {
            this->solution->SetValueAt(index, 0, result(index));
        }

        this->isSolved = true;
    }

    return *(this->solution);
}

} //namespace PetitPas
