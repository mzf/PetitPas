/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DisplacementResult.h"
#include "Solution.h"
#include "Mesh.h"

namespace PetitPas
{

DisplacementResult::DisplacementResult(const Solution& solution) :
    solution(solution)
{

}

std::vector<double> DisplacementResult::GetNodalDisplacements(unsigned int nodeId) const
{
    return this->solution.GetNodalValues(nodeId);
}

const std::set<unsigned int>& DisplacementResult::GetNodeIds() const
{
    const Mesh& mesh = this->solution.GetMesh();
    return mesh.GetNodeIds();
}

} //namespace PetitPas
