/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DisplacementBoundaryCondition.h"
#include "StiffnessMatrix.h"
#include "NodalValuesMatrix.h"
#include <cmath>

namespace PetitPas
{

DisplacementBoundaryCondition::DisplacementBoundaryCondition(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double value) :
    nodeId(nodeId),
    degreeOfFreedomIndex(degreeOfFreedomIndex),
    displacementValue(value)
{

}

void DisplacementBoundaryCondition::ApplyTo(StiffnessMatrix& stiffnessMatrix, NodalValuesMatrix& nodalForceVector)
{
    double bigNumber = pow(10.0, 20.0);
    double currentStiffnessValue = stiffnessMatrix.GetDiagonalValue(this->nodeId, this->degreeOfFreedomIndex);
    stiffnessMatrix.SetDiagonalValue(this->nodeId, this->degreeOfFreedomIndex, currentStiffnessValue * bigNumber);
    nodalForceVector.SetValue(this->nodeId, this->degreeOfFreedomIndex, currentStiffnessValue * bigNumber * this->displacementValue);
}

} //namespace PetitPas
