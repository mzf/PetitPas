/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MESH_H
#define MESH_H

#include "Element.h"
#include <set>
#include <map>

namespace PetitPas
{
    /** \brief The Mesh class.
     */
    class Mesh
    {
    public:

        /** \brief Mesh constructor.
         *
         *
         */
        Mesh();

        /** \brief Mesh destructor.
         *
         *
         */
        ~Mesh();

        /** \brief Get the dimension of the mesh.
         *
         * \return the dimension of the mesh. If the mesh id empty, it returns 0.
         */
        unsigned int GetDimension() const;

        /** \brief Create a node and add it to the mesh.
         *
         * \param nodeLocation A Point object containing the location of the node.
         * \param nodeId The id of the node. If the id is zero, a new unique id will be generated.
         * \return A pointer to the new Node object.
         *
         * \throw std::runtime_error if the dimension of \a nodeLocation is different from other node in the mesh,
         *        or if the given \a nodeId is already present in the mesh.
         */
        Node* CreateNode(const Point& nodeLocation, unsigned int nodeId = 0);

        /** \brief Get a specific node from its id.
         *
         * \param nodeId The id of the node.
         * \return A pointer to the node with the given id.
         *
         * \throw std::runtime_error if no node has the given \a nodeId.
         */
        Node* GetNode(unsigned int nodeId) const;

        /** \brief Check if a node with the given id exists in the mesh.
         *
         * \param nodeId The id of the node that we want to check.
         * \return true if a node in the mesh has the \a nodeId, false otherwise.
         *
         */
        bool ContainsNode(unsigned int nodeId) const;

        /** \brief Get all nodes of the mesh.
         *
         * \return The list of all pointers to nodes that the mesh contains.
         *
         */
        const std::vector<Node*>& GetNodes() const;

        /** \brief Get all the node ids of the mesh.
         *
         * \return A set that contains all node ids of the mesh.
         *
         */
        const std::set<unsigned int>& GetNodeIds() const;

        /** \brief Get the maximum of the node ids
         *
         * \return The max of the node ids. If the mesh contains no node, it returns 0.
         *
         */
        unsigned int GetNodeIdMax() const;

        /** \brief Create an element and add it to the mesh.
         *
         * \param elementNodeIds List of node ids for this element. The nodes must be present in the mesh before creating the element.
         * \param materialBehavior The material associated with this element.
         * \param elementId The id of the element. If omitted or equal to zero, then a newly unique id will be generated.
         * \return The newly created element.
         *
         * \throw std::runtime_error if the mesh does not contain the given node ids or
         *                           if the given \a elementId is already present in the mesh.
         */
        Element* CreateElement(const std::vector<unsigned int>& elementNodeIds, const MaterialBehavior& materialBehavior, unsigned int elementId = 0);

        /** \brief Get the maximum of the element ids
         *
         * \return The max of the element ids. If the mesh contains no element, it returns 0.
         *
         */
        unsigned int GetElementIdMax() const;

        /** \brief Check if an element with the given id exists in the mesh.
         *
         * \param elementId The id of the element that we want to check.
         * \return true if an element in the mesh has the \a elementId, false otherwise.
         *
         */
        bool ContainsElement(unsigned int elementId) const;

        /** \brief Get a specific element from its id.
         *
         * \param elementId The id of the element.
         * \return A reference to the element with the given id.
         *
         * \throw std::runtime_error if no element has the given \a elementId.
         */
        Element* GetElement(unsigned int elementId) const;

        /** \brief Get all elements of the mesh.
         *
         * \return The list of all pointers to elements that the mesh contains.
         *
         */
        const std::vector<Element*>& GetElements() const;

        /** \brief Compute and return the global stiffness matrix of the mesh.
         *
         * \return The stiffness matrix of the mesh. The coefficient order is identical at the mesh node list,
         *         and the matrix dimension is the total number of node degrees.
         *         For instance, if you node list contains ids [3, 1, 6] in 2d, the matrix dimension will be 3*2=6,
         *         and the coefficient's order: [u3, v3, u1, v1, u6, v6].
         *
         */
        StiffnessMatrix GetStiffnessMatrix() const;

    private:
        std::vector<Node*> nodes;
        std::set<unsigned int> nodeIds;
        std::map<unsigned int, Node*> nodeFromId;
        std::vector<Element*> elements;
        std::set<unsigned int> elementIds;
        std::map<unsigned int, Element*> elementFromId;

    };

}//namespace PetitPas


#endif // MESH_H
