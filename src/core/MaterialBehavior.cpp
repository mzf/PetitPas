/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "MaterialBehavior.h"
#include "Matrix.h"
#include <stdexcept>

namespace PetitPas
{

MaterialBehavior::MaterialBehavior(double _youngModulus, double _poissonRatio, MaterialHypothesis _materialHypothesis) :
    youngModulus(_youngModulus),
    poissonRatio(_poissonRatio),
    hypothesis(_materialHypothesis),
    hookeMatrix(NULL)
{
    if((_poissonRatio >= 0.5) || (_poissonRatio <= -1.0) )
    {
        throw std::runtime_error("The Poisson's ratio must be between -1 and 0.5.");
    }

    this->InitializeHookeMatrix();
}

MaterialBehavior::~MaterialBehavior()
{
    delete this->hookeMatrix;
}

void MaterialBehavior::InitializeHookeMatrix()
{
    double coefficient;

    switch(this->hypothesis)
    {
    case MaterialHypothesis_2DPlaneStress:

        // Matrix is:
        //             [ 1 v 0
        // E/(1-v*v) *   v 1 0
        //               0 0 (1-v)/2 ]

        this->hookeMatrix = new Matrix(3,3);

        this->hookeMatrix->SetValueAt(0, 0, 1.0);
        this->hookeMatrix->SetValueAt(0, 1, this->poissonRatio);
        this->hookeMatrix->SetValueAt(1, 0, this->poissonRatio);
        this->hookeMatrix->SetValueAt(1, 1, 1.0);
        this->hookeMatrix->SetValueAt(2, 2, (1.0 - this->poissonRatio) * 0.5);

        coefficient = this->youngModulus / (1.0 - (this->poissonRatio*this->poissonRatio));
        this->hookeMatrix->MultiplyBy(coefficient);

        break;

    case MaterialHypothesis_2DPlaneStrain:

        // Matrix is:
        //                     [ 1-v v   0
        // E/((1+v)*(1-2*v)) *   v   1-v 0
        //                       0   0   (1-2v)/2 ]

        this->hookeMatrix = new Matrix(3,3);

        this->hookeMatrix->SetValueAt(0, 0, 1.0 - this->poissonRatio);
        this->hookeMatrix->SetValueAt(0, 1, this->poissonRatio);
        this->hookeMatrix->SetValueAt(1, 0, this->poissonRatio);
        this->hookeMatrix->SetValueAt(1, 1, 1.0 - this->poissonRatio);
        this->hookeMatrix->SetValueAt(2, 2, (1.0 - (2 * this->poissonRatio)) * 0.5);

        coefficient = this->youngModulus / ((1.0 + this->poissonRatio) * (1.0 - (2.0 * this->poissonRatio)));
        this->hookeMatrix->MultiplyBy(coefficient);

        break;


    case MaterialHypothesis_3D:

        // Matrix is:
        //                     [ 1-v v   v   0        0        0
        //                       v   1-v v   0        0        0
        //                       v   v   1-v 0        0        0
        // E/((1+v)*(1-2*v)) *   0   0   0   (1-2v)/2 0        0
        //                       0   0   0   0        (1-2v)/2 0
        //                       0   0   0   0        0        (1-2v)/2 ]

        this->hookeMatrix = new Matrix(6,6);

        this->hookeMatrix->SetValueAt(0, 0, 1.0 - this->poissonRatio);
        this->hookeMatrix->SetValueAt(0, 1, this->poissonRatio);
        this->hookeMatrix->SetValueAt(0, 2, this->poissonRatio);
        this->hookeMatrix->SetValueAt(1, 0, this->poissonRatio);
        this->hookeMatrix->SetValueAt(1, 1, 1.0 - this->poissonRatio);
        this->hookeMatrix->SetValueAt(1, 2, this->poissonRatio);
        this->hookeMatrix->SetValueAt(2, 0, this->poissonRatio);
        this->hookeMatrix->SetValueAt(2, 1, this->poissonRatio);
        this->hookeMatrix->SetValueAt(2, 2, 1.0 - this->poissonRatio);
        this->hookeMatrix->SetValueAt(3, 3, (1.0 - (2 * this->poissonRatio)) * 0.5);
        this->hookeMatrix->SetValueAt(4, 4, (1.0 - (2 * this->poissonRatio)) * 0.5);
        this->hookeMatrix->SetValueAt(5, 5, (1.0 - (2 * this->poissonRatio)) * 0.5);

        coefficient = this->youngModulus / ((1.0 + this->poissonRatio) * (1.0 - (2.0 * this->poissonRatio)));
        this->hookeMatrix->MultiplyBy(coefficient);

        break;

    default:
        throw std::runtime_error("Unknown Material Hypothesis.");

    }
}

const Matrix& MaterialBehavior::GetHookeMatrix() const
{
    return *(this->hookeMatrix);
}

MaterialBehavior::MaterialHypothesis MaterialBehavior::GetHypothesis() const
{
    return this->hypothesis;
}

} //namespace PetitPas
