/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "NodalValuesMatrix.h"
#include "Node.h"
#include "Matrix.h"

#include <stdexcept>

namespace PetitPas
{

NodalValuesMatrix::NodalValuesMatrix(const std::vector<Node*>& nodes)
{
    if(nodes.empty())
    {
        throw std::runtime_error("The given node id list is empty.");
    }

    // For now, let's assume that all nodes have the same number of degrees of freedom,
    // and that it is the dimension of the node.
    this->degreesOfFreedomPerNode = nodes[0]->GetDimension();

    //Create an empty matrix object.
    unsigned int matrixDimension = this->degreesOfFreedomPerNode * nodes.size();
    this->rawMatrix = new Matrix(matrixDimension, 1, Matrix::MatrixContainerType_Sparse);

    // Initialize data.
    this->InitData(nodes);
}

NodalValuesMatrix::NodalValuesMatrix(const Matrix& givenRawMatrix, const std::vector<Node*>& nodes)
{
    if(givenRawMatrix.GetColumnCount() != 1)
    {
        throw std::runtime_error("The given matrix is not vertical.");
    }

    if(nodes.empty())
    {
        throw std::runtime_error("The given node id list is empty.");
    }

    // For now, let's assume that all nodes have the same number of degrees of freedom,
    // and that it is the dimension of the node.
    this->degreesOfFreedomPerNode = nodes[0]->GetDimension();
    if(givenRawMatrix.GetRowCount() != nodes.size() * this->degreesOfFreedomPerNode)
    {
        throw std::runtime_error("The given matrix dimension is incorrect, it must equals the number of freedom degrees of the nodes.");
    }

    // Everything is ok, so we can build our matrix object.

    this->rawMatrix = new Matrix(givenRawMatrix);

    this->InitData(nodes);
}

void NodalValuesMatrix::InitData(const std::vector<Node*>& nodes)
{
    unsigned int index = 0;
    for(std::vector<Node*>::const_iterator nodeIterator = nodes.begin();
        nodeIterator != nodes.end();
        ++nodeIterator)
    {
        unsigned int nodeId = (*nodeIterator)->GetId();

        // Save node ids order.
        this->nodeIds.push_back(nodeId);

        // Compute node index.
        this->nodeIndexes[nodeId] = index * this->degreesOfFreedomPerNode;
        index++;
    }
}

NodalValuesMatrix::NodalValuesMatrix(const NodalValuesMatrix& other):
    nodeIds(other.nodeIds),
    nodeIndexes(other.nodeIndexes),
    degreesOfFreedomPerNode(other.degreesOfFreedomPerNode)
{
    this->rawMatrix = new Matrix(*(other.rawMatrix));
}

NodalValuesMatrix& NodalValuesMatrix::operator=(const NodalValuesMatrix& other)
{
    if (this != &other)
    {
        delete this->rawMatrix;
        this->rawMatrix = new Matrix(*(other.rawMatrix));

        this->nodeIds = other.nodeIds;
        this->nodeIndexes = other.nodeIndexes;
        this->degreesOfFreedomPerNode = other.degreesOfFreedomPerNode;
    }
    return *this;
}

NodalValuesMatrix::~NodalValuesMatrix()
{
    delete this->rawMatrix;
}

Matrix& NodalValuesMatrix::GetRawMatrix() const
{
    return *(this->rawMatrix);
}

const std::vector<unsigned int>& NodalValuesMatrix::GetNodeIds() const
{
    return this->nodeIds;
}

bool NodalValuesMatrix::ContainsNode(unsigned int nodeId) const
{
    return this->nodeIndexes.count(nodeId) > 0;
}

void NodalValuesMatrix::SetValue(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double value)
{
    this->CheckNodeIdAndDOFValidity(nodeId, degreeOfFreedomIndex);

    unsigned int nodeIndex = this->nodeIndexes.find(nodeId)->second;

    this->rawMatrix->SetValueAt(nodeIndex + degreeOfFreedomIndex, 0, value);
}

double NodalValuesMatrix::GetValue(unsigned int nodeId, unsigned int degreeOfFreedomIndex) const
{
    this->CheckNodeIdAndDOFValidity(nodeId, degreeOfFreedomIndex);

    unsigned int nodeIndex = this->nodeIndexes.find(nodeId)->second;

    return this->rawMatrix->GetValueAt(nodeIndex + degreeOfFreedomIndex, 0);
}

void NodalValuesMatrix::CheckNodeIdAndDOFValidity(unsigned int nodeId, unsigned int degreeOfFreedomIndex) const
{
    if(this->ContainsNode(nodeId) == false)
    {
        throw std::runtime_error("The nodal loads matrix does not contain the given node id.");
    }

    if(degreeOfFreedomIndex >= this->degreesOfFreedomPerNode)
    {
        throw std::runtime_error("The given degree of freedom index is out of range.");
    }

}

} //namespace PetitPas
