/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MATRIX_H
#define MATRIX_H

#include <stdexcept>

namespace PetitPas
{
    class MatrixContainer;

    /** \brief The Matrix class provides basic tools to manipulate matrix.
     */
    class Matrix
    {
    public:

        /** \brief Enumeration of matrix container types.
         */
        enum MatrixContainerType
        {
            MatrixContainerType_Dense, /**< The storage will be a big array. This is the best solution for small matrices (3x3 for instance). */
            MatrixContainerType_Sparse /**< The storage will be adapted to sparse matrices, i.e. big matrices that contain lots of zeros. */
        };

        /** \brief Matrix Constructor.
         *
         * \param rowCount Number of row for the matrix.
         * \param columnCount Number of column for the matrix
         * \param containerType The type of container for this matrix. If omitted it is MatrixContainerType_Dense.
         *
         * The matrix dimensions are set to \a rowCount and \a columnCount,
         * and every terms of the matrix are initialized at 0.
         *
         * \throw std::runtime_error if \a rowCount or \a columnCount are 0, or if \a containerType is not a valid MatrixContainerType.
         */
        Matrix(unsigned int rowCount, unsigned int columnCount, MatrixContainerType containerType = MatrixContainerType_Dense);

        /** \brief Matrix Destructor.
         */
        ~Matrix();

        /** \brief Matrix Copy Constructor.
         *
         * \param other The reference matrix to copy.
         *
         * The matrix dimensions and data are copied from the \a other Matrix.
         */
        Matrix(const Matrix& other);


        /** \brief Returns the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \return The value at the given indexes.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        double GetValueAt(unsigned int rowIndex, unsigned int columnIndex) const;

        /** \brief Set the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to assign.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        void SetValueAt(unsigned int rowIndex, unsigned int columnIndex, double value);

        /** \brief Add a value to the value at the given row and column indexes.
         * \code
         * matrix(rowIndex, columnIndex) += value
         * \endcode
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to add.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        void AddValueAt(unsigned int rowIndex, unsigned int columnIndex, double value);

        /** \brief Multiply the value at the given position by the given value.
         * \code
         * matrix(rowIndex, columnIndex) *= value
         * \endcode
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to multiply to the existing value.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        void MultiplyValueAt(unsigned int rowIndex, unsigned int columnIndex, double value);

        /** \brief Returns the number of row of the matrix.
         *
         * \return The number of row of the matrix.
         */
        unsigned int GetRowCount() const;

        /** \brief Returns the number of column of the matrix.
         *
         * \return The number of column of the matrix.
         */
        unsigned int GetColumnCount() const;


        /** \brief The assignment operator.
         *
         * \param other The other matrix object to copy.
         * \return A Matrix reference to this matrix.
         *
         */
        Matrix& operator= (const Matrix& other);

        /** \brief Addition operator.
         *
         * \param other The other matrix to add to this matrix.
         * \return A new Matrix that is the addition of this and \a other Matrix.
         *
         * \throw std::runtime_error if the dimensions of this and \a other are not equal.
         */
        Matrix operator+(const Matrix &other) const;

        /** \brief Subtraction operator.
         *
         * \param other The other matrix to subtract to this matrix.
         * \return A new Matrix that is the result of this minus \a other.
         *
         * \throw std::runtime_error if the dimensions of this and \a other are not equal.
         */
        Matrix operator-(const Matrix &other) const;

        /** \brief Equal operator.
         *
         * \param other The other matrix to check with this matrix.
         * \return true if the data of the \a other Matrix is the same of this Matrix.
         *
         */
        bool operator==(const Matrix &other) const;


        /** \brief Multiplication operator.
         *
         * \param other The other matrix to multiply to this matrix.
         * \return A new Matrix that is the the result of this * \a other.
         *         The dimensions of the result matrix are the row count of \a this matrix and the column count of the \a other matrix.
         *
         * \throw std::runtime_error if the column count of \a this is not equal to the row count of \a other.
         */
        Matrix operator*(const Matrix &other) const;


        /** \brief Returns a nice graphic representation of the matrix.
         *
         * \return A std::string object containing the representation of the matrix.
         *         Example:
         * \code
         * [ 1 2 3
         *   4 5 6 ]
         * \endcode
         *
         */
        std::string GetStringRepresentation() const;


        /** \brief Checks that the matrix is square.
         *
         * \return true if the matrix dimensions are equals.
         *
         */
        bool IsSquare() const;

        /** \brief Get the transpose of the matrix.
         *
         * \return A Matrix that is the transpose of the current matrix.
         *
         */
        Matrix GetTranspose() const;

        /** \brief Computes and returns the determinant of the matrix.
         *
         * \return The determinant of the matrix.
         *
         * \throw std::runtime_error if the matrix is not square.
         */
        double GetDeterminant() const;

        /** \brief Get the cofactor matrix.
         *
         * \return A Matrix that contains cofactors of the current matrix.
         *
         * \throw std::runtime_error if the matrix is not square.
         */
        Matrix GetCofactorMatrix() const;

        /** \brief Compute and get the inverse matrix.
         *
         * \return A Matrix that is the inverse of the current Matrix.
         *
         * The actual method is to use the cofactors matrix transposed and divided by determinant.
         *
         * \throw std::runtime_error if the matrix is not square or if it is not invertible.
         */
        Matrix GetInverse() const;


        /** \brief Multiply the matrix coefficients by a constant.
         *
         * \param value The constant value to multiply every coefficients of the matrix.
         *
         */
        void MultiplyBy(double value);

        /** \brief Swap two rows of the matrix.
         *
         * \param firstRowIndex index of the first row to swap.
         * \param secondRowIndex index of the second row to swap.
         *
         * \throw std::runtime_error if the given indexes are out of bounds.
         */
        void SwapRows(unsigned int firstRowIndex, unsigned int secondRowIndex);

        /** \brief Swap two columns of the matrix.
         *
         * \param firstColumnIndex index of the first column to swap.
         * \param secondColumnIndex index of the second column to swap.
         *
         * \throw std::runtime_error if the given indexes are out of bounds.
         */
        void SwapColumns(unsigned int firstColumnIndex, unsigned int secondColumnIndex);

        /** \brief Get the MatrixContainer of this Matrix.
         *
         */
        MatrixContainer* GetContainer() const;

    private:

        /** \brief Matrix Constructor with a MatrixContainer.
         *
         * \param newMatrixContainer A matrix container.
         *
         * The matrix will be initialized with the given matrix container.
         *
         * \throw std::runtime_error if \a rowCount or \a columnCount are 0.
         */
        Matrix(const MatrixContainer& newMatrixContainer);


        MatrixContainer* matrixContainer;
    };

} //namespace PetitPas

#endif // MATRIX_H
