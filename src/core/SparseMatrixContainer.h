/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SPARSE_MATRIX_CONTAINER_H
#define SPARSE_MATRIX_CONTAINER_H

#include "MatrixContainer.h"
#include "SparseMatrixIterator.h"

#include <map>

namespace PetitPas
{

    /** \brief The Sparse Matrix Container store data in as a list of key/values,
     *         where key is the position of the value inside the matrix.
     *         This container is useful for big matrices that contain lots of zero,
     *         because it reduces the memory usage.
     */
    class SparseMatrixContainer : public MatrixContainer
    {
    public:

        /** \brief Constructor.
         *
         * \param rowCount Number of row for the matrix.
         * \param columnCount Number of column for the matrix
         *
         * The matrix dimensions are set to \a rowCount and \a columnCount,
         * and every terms of the matrix are initialized at 0.
         *
         * \throw std::runtime_error if \a rowCount or \a columnCount are 0.
         */
        SparseMatrixContainer(unsigned int rowCount, unsigned int columnCount);

        /** \brief Destructor.
         */
        ~SparseMatrixContainer();

        /** \brief SparseMatrixContainer Constructor from a generic MatrixContainer.
         *
         * \param other The reference MatrixContainer to copy.
         *
         */
        SparseMatrixContainer(const MatrixContainer& other);

        /** \brief Returns the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \return The value at the given indexes.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        double GetValueAt(unsigned int rowIndex, unsigned int columnIndex) const;

        /** \brief Set the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to assign.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        void SetValueAt(unsigned int rowIndex, unsigned int columnIndex, double value);

        /** \brief Add a value to the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to add.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        void AddValueAt(unsigned int rowIndex, unsigned int columnIndex, double value);

        /** \brief Multiply the value at the given position by the given value.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to multiply to the existing value.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        void MultiplyValueAt(unsigned int rowIndex, unsigned int columnIndex, double value);

        /** \brief Returns the number of row of the SparseMatrixContainer.
         *
         * \return The number of row of the matrix.
         */
        unsigned int GetRowCount() const;

        /** \brief Returns the number of column of the SparseMatrixContainer.
         *
         * \return The number of column of the matrix.
         */
        unsigned int GetColumnCount() const;

        /** \brief Get the begin iterator of the non zero values.
         *
         */
        SparseMatrixIterator GetBeginIterator() const;

        /** \brief Get the end iterator of the non zero values.
         *
         */
        SparseMatrixIterator GetEndIterator() const;


    private:

        std::map<std::pair<unsigned int, unsigned int>, double> values;
        unsigned int rowCount;
        unsigned int columnCount;
    };

}//namespace PetitPas


#endif // SPARSE_MATRIX_CONTAINER_H
