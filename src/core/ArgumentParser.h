/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ARGUMENT_PARSER_H
#define ARGUMENT_PARSER_H

#include "ArgumentParserResult.h"

#include <string>
#include <list>

namespace PetitPas
{

    /** \brief Tool to parse command line arguments.
     *         This class is mainly inspired by the "argparse" module of Python language.
     */
    class ArgumentParser
    {
    public:

        /** \brief Defines the type of the argument.
         */
        enum ArgumentType
        {
            StandAlone,     /**< The argument does not requires a value. */
            RequiresValue   /**< The argument requires an other argument after it. */
        };

        /** \brief Constructor. You need to call AddArgument to initialize the parser.
         */
        ArgumentParser();

        /** \brief Add an argument to the parser.
         *
         * \param name The name of the argument as it will appears in the command line, for instance: "-f"
         * \param alternativeName An alternative name of the argument. Useful for double dash, for instance: "--file"
         * \param description The description of the argument. It will be displayed in the "help" command.
         * \param argumentType The ArgumentType flag that defines the behavior of the argument.
         *
         * \throw std::runtime_error if an argument with the same name or alternative name had already been added.
         */
        void AddArgument(const std::string& name, const std::string& alternativeName, const std::string& description,  ArgumentType argumentType);

        /** \brief Parse the given input arguments according to the rules added via AddArgument(),
         *         and return an ArgumentParserResult object to retrieve the arguments.
         *
         * \param argumentCount The number of arguments.
         * \param argumentVector List of arguments. The number of argument must be given by \a argumentCount.
         * \return An ArgumentParserResult that contains the parsed arguments.
         *
         */
        ArgumentParserResult ParseArguments(int argumentCount, char* argumentVector[]);

    private:

        struct Argument
        {
            Argument(const std::string& name, const std::string& alternativeName, const std::string& description,  ArgumentType argumentType):
                name(name),
                alternativeName(alternativeName),
                description(description),
                type(argumentType)
            { }

            bool Matches(const std::string& nameToMatch) const
            {
                return (this->name == nameToMatch) || (this->alternativeName == nameToMatch);
            }

            std::string name;
            std::string alternativeName;
            std::string description;
            ArgumentType type;
        };

        std::string GetHelpMessage(const std::string& programName);

        std::list<Argument> arguments;


    };

}//namespace PetitPas


#endif // ARGUMENT_PARSER_H
