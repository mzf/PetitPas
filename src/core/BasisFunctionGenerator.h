/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BASISFUNCTIONGENERATOR_H
#define BASISFUNCTIONGENERATOR_H

#include "Element.h"
#include "PolynomialFunction.h"

namespace PetitPas
{
    /** \brief Element Basis Function Generator.
     *
     *  This static class generates a basis function for a given element.
     */
    class BasisFunctionGenerator
    {
    public:
        /** \brief Computes and returns a polynomial function for a given element.
         *
         * \param element The element that needs a basis function.
         * \return A list of PolynomialFunction object that represents a polynomial basis function for
         *         the given \a element. Each function is associated to an element node.
         *
         */
        static std::vector<PolynomialFunction> GetPolynomialFunctions(const Element& element);

        /** \brief Computes and returns a polynomial function for a given list of nodes.
         *
         * \param nodes The node pointers list that needs a basis function.
         * \return A list of PolynomialFunction object that represents a polynomial basis function for
         *         the given \a nodes. Each function is associated to a node of the list.
         *
         */
        static std::vector<PolynomialFunction> GetPolynomialFunctions(const std::vector<Node*>& nodes);

        /** \brief Computes and returns a polynomial function for a given list of nodes.
         *
         * \param nodes The node list that needs a basis function.
         * \return A list of PolynomialFunction object that represents a polynomial basis function for
         *         the given \a nodes. Each function is associated to a node of the list.
         *
         */
        static std::vector<PolynomialFunction> GetPolynomialFunctions(std::vector<Node>& nodes);

    private:
        /**< Returns the polynomial terms for the given node count and given dimension. */
        static std::vector<PolynomialTerm> GetTerms(unsigned int nodeCount, unsigned int dimension);
    };

}

#endif // BASISFUNCTIONGENERATOR_H
