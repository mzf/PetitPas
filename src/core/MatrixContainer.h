/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MATRIX_CONTAINER_H
#define MATRIX_CONTAINER_H

namespace PetitPas
{

    /** \brief This is an abstract class to define a data container for a matrix.
     */
    class MatrixContainer
    {
    public:

        /** \brief Virtual Destructor.
         *
         *
         */
        virtual ~MatrixContainer() {}

        /** \brief Returns the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \return The value at the given indexes.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        virtual double GetValueAt(unsigned int rowIndex, unsigned int columnIndex) const = 0;

        /** \brief Set the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to assign.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        virtual void SetValueAt(unsigned int rowIndex, unsigned int columnIndex, double value) = 0;

        /** \brief Add a value to the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to add.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        virtual void AddValueAt(unsigned int rowIndex, unsigned int columnIndex, double value) = 0;

        /** \brief Multiply the value at the given position by the given value.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to multiply to the existing value.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        virtual void MultiplyValueAt(unsigned int rowIndex, unsigned int columnIndex, double value) = 0;

        /** \brief Returns the number of row of the matrix.
         *
         * \return The number of row of the matrix.
         */
        virtual unsigned int GetRowCount() const = 0;

        /** \brief Returns the number of column of the matrix.
         *
         * \return The number of column of the matrix.
         */
        virtual unsigned int GetColumnCount() const = 0;

    };

}//namespace PetitPas


#endif // MATRIX_CONTAINER_H
