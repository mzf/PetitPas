/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DISPLACEMENT_BOUNDARY_CONDITION_H
#define DISPLACEMENT_BOUNDARY_CONDITION_H

#include "BoundaryCondition.h"

namespace PetitPas
{

    /** \brief Represents a displacement boundary condition.
     */
    class DisplacementBoundaryCondition : public BoundaryCondition
    {
    public:
        /** \brief Displacement Constructor.
         *
         * \param nodeId The id of the concerned node.
         * \param degreeOfFreedomIndex The degree of freedom index of the node, starts at 0.
         * \param value The value of the displacement.
         *
         */
        DisplacementBoundaryCondition(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double value);

        /** \brief Apply this displacement to a finite element system.
         *
         * Currently, the method to apply the displacement is:
         *  - the diagonal coefficient value of the stiffness matrix will be set at a big constant multiplied by the displacement value.
         *  - the coefficient in the nodal force vector will be set at big_constant * stiffness_diagonal_coefficient * displacement.
         *
         * Example: if the stiffness matrix is
         *\code
         * [ a b c d
         *   e f g h
         *   i j k l
         *   m n p q ]
         *\endcode
         * for the 3rd coefficient it will become:
         *\code
         * [ a b c       d
         *   e f g       h
         *   i j k*10^20 l
         *   m n p       q ]
         *\endcode
         * and the nodal force will be:
         *\code
         * k*value*10^20
         *\endcode
         *
         * \param stiffnessMatrix The StiffnessMatrix object that represents the stiffness matrix of the system.
         * \param nodalForceVector A NodalValuesMatrix object that represents a vertical vector.
         *
         */
        void ApplyTo(StiffnessMatrix& stiffnessMatrix, NodalValuesMatrix& nodalForceVector);

    private:
        unsigned int nodeId;
        unsigned int degreeOfFreedomIndex;
        double displacementValue;

    };

}//namespace PetitPas


#endif // DISPLACEMENT_BOUNDARY_CONDITION_H
