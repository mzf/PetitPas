/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LOGGER_H
#define LOGGER_H

#include <list>
#include <ctime>
#include <string>

namespace PetitPas
{
    /** \brief The Logger class to record messages during processing.
     */
    class Logger
    {
    public:

        /** \brief Destructor.
         *
         */
        ~Logger();

        /** \brief Log a message.
         *
         * \param message The message to record.
         *
         */
        static void LogMessage(const std::string& message);

        /** \brief Log the time since the last call.
         *
         *
         */
        static void LogTime();

    private:
        Logger();

        static Logger instance;

        std::list<std::string> messages;
        time_t* startTime;

    };

}//namespace PetitPas


#endif // LOGGER_H
