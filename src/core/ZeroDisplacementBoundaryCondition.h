/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ZERO_DISPLACEMENT_BOUNDARY_CONDITION_H
#define ZERO_DISPLACEMENT_BOUNDARY_CONDITION_H

#include "BoundaryCondition.h"

namespace PetitPas
{

    /** \brief Represents a zero displacement boundary condition.
     */
    class ZeroDisplacementBoundaryCondition : public BoundaryCondition
    {
    public:
        /** \brief Zero Displacement Constructor.
         *
         * \param nodeId The id of the concerned node.
         * \param degreeOfFreedomIndex The degree of freedom index of the node, starts at 0.
         *
         */
        ZeroDisplacementBoundaryCondition(unsigned int nodeId, unsigned int degreeOfFreedomIndex);

        /** \brief Apply this zero displacement to a finite element system.
         *
         * Currently, the method to apply the zero displacement is:
         *  - the row and column of the corresponding index in the stiffness matrix will be set at O.
         *  - the diagonal coefficient value of the stiffness matrix will be set at 1.
         *  - the coefficient in the nodal force vector will be set at 0.
         *
         * Example: if the stiffness matrix is
         *\code
         * [ a b c d
         *   e f g h
         *   i j k l
         *   m n p q ]
         *\endcode
         * for the 3rd coefficient it will become:
         *\code
         * [ a b 0 d
         *   e f 0 h
         *   0 0 1 0
         *   m n 0 q ]
         *\endcode
         *
         * \param stiffnessMatrix The StiffnessMatrix object that represents the stiffness matrix of the system.
         * \param nodalForceVector A NodalValuesMatrix object that represents a vertical vector.
         *
         */
        void ApplyTo(StiffnessMatrix& stiffnessMatrix, NodalValuesMatrix& nodalForceVector);

    private:
        unsigned int nodeId;
        unsigned int degreeOfFreedomIndex;

    };

}//namespace PetitPas


#endif // ZERO_DISPLACEMENT_BOUNDARY_CONDITION_H
