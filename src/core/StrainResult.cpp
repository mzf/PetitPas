/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "StrainResult.h"
#include "DisplacementResult.h"
#include "Solution.h"
#include "Mesh.h"
#include "ResultNotFoundException.h"


namespace PetitPas
{

StrainResult::StrainResult(const Solution& solution):
    solution(solution)
{
    // Compute the strain result for each element.

    const Mesh& mesh = solution.GetMesh();
    const std::vector<Element*>& elements = mesh.GetElements();
    for(std::vector<Element*>::const_iterator elementIterator = elements.begin();
        elementIterator != elements.end();
        ++elementIterator)
    {
        Element* currentElement = (*elementIterator);
        const std::vector<Node*>& elementNodes = currentElement->GetNodes();

        //Discard empty elements.
        if(elementNodes.empty())
            continue;

        // Create the displacement matrix of the nodes of this element.
        DisplacementResult displacementResult(this->solution);
        unsigned int degreeOfFreedomPerNode = elementNodes[0]->GetDimension();
        unsigned int degreeOfFreedomPerElement = degreeOfFreedomPerNode * elementNodes.size();
        Matrix nodalDisplacements(degreeOfFreedomPerElement, 1);
        unsigned int matrixVerticalIndex = 0;
        for(std::vector<Node*>::const_iterator nodeIterator = elementNodes.begin();
            nodeIterator != elementNodes.end();
            ++nodeIterator)
        {
            std::vector<double> rawNodalDisplacements = displacementResult.GetNodalDisplacements((*nodeIterator)->GetId());
            for(std::vector<double>::iterator nodalDisplacementIterator = rawNodalDisplacements.begin();
                nodalDisplacementIterator != rawNodalDisplacements.end();
                ++nodalDisplacementIterator)
            {
                nodalDisplacements.SetValueAt(matrixVerticalIndex, 0, *(nodalDisplacementIterator));
                matrixVerticalIndex++;
            }
        }

        // Compute the strain at each node of this element.
        unsigned int nodeIndex = 0;
        for(std::vector<Node*>::const_iterator nodeIterator = elementNodes.begin();
            nodeIterator != elementNodes.end();
            ++nodeIterator)
        {
            // Compute the strain at the node index.
            Matrix strainMatrix = currentElement->GetFieldDerivativeValue(nodalDisplacements, nodeIndex);
            nodeIndex++;

            // Create a key <element, node> to store the result.
            ElementIdNodeIdPair elementNodePair(currentElement->GetId(), (*nodeIterator)->GetId());

            // Add the result to the map.
            this->strainAtElementNode.insert(std::pair<ElementIdNodeIdPair, Matrix>(elementNodePair, strainMatrix));

        }

        // Save the element.
        this->elements.push_back(currentElement);
    }
}

const Matrix& StrainResult::GetElementalStrainAtNode(unsigned int elementId, unsigned int nodeId) const
{
    // Create a key <element, node> to get the result.
    ElementIdNodeIdPair elementNodePair(elementId, nodeId);

    std::map<ElementIdNodeIdPair, Matrix>::const_iterator strainIterator = this->strainAtElementNode.find(elementNodePair);
    if(strainIterator == this->strainAtElementNode.end())
    {
        throw ResultNotFoundException(elementId, nodeId);
    }

    return strainIterator->second;
}

const std::vector<Element*>& StrainResult::GetElements() const
{
    return this->elements;
}

} //namespace PetitPas
