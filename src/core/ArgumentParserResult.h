/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ARGUMENT_PARSER_RESULT_H
#define ARGUMENT_PARSER_RESULT_H

#include <map>
#include <string>

namespace PetitPas
{

    /** \brief The result of parsing command line arguments.
     */
    class ArgumentParserResult
    {
    public:

        /** \brief An enum to set the status of the parsing.
         */
        enum ExitStatus
        {
            MustExitStatus, /**< The process must not continue. */
            ContinueStatus  /**< The process must continue, even if an error were detected. */
        };

        /** \brief Constructor.
         *
         * \param parsedArguments A map of arguments and associated values.
         * \param exitStatus Specifies if the process must not continue after the parsing.
         * \param errorMessage An message for an error that occurs during the parsing. If no error then pass an empty string.
         *
         */
        ArgumentParserResult(const std::map<std::string, std::string>& parsedArguments, ExitStatus exitStatus, const std::string& errorMessage);

        /** \brief Check if an argument were in the input command line.
         *
         * \param argumentName The name of the argument that we want to check.
         * \return true if the argument were found in the input command line.
         *
         */
        bool ContainsArgument(const std::string& argumentName) const;

        /** \brief Get the value associated with an argument.
         *
         * \param argumentName The name of the argument.
         * \return The string value associated with the argument.
         *
         * \throw std::runtime_error if no argument matches \a argumentName.
         *        Check the presence of an argument with ContainsArgument() method to avoid this exception.
         */
        const std::string& GetArgumentValue(const std::string& argumentName) const;

        /** \brief Check of the process must not continue.
         *
         * \return true if the process must exit.
         *
         */
        bool MustExit() const;

        /** \brief Get the associated error message. If the parsing did not generate any error, the error message is empty.
         *
         * \return A string that contains the error message. If no error, then it returns an empty string.
         *
         */
        const std::string& GetErrorMessage() const;


    private:
        std::map<std::string, std::string> arguments;
        ExitStatus exitStatus;
        std::string errorMessage;

    };

}//namespace PetitPas


#endif // ARGUMENT_PARSER_RESULT_H
