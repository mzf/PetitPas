/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef POLYNOMIALTERM_H
#define POLYNOMIALTERM_H

#include <vector>

namespace PetitPas
{
    /** \brief Represents a term of a polynomial function.
     */
    class PolynomialTerm
    {
    public:
        /** \brief PolynomialTerm constructor.
         *
         * \param degrees A list of degree for each dimension coefficients.
         *                For instance, the term x*y^2 will need the list {1, 2}.
         * \param coefficient The multiplicative coefficient. Default value is 1.0.
         *
         */
        PolynomialTerm(const std::vector<int>& degrees, double coefficient = 1.0);

        /** \brief Get the number of variable of the term.
         *
         * \return Return the dimension of the polynomial term.
         *
         */
        unsigned int GetDimension() const;

        /** \brief Evaluator of the term.
         *
         * \param variableValues The list of value for each variable of the term.
         * \return The result of the evaluation of the term with the given values.
         *         For instance, if the the term is x*y^2 then \a variableValues must contains 2 elements.
         *
         * \throw std::runtime_error if the size of \a variableValues is not the same as the term variable number.
         */
        double GetValue(const std::vector<double>& variableValues) const;

        /** \brief Evaluate the derivative value of this term at a given point.
         *
         * \param byVariableIndex index of the variable index that starts at 0. For instance if you have 3 variables (x, y, z)
         *                        and you want the derivative by y, this index must be 1.
         * \param variableValues The list of value for each variable of the term.
         *
         * \return The value of the derivative of this term at the given variable values by the variable index.
         *
         * \throw std::runtime_error if the size of \a variableValues is not the same as the term variable number,
         *                         or if the variable index is out of range (0..dimension-1).
         */
        double GetDerivativeValue(unsigned int byVariableIndex, const std::vector<double>& variableValues) const;

        /** \brief Set the coefficient of the term.
         *
         * \param value The new value of the coefficient.
         *
         */
        void SetCoefficient(double value);

    private:
        std::vector<int> degrees;
        double coefficient;
    };

}

#endif // POLYNOMIALTERM_H
