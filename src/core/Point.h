/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef POINT_H
#define POINT_H

#include <vector>

namespace PetitPas
{
    //TODO: Add Coordinate system to Point.

    /** \brief The Point class that represents a point of any dimension > 0.
     */
    class Point
    {
    public:

        /** \brief Point constructor
         *
         * \param coords The list of coordinates.
         *               The size of the list must be equals to the number of dimensions of the point.
         *
         * \throw std::runtime_error if the number of coordinates of the point is 0.
         */
        explicit Point(const std::vector<double> &coords);


        /** \brief Get the point coordinates
         *
         * \return The coordinates in a std::vector<double> object.
         *
         */
        const std::vector<double>& GetCoordinates() const;

        /** \brief Get the dimension of the point.
         *
         * \return The number of coordinates of the point.
         *
         */
        unsigned int GetDimension() const;

    private:
        std::vector<double> coordinates;
    };

}//namespace PetitPas


#endif // POINT_H
