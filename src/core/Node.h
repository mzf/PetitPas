/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NODE_H
#define NODE_H

#include "Point.h"

namespace PetitPas
{
    /** \brief The node class.
     */
    class Node
    {
    public:
        /** \brief Node constructor.
         *
         * \param location A Point where the node is located.
         * \param id The id of the node.
         *
         */
        Node(const Point& location, unsigned int id);

        /** \brief Node destructor.
         */
        ~Node();

        /** \brief Get the location of the node.
         *
         * \return A Point object containing the location of the node.
         *
         */
        const Point& GetLocation() const;

        /** \brief Get the id of the node.
         *
         * \return The id of the node.
         *
         */
        unsigned int GetId() const;

        /** \brief Get the coordinates of the node.
         *
         * \return The list of coordinates.
         *
         */
        const std::vector<double>& GetCoordinates() const;

        /** \brief Get the dimension of the node.
         *
         * \return The number of coordinates of the node location.
         *
         */
        unsigned int GetDimension() const;

    private:
        Point location;
        unsigned int id;
    };

}

#endif // NODE_H
