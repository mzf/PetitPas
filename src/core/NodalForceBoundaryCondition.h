/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NODAL_FORCE_BOUNDARY_CONDITION_H
#define NODAL_FORCE_BOUNDARY_CONDITION_H

#include "BoundaryCondition.h"

namespace PetitPas
{

    /** \brief Represents a nodal force boundary condition.
     */
    class NodalForceBoundaryCondition : public BoundaryCondition
    {
    public:
        /** \brief Nodal Force Constructor.
         *
         * \param nodeId The id of the concerned node.
         * \param degreeOfFreedomIndex The degree of freedom index of the node, starts at 0.
         * \param value The value of the force to apply on the node.
         *
         */
        NodalForceBoundaryCondition(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double value);

        /** \brief Apply this nodal force to a finite element system.
         *
         * This method will add the value in the nodal force vector at the right index.
         * The stiffness matrix remains unchanged.
         *
         * \param stiffnessMatrix The StiffnessMatrix object that represents the stiffness matrix of the system.
         * \param nodalForceVector A NodalValuesMatrix object that represents a vertical vector.
         *
         */
        void ApplyTo(StiffnessMatrix& stiffnessMatrix, NodalValuesMatrix& nodalForceVector);

    private:
        unsigned int nodeId;
        unsigned int degreeOfFreedomIndex;
        double forceValue;

    };

}//namespace PetitPas


#endif // NODAL_FORCE_BOUNDARY_CONDITION_H
