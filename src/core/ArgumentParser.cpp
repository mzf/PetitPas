/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ArgumentParser.h"

#include <stdexcept>
#include <sstream>

namespace PetitPas
{

ArgumentParser::ArgumentParser()
{

}

void ArgumentParser::AddArgument(const std::string& name, const std::string& alternativeName, const std::string& description,  ArgumentType argumentType)
{
    // Check argument duplicate.
    for(std::list<Argument>::iterator argumentIterator = this->arguments.begin();
        argumentIterator != this->arguments.end();
        ++argumentIterator)
    {
        if((argumentIterator->name == name) ||
           (argumentIterator->alternativeName == name) ||
           (argumentIterator->name == alternativeName) ||
           (argumentIterator->alternativeName == alternativeName))
        {
            throw std::runtime_error("An argument with the same name or alternative name have already been added.");
        }
    }

    // Finally add the argument in the database.
    Argument newArgument(name, alternativeName, description, argumentType);
    this->arguments.push_back(newArgument);
}

ArgumentParserResult ArgumentParser::ParseArguments(int argumentCount, char* argumentVector[])
{
    std::map<std::string, std::string> parsedArguments;
    std::stringstream errorMessage;
    ArgumentParserResult::ExitStatus status = ArgumentParserResult::ContinueStatus;

    if((argumentCount <= 0) || argumentVector == NULL)
    {
        // Return an empty result.
        return ArgumentParserResult(parsedArguments, ArgumentParserResult::ContinueStatus, "");
    }

    // First argument is the program's name.
    std::string programName(argumentVector[0]);

    // If no argument
    if(argumentCount == 1)
    {
        return ArgumentParserResult(parsedArguments, ArgumentParserResult::MustExitStatus, "Use -h to get help on PetitPas usage.");
    }

    // Parse each given argument.
    for(int argumentIndex = 1; argumentIndex < argumentCount; argumentIndex++)
    {
        char* argumentCharPointer = argumentVector[argumentIndex];
        if(argumentCharPointer == NULL)
            continue;
        std::string argumentName(argumentCharPointer);

        // Special process for the help message.
        if((argumentName == "help") ||
           (argumentName == "-h") ||
           (argumentName == "--help"))
        {
            errorMessage << this->GetHelpMessage(programName);
            status = ArgumentParserResult::MustExitStatus;
            continue;
        }

        // Check if an added argument matches.
        for(std::list<Argument>::const_iterator argumentIterator = this->arguments.begin();
            argumentIterator != this->arguments.end();
            ++argumentIterator)
        {
            if (argumentIterator->Matches(argumentName))
            {
                // Check if this argument were not already found.
                if(parsedArguments.find(argumentIterator->name) != parsedArguments.end())
                {
                    errorMessage << "The argument " << argumentIterator->name << " has been entered twice. The second will be ignored." << std::endl;
                    continue;
                }

                // Store the argument.
                std::string nextArgument;
                switch(argumentIterator->type)
                {
                case StandAlone:
                    // Add the argument.
                    parsedArguments.insert(std::pair<std::string, std::string>(argumentIterator->name, ""));
                    break;

                case RequiresValue:
                    // Parse the next argument.
                    argumentIndex++;
                    if(argumentIndex >= argumentCount)
                    {
                        errorMessage << "The argument " << argumentIterator->name << " requires a value." << std::endl;
                        status = ArgumentParserResult::MustExitStatus;
                        break;
                    }
                    nextArgument = argumentVector[argumentIndex];
                    parsedArguments.insert(std::pair<std::string, std::string>(argumentIterator->name, nextArgument));
                    break;

                default:
                    // Do nothing.
                    break;
                } // switch type

            } // argument matches

        } // argument list

    } // argument indexes

    // Create the result and return it.
    return ArgumentParserResult(parsedArguments, status, errorMessage.str());
}

std::string ArgumentParser::GetHelpMessage(const std::string& programName)
{
    std::stringstream helpStream;

    // Usage.
    helpStream << "usage: " << programName;
    for(std::list<Argument>::const_iterator argumentIterator = this->arguments.begin();
            argumentIterator != this->arguments.end();
            ++argumentIterator)
    {
        helpStream << " [" << argumentIterator->name;
        if(argumentIterator->type == RequiresValue)
        {
            helpStream << " {value}";
        }
        helpStream << "]";
    }
    helpStream << std::endl;
    helpStream << std::endl;

    // Arguments description.
    helpStream << "arguments:" << std::endl;
    for(std::list<Argument>::const_iterator argumentIterator = this->arguments.begin();
            argumentIterator != this->arguments.end();
            ++argumentIterator)
    {
        helpStream << "  " << argumentIterator->name;
        if(argumentIterator->type == RequiresValue)
        {
            helpStream << " {value}";
        }
        helpStream << ", " << argumentIterator->alternativeName;
        if(argumentIterator->type == RequiresValue)
        {
            helpStream << " {value}";
        }
        helpStream << "\t" << argumentIterator->description;

        helpStream << std::endl;
    }

    return helpStream.str();
}

} //namespace PetitPas
