/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Logger.h"

#include <sstream>
#include <iostream>


namespace PetitPas
{

Logger Logger::instance = Logger();

Logger::Logger():
    startTime(NULL)
{

}

Logger::~Logger()
{
    delete this->startTime;
}

void Logger::LogMessage(const std::string& message)
{
    instance.messages.push_back(message);
    std::cout << message << std::endl;
}

void Logger::LogTime()
{
    // First call at this method initialize the start time.
    if(instance.startTime == NULL)
    {
        instance.startTime = new time_t;
        time(instance.startTime);
    }

    // Compute time difference from start time.
    time_t currentTime;
    time(&currentTime);

    double diffTime = difftime(currentTime, *(instance.startTime));

    // Print the message.
    std::stringstream message;
    message << "Elapsed time: " << diffTime << " seconds.";
    instance.LogMessage(message.str());

}


} //namespace PetitPas
