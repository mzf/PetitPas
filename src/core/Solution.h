/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SOLUTION_H
#define SOLUTION_H

#include "NodalValuesMatrix.h"

namespace PetitPas
{
    class Mesh;

    /** \brief Contains the solution of the finite element simulation.
     */
    class Solution
    {
    public:
        /** \brief Solution constructor.
         *
         * \param mesh The Mesh object that the result apply to.
         * \param nodalDisplacementVector A vertical NodalValuesMatrix object where values per nodes are the displacement for each node's degree of freedom.
         *
         * \throw std::runtime_error if \a nodalDisplacementVector node ids are not in the given \a mesh.
         */
        Solution(const Mesh& mesh, const NodalValuesMatrix& nodalDisplacementVector);

        /** \brief Get a nodal value.
         *         In a structural analysis, this value represents a displacement.
         *
         * \param nodeId The id of the node that we want the solution value.
         * \param degreeOfFreedomIndex The index of the degree of freedom that we want the value.
         * \return The displacement value of the DOF of the given node.
         *
         * \throw std::runtime_error if the mesh does not contains the node id or
         *                           if \a degreeOfFreedomIndex is out of bounds.
         */
        double GetNodalValue(unsigned int nodeId, unsigned int degreeOfFreedomIndex) const;

        /** \brief Get all the values at a specific node.
         *
         * \param nodeId The id of the node that we want the solution value.
         * \return A list of double that are the values of the solution for a given node.
         *         The number of value is equals to the total degree of freedom of this node.
         *
         * \throw std::runtime_error if the mesh does not contains the node id
         */
        std::vector<double> GetNodalValues(unsigned int nodeId) const;

        /** \brief Get the mesh associated with this solution.
         *
         * \return A Mesh object associated with this solution.
         *
         */
        const Mesh& GetMesh() const;

    private:

        const Mesh& mesh;
        NodalValuesMatrix nodalValueVector;

    };

}//namespace PetitPas


#endif // SOLUTION_H
