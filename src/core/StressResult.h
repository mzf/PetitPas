/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRESS_RESULT_H
#define STRESS_RESULT_H

#include "ElementIdNodeIdPair.h"
#include <map>

namespace PetitPas
{
    class StrainResult;
    class Matrix;
    class Element;

    /** \brief The Stress Result.
     *         The computation of stress is based on the strain of the element.
     *
     * \code
     * [Stress] = [C] * [Strain]
     * \endcode
     * with [C] the behavior matrix (or material matrix) of the element and [Strain] the vertical strain matrix of the element.
     */
    class StressResult
    {
    public:
        /** \brief Constructor from a StrainResult object.
         *
         * \param strainResult A StrainResult object from a solved analysis.
         *
         */
        StressResult(const StrainResult& strainResult);

        /** \brief Get the stress at an element's node.
         *
         * \param elementId Id of the element we want to process.
         *                  The solution's mesh must contains this element.
         * \param nodeId Id of the node of the element where we want the stress result.
         *               The \a elementId element must contains this node Id.
         * \return A vertical Matrix object that contains the stress values at the given node for the given element.
         *
         * \throw ResultNotFoundException if there is not result associated to the given pair \a elementId \a nodeId.
         */
        const Matrix& GetElementalStressAtNode(unsigned int elementId, unsigned int nodeId) const;

        /** \brief Get the elements that have a stress result.
         *
         * \return A list of pointer to Element objects.
         *
         */
        const std::vector<Element*>& GetElements() const;

    private:

        const StrainResult& strainResult;
        std::map<ElementIdNodeIdPair, Matrix> stressAtElementNode;
        std::vector<Element*> elements;

    };

}//namespace PetitPas


#endif // STRESS_RESULT_H
