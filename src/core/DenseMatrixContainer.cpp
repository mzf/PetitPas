/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "DenseMatrixContainer.h"

#include <stdexcept>

namespace PetitPas
{

DenseMatrixContainer::DenseMatrixContainer(unsigned int rowCount, unsigned int columnCount)
{
    if((rowCount == 0) || (columnCount == 0))
    {
        throw std::runtime_error("Matrix dimension can not be 0.");
    }

    this->eigenMatrix.resize(rowCount, columnCount);
    this->eigenMatrix.setZero();
}

DenseMatrixContainer::~DenseMatrixContainer()
{
}

DenseMatrixContainer::DenseMatrixContainer(const DenseMatrixContainer& other)
{
    *this = other;
}

DenseMatrixContainer::DenseMatrixContainer(const MatrixContainer& other)
{
    throw std::runtime_error("not implemented");
    //this->eigenMatrix = new Eigen::MatrixXd(other.eigenMatrix);
}

DenseMatrixContainer::DenseMatrixContainer(const Eigen::MatrixXd& newEigenMatrix)
{
    if(newEigenMatrix.rows() <= 0 || newEigenMatrix.cols() <= 0)
    {
        throw std::runtime_error("The given eigen matrix has rows/columns equals zero.");
    }

    this->eigenMatrix = newEigenMatrix;
}

DenseMatrixContainer& DenseMatrixContainer::operator=(const DenseMatrixContainer& other)
{
    if (this != &other) // protect against invalid self-assignment
    {
        this->eigenMatrix = other.eigenMatrix;
    }

    return *this;
}

unsigned int DenseMatrixContainer::GetRowCount() const
{
    return this->eigenMatrix.rows();
}

unsigned int DenseMatrixContainer::GetColumnCount() const
{
    return this->eigenMatrix.cols();
}

double DenseMatrixContainer::GetValueAt(unsigned int rowIndex, unsigned int columnIndex) const
{
    if(rowIndex >= this->GetRowCount() || columnIndex >= this->GetColumnCount())
    {
        throw std::runtime_error("Invalid index in GetValueAt.");
    }

    return this->eigenMatrix(rowIndex,columnIndex);
}

void DenseMatrixContainer::SetValueAt(unsigned int rowIndex, unsigned int columnIndex, double value)
{
    if(rowIndex >= this->GetRowCount() || columnIndex >= this->GetColumnCount())
    {
        throw std::runtime_error("Invalid index in SetValueAt");
    }

    this->eigenMatrix(rowIndex,columnIndex) = value;
}


void DenseMatrixContainer::AddValueAt(unsigned int rowIndex, unsigned int columnIndex, double value)
{
    if(rowIndex >= this->GetRowCount() || columnIndex >= this->GetColumnCount())
    {
        throw std::runtime_error("Invalid index in AddValueAt");
    }

    this->eigenMatrix(rowIndex,columnIndex) += value;
}

void DenseMatrixContainer::MultiplyValueAt(unsigned int rowIndex, unsigned int columnIndex, double value)
{
    if(rowIndex >= this->GetRowCount() || columnIndex >= this->GetColumnCount())
    {
        throw std::runtime_error("Invalid index in MultiplyValueAt");
    }

    this->eigenMatrix(rowIndex,columnIndex) *= value;
}

DenseMatrixContainer DenseMatrixContainer::GetInverse() const
{
    return DenseMatrixContainer(this->eigenMatrix.inverse());
}

} //namespace PetitPas
