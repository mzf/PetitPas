/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "PolynomialFunction.h"
#include <stdexcept>

namespace PetitPas
{

PolynomialFunction::PolynomialFunction(const std::vector<PolynomialTerm>& terms)
{
    if (terms.empty())
    {
        throw std::runtime_error("The number of terms can not be 0.");
    }

    //Check the dimension of the given terms
    std::vector<PolynomialTerm>::const_iterator it = terms.begin();
    unsigned int size = it->GetDimension();
    while(it != terms.end())
    {
        if(it->GetDimension() != size)
        {
            throw std::runtime_error("The terms must all have the same dimension.");
        }

        ++it;
    }

    this->terms = terms;
}

double PolynomialFunction::GetValue(const std::vector<double>& variableValues) const
{
    if(this->terms[0].GetDimension() != variableValues.size())
    {
        throw std::runtime_error("The variable values must have the same dimension as the function's dimension.");
    }

    double result = 0.;
    for(std::vector<PolynomialTerm>::const_iterator it = this->terms.begin();
        it != this->terms.end();
        ++it)
    {
        result += it->GetValue(variableValues);
    }

    return result;
}

double PolynomialFunction::GetDerivativeValue(unsigned int byVariableIndex, const std::vector<double>& variableValues) const
{
    unsigned int dimension = this->terms[0].GetDimension();
    if(dimension != variableValues.size())
    {
        throw std::runtime_error("The size of the given variable values is not the same as the function degrees number.");
    }

    if(byVariableIndex >= dimension)
    {
        throw std::runtime_error("The variable index is out of range.");
    }

    double result = 0.;
    for(std::vector<PolynomialTerm>::const_iterator it = this->terms.begin();
        it != this->terms.end();
        ++it)
    {
        result += it->GetDerivativeValue(byVariableIndex, variableValues);
    }

    return result;

}

} //namespace PetitPas
