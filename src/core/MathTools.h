/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MATHTOOLS_H_INCLUDED
#define MATHTOOLS_H_INCLUDED


namespace PetitPas
{

/** \brief Tools for math operations.
 *
 */

class MathTools
{
public:

    /** \brief Checks that the given \a actual value is close to the \a expected value.
     *
     * \param expected The expected value.
     * \param actual The actual value.
     * \param tolerance The tolerance to check the values. The default is 1e-6.
     * \return true if \a actual is equal to \a expected +/- \a tolerance, false otherwise.
     *
     */
    static bool IsDoubleClose(double expected, double actual, double tolerance = 1e-6)
    {
        return (actual >= (expected - tolerance)) && (actual <= (expected + tolerance));
    }

};

} //namespace PetitPas

#endif // MATHTOOLS_H_INCLUDED
