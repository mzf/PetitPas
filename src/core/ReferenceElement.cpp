/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ReferenceElement.h"
#include "BasisFunctionGenerator.h"
#include "Node.h"

namespace PetitPas
{

ReferenceElement::ReferenceElement(const std::vector<Node>& nodes,
                                   const std::vector<Point>& integrationPoints,
                                   const std::vector<double>& integrationWeights) :
    nodes(nodes),
    integrationPoints(integrationPoints),
    integrationWeights(integrationWeights)

{
    if(this->nodes.empty())
    {
        throw std::runtime_error("The node list in reference element can not be empty.");
    }

    if(this->integrationPoints.empty())
    {
        throw std::runtime_error("The integration point list in reference element can not be empty.");
    }

    if(this->integrationWeights.empty())
    {
        throw std::runtime_error("The integration weight list in reference element can not be empty.");
    }

    if(this->integrationPoints.size() != this->integrationWeights.size())
    {
        throw std::runtime_error("The integration point and weight lists must have the same size.");
    }

    this->CreateBasisFunctions();
    this->CreateBasisFunctionMatrices();
}

void ReferenceElement::CreateBasisFunctions()
{
    this->basisFunctions.clear();

    this->basisFunctions = BasisFunctionGenerator::GetPolynomialFunctions(this->nodes);
}

void ReferenceElement::CreateBasisFunctionMatrices()
{
    this->integrationPointBasisFunctionDerivativeMatrices.clear();

    // For each integration point, we compute the derivate matrix of the basis functions.
    // These matrices will be used by the element to compute the Jacobian matrix of the real element.
    for(unsigned int integrationPointIndex = 0; integrationPointIndex < this->integrationPoints.size(); integrationPointIndex++)
    {
        const std::vector<double>& pointCoords = this->integrationPoints[integrationPointIndex].GetCoordinates();

        Matrix matrix = this->GetBasisFunctionMatrix(pointCoords);

        this->integrationPointBasisFunctionDerivativeMatrices.push_back(matrix);
    }

    // Same for nodes.
    for(unsigned int nodeIndex = 0; nodeIndex < this->nodes.size(); nodeIndex++)
    {
        const std::vector<double>& pointCoords = this->nodes[nodeIndex].GetCoordinates();

        Matrix matrix = this->GetBasisFunctionMatrix(pointCoords);

        this->nodeBasisFunctionDerivativeMatrices.push_back(matrix);
    }

}

Matrix ReferenceElement::GetBasisFunctionMatrix(const std::vector<double>& pointCoords) const
{
    unsigned int dimension = this->GetDimension();
    unsigned int nodeCount = this->basisFunctions.size();

    Matrix matrix(dimension, nodeCount);

    for(unsigned int coordinateIndex = 0; coordinateIndex < dimension; coordinateIndex++)
    {
        for(unsigned int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
        {
            double value = this->basisFunctions[nodeIndex].GetDerivativeValue(coordinateIndex, pointCoords);
            matrix.SetValueAt(coordinateIndex, nodeIndex, value);
        }
    }

    return matrix;
}

const std::vector<Node>& ReferenceElement::GetNodes() const
{
    return this->nodes;
}

const std::vector<Point>& ReferenceElement::GetIntegrationPoints() const
{
    return this->integrationPoints;
}

const std::vector<double>& ReferenceElement::GetIntegrationWeights() const
{
    return this->integrationWeights;
}

const std::vector<PolynomialFunction>& ReferenceElement::GetBasisFunctions() const
{
    return this->basisFunctions;
}

unsigned int ReferenceElement::GetDimension() const
{
    return this->integrationPoints[0].GetDimension();
}

const Matrix& ReferenceElement::GetBasisFunctionDerivativeMatrix(unsigned int integrationPointIndex) const
{
    if(integrationPointIndex >= this->integrationPointBasisFunctionDerivativeMatrices.size())
    {
        throw std::runtime_error("Invalid integration point index.");
    }

    return this->integrationPointBasisFunctionDerivativeMatrices[integrationPointIndex];
}

const Matrix& ReferenceElement::GetBasisFunctionDerivativeMatrixAtNode(unsigned int nodeIndex) const
{
    if(nodeIndex >= this->nodeBasisFunctionDerivativeMatrices.size())
    {
        throw std::runtime_error("Invalid node index.");
    }

    return this->nodeBasisFunctionDerivativeMatrices[nodeIndex];
}

} //namespace PetitPas

