/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef REFERENCE_ELEMENT_BUILDER_H
#define REFERENCE_ELEMENT_BUILDER_H


namespace PetitPas
{
    class ReferenceElement;

    /** \brief This is a builder for reference elements.
     *         This class is a singleton, you have to use it via the GetInstance() method.
     *
     * Example:
     * \code
     * // First get the instance.
     * ReferenceElementBuilder& referenceElementBuilder = ReferenceElementBuilder::GetInstance();
     *
     * // Then get the reference element.
     * const ReferenceElement* referenceElement = referenceElementBuilder.GetReferenceElement(3, 2);
     *
     * // Use the reference element...
     * referenceElement->GetIntegrationPoints();
     * \endcode
     */
    class ReferenceElementBuilder
    {
    public:

        /** \brief Get the unique instance of this object.
         *
         * \return A reference to the unique ReferenceElementBuilder object.
         *
         */
        static ReferenceElementBuilder& GetInstance();

        /** \brief Get the ReferenceElement associated to the given node count and dimension.
         *
         * \param nodeCount The number of nodes of the real element that we want to bind to this new reference element.
         * \param dimension The dimension of the element.
         * \return A pointer to the ReferenceElement. This instance MUST NOT be destroyed by the caller.
         *
         * \throw std::runtime_error if not reference element exist for the given \a nodeCount and \a dimension.
         */
        const ReferenceElement* GetReferenceElement(unsigned int nodeCount, unsigned int dimension);

    private:
        ReferenceElementBuilder();
        ~ReferenceElementBuilder();

        ReferenceElement* GetNewTriangle3NodesReferenceElement();
        ReferenceElement* GetNewTetrahedron4NodesReferenceElement();

        static ReferenceElementBuilder instance;

        ReferenceElement* triangle3NodesReferenceElement;
        ReferenceElement* tetrahedron4NodesReferenceElement;
    };

}//namespace PetitPas


#endif // REFERENCE_ELEMENT_BUILDER_H
