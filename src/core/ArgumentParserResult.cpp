/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ArgumentParserResult.h"
#include <stdexcept>

namespace PetitPas
{

ArgumentParserResult::ArgumentParserResult(const std::map<std::string, std::string>& parsedArguments,
                                           ExitStatus exitStatus,
                                           const std::string& errorMessage):
    arguments(parsedArguments),
    exitStatus(exitStatus),
    errorMessage(errorMessage)
{

}

bool ArgumentParserResult::ContainsArgument(const std::string& argumentName) const
{
    return this->arguments.find(argumentName) != this->arguments.end();
}

const std::string& ArgumentParserResult::GetArgumentValue(const std::string& argumentName) const
{
    std::map<std::string, std::string>::const_iterator argumentIterator = this->arguments.find(argumentName);
    if(argumentIterator == this->arguments.end())
    {
        throw std::runtime_error("The given argument name does not exist.");
    }

    return argumentIterator->second;
}

bool ArgumentParserResult::MustExit() const
{
    return this->exitStatus == MustExitStatus;
}

const std::string& ArgumentParserResult::GetErrorMessage() const
{
    return this->errorMessage;
}

} //namespace PetitPas
