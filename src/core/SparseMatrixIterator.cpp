/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "SparseMatrixIterator.h"


namespace PetitPas
{

SparseMatrixIterator::SparseMatrixIterator(const std::map<std::pair<unsigned int, unsigned int>, double>::const_iterator& wrappedIterator):
    containerIterator(wrappedIterator)
{
    std::pair<unsigned int, unsigned int> p;
}

int SparseMatrixIterator::row() const
{
    return this->containerIterator->first.first;
}


int SparseMatrixIterator::col() const
{
    return this->containerIterator->first.second;
}

double SparseMatrixIterator::value() const
{
    return this->containerIterator->second;
}

bool SparseMatrixIterator::operator!=(const SparseMatrixIterator& other) const
{
    return this->containerIterator != other.containerIterator;
}

SparseMatrixIterator SparseMatrixIterator::operator++()
{
    ++(this->containerIterator);
    return (*this);
}

const SparseMatrixIterator* SparseMatrixIterator::operator->() const
{
    return this;
}

} //namespace PetitPas
