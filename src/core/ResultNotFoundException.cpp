/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ResultNotFoundException.h"

#include <sstream>

namespace PetitPas
{

ResultNotFoundException::ResultNotFoundException() : exception()
{
    this->InitializeMessage(-1, -1);
}

ResultNotFoundException::ResultNotFoundException(int elementId, int nodeId) : exception()
{
    this->InitializeMessage(elementId, nodeId);
}

ResultNotFoundException::~ResultNotFoundException() throw()
{
}

void ResultNotFoundException::InitializeMessage(int elementId, int nodeId)
{
    std::stringstream messageStream;
    messageStream << "Result not found";
    if(elementId >= 0)
        messageStream << " at element " << elementId;
    if(nodeId >= 0)
        messageStream << " at node " << nodeId;

    this->message = messageStream.str();
}

const char* ResultNotFoundException::what() const throw()
{
    return this->message.c_str();
}

} //namespace PetitPas
