/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef POLYNOMIALFUNCTION_H
#define POLYNOMIALFUNCTION_H

#include "PolynomialTerm.h"

namespace PetitPas
{
    /** \brief Represents a polynomial function.
     */
    class PolynomialFunction
    {
    public:
        /** \brief PolynomialFunction constructor.
         *
         * \param terms A list of PolynomialTerm objects. The terms must have the same dimension.
         *
         * \throw std::runtime_error if the terms don't have the same dimension or if the list is empty.
         */
        explicit PolynomialFunction(const std::vector<PolynomialTerm>& terms);

        /** \brief Evaluator of the function.
         *
         * \param variableValues The list of value for each variable of the term.
         * \return The result of the evaluation of the term with the given values.
         *
         * \throw std::runtime_error if the size of \a variableValues is not the same as the term variable number.
         */
        double GetValue(const std::vector<double>& variableValues) const;

        /** \brief Evaluate the derivative value of this function at a given point.
         *
         * \param byVariableIndex index of the variable index that starts at 0. For instance if you have 3 variables (x, y, z)
         *                        and you want the derivative by y, this index must be 1.
         * \param variableValues The list of value for each variable of the term.
         *
         * \return The value of the derivative of the function at the given variable values by the variable index.
         *
         * \throw std::runtime_error if the size of \a variableValues is not the same as the function dimension,
         *                           or if the variable index is out of range (0..dimension-1).
         */
        double GetDerivativeValue(unsigned int byVariableIndex, const std::vector<double>& variableValues) const;

    private:
        std::vector<PolynomialTerm> terms;
    };

}

#endif // POLYNOMIALFUNCTION_H
