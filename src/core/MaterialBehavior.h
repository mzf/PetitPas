/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MATERIALBEHAVIOR_H
#define MATERIALBEHAVIOR_H


namespace PetitPas
{
    class Matrix;

    /** \brief This class represents the behavior of a material. Only Hooke elastic law at the moment.
     */
    class MaterialBehavior
    {
    public:

        enum MaterialHypothesis
        {
            MaterialHypothesis_2DPlaneStress,
            MaterialHypothesis_2DPlaneStrain,
            MaterialHypothesis_3D
        };

        /** \brief Constructor of the material behavior.
         *
         * \param youngModulus Elasticity modulus (Young's modulus).
         * \param poissonRatio Poisson's ratio.
         * \param materialHypothesis The behavior hypothesis for this material. \sa MaterialHypothesis.
         *
         * \throw std::runtime_error if \a poissonRatio is not between -1 and 0.5.
         */
        MaterialBehavior(double youngModulus, double poissonRatio, MaterialHypothesis materialHypothesis);

        /** \brief Destructor.
         *
         */
        ~MaterialBehavior();

        /** \brief Get the Hooke matrix of this material.
         *
         * \return Matrix The Hooke matrix. The dimension of the matrix depends on the hypothesis.
         *         In 2D, the dimensions are 3 by 3 and in 3D they are 6 by 6.
         *
         */
        const Matrix& GetHookeMatrix() const;

        /** \brief Get the hypothesis associated with this material.
         *
         * \return a MaterialHypothesis value.
         *
         */
        MaterialHypothesis GetHypothesis() const;

    private:

        void InitializeHookeMatrix();

        double youngModulus;
        double poissonRatio;
        MaterialHypothesis hypothesis;
        Matrix* hookeMatrix;

    };

}//namespace PetitPas


#endif // MATERIALBEHAVIOR_H
