/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Node.h"

namespace PetitPas
{

Node::Node(const Point &location, unsigned int id) :
    location(location),
    id(id)
{
}

Node::~Node() { }

const Point& Node::GetLocation() const
{
    return this->location;
}

unsigned int Node::GetId() const
{
    return this->id;
}

const std::vector<double>& Node::GetCoordinates() const
{
    return this->location.GetCoordinates();
}

unsigned int Node::GetDimension() const
{
    return this->location.GetDimension();
}

} //namespace PetitPas
