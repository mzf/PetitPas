/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "ReferenceElementBuilder.h"
#include "ReferenceElement.h"
#include "Node.h"

#include <sstream>
#include <cmath>

namespace PetitPas
{

ReferenceElementBuilder ReferenceElementBuilder::instance = ReferenceElementBuilder();

ReferenceElementBuilder& ReferenceElementBuilder::GetInstance()
{
    return instance;
}

ReferenceElementBuilder::ReferenceElementBuilder():
    triangle3NodesReferenceElement(NULL),
    tetrahedron4NodesReferenceElement(NULL)
{

}

ReferenceElementBuilder::~ReferenceElementBuilder()
{
    delete triangle3NodesReferenceElement;
    delete tetrahedron4NodesReferenceElement;
}

const ReferenceElement* ReferenceElementBuilder::GetReferenceElement(unsigned int nodeCount, unsigned int dimension)
{
    if((nodeCount == 3) and (dimension == 2))
    {
        if(this->triangle3NodesReferenceElement == NULL)
        {
            this->triangle3NodesReferenceElement = this->GetNewTriangle3NodesReferenceElement();
        }

        return this->triangle3NodesReferenceElement;
    }

    if((nodeCount == 4) and (dimension == 3))
    {
        if(this->tetrahedron4NodesReferenceElement == NULL)
        {
            this->tetrahedron4NodesReferenceElement = this->GetNewTetrahedron4NodesReferenceElement();
        }

        return this->tetrahedron4NodesReferenceElement;
    }

    std::stringstream message;
    message << "Unsupported element type with ";
    message << nodeCount << " nodes in ";
    message << dimension << "D.";
    throw std::runtime_error(message.str());
}

ReferenceElement* ReferenceElementBuilder::GetNewTriangle3NodesReferenceElement()
{
    std::vector<Node> nodes;
    std::vector<Point> integrationPoints;
    std::vector<double> integrationWeights;

    //Nodes
    std::vector<double> coords(2);
    coords[0] = 0.;
    coords[1] = 0.;
    Point point1(coords);
    nodes.push_back(Node(point1, 1));

    coords[0] = 1.;
    coords[1] = 0.;
    Point point2(coords);
    nodes.push_back(Node(point2, 2));

    coords[0] = 0.;
    coords[1] = 1.;
    Point point3(coords);
    nodes.push_back(Node(point3, 3));

    // integrate with 3 Hammer points.
    coords[0] = 1.0/6.0;
    coords[1] = 1.0/6.0;
    Point integrationPoint1(coords);
    integrationPoints.push_back(integrationPoint1);
    integrationWeights.push_back(1.0/6.0);

    coords[0] = 2.0/3.0;
    coords[1] = 1.0/6.0;
    Point integrationPoint2(coords);
    integrationPoints.push_back(integrationPoint2);
    integrationWeights.push_back(1.0/6.0);

    coords[0] = 1.0/6.0;
    coords[1] = 2.0/3.0;
    Point integrationPoint3(coords);
    integrationPoints.push_back(integrationPoint3);
    integrationWeights.push_back(1.0/6.0);

    return new ReferenceElement(nodes, integrationPoints, integrationWeights);

}

ReferenceElement* ReferenceElementBuilder::GetNewTetrahedron4NodesReferenceElement()
{
    std::vector<Node> nodes;
    std::vector<Point> integrationPoints;
    std::vector<double> integrationWeights;

    //Nodes
    std::vector<double> coords(3);
    coords[0] = 0.;
    coords[1] = 0.;
    coords[2] = 0.;
    Point point1(coords);
    nodes.push_back(Node(point1, 1));

    coords[0] = 1.;
    coords[1] = 0.;
    coords[2] = 0.;
    Point point2(coords);
    nodes.push_back(Node(point2, 2));

    coords[0] = 0.;
    coords[1] = 1.;
    coords[2] = 0.;
    Point point3(coords);
    nodes.push_back(Node(point3, 3));

    coords[0] = 0.;
    coords[1] = 0.;
    coords[2] = 1.;
    Point point4(coords);
    nodes.push_back(Node(point4, 4));

    // integrate with 4 points.
    double a = (5.0 - std::sqrt(5.0)) / 20.0;
    double b = (5.0 + 3 * std::sqrt(5.0)) / 20.0;
    double w = 1.0/24.0;

    coords[0] = a;
    coords[1] = a;
    coords[2] = a;
    Point integrationPoint1(coords);
    integrationPoints.push_back(integrationPoint1);
    integrationWeights.push_back(w);

    coords[0] = a;
    coords[1] = a;
    coords[2] = b;
    Point integrationPoint2(coords);
    integrationPoints.push_back(integrationPoint2);
    integrationWeights.push_back(w);

    coords[0] = a;
    coords[1] = b;
    coords[2] = a;
    Point integrationPoint3(coords);
    integrationPoints.push_back(integrationPoint3);
    integrationWeights.push_back(w);

    coords[0] = b;
    coords[1] = a;
    coords[2] = a;
    Point integrationPoint4(coords);
    integrationPoints.push_back(integrationPoint4);
    integrationWeights.push_back(w);

    return new ReferenceElement(nodes, integrationPoints, integrationWeights);
}

} //namespace PetitPas
