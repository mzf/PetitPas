/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef DENSE_MATRIX_CONTAINER_H
#define DENSE_MATRIX_CONTAINER_H

#include "MatrixContainer.h"
#include <Eigen/Dense>

namespace PetitPas
{

    /** \brief The Dense Matrix Container store data in an array of dimension rowCount*columnCount
     *         The implementation uses the Eigen's Dense Matrix.
     */
    class DenseMatrixContainer : public MatrixContainer
    {
    public:

        /** \brief Constructor.
         *
         * \param rowCount Number of row for the matrix.
         * \param columnCount Number of column for the matrix
         *
         * The matrix dimensions are set to \a rowCount and \a columnCount,
         * and every terms of the matrix are initialized at 0.
         *
         * \throw std::runtime_error if \a rowCount or \a columnCount are 0.
         */
        DenseMatrixContainer(unsigned int rowCount, unsigned int columnCount);

        /** \brief Destructor.
         */
        ~DenseMatrixContainer();

        /** \brief DenseMatrixContainer Copy Constructor.
         *
         * \param other The reference DenseMatrixContainer to copy.
         *
         * The matrix dimensions and data are copied from the \a other DenseMatrixContainer.
         */
        DenseMatrixContainer(const DenseMatrixContainer& other);

        /** \brief DenseMatrixContainer Constructor from a generic MatrixContainer.
         *
         * \param other The reference MatrixContainer to copy.
         *
         */
        DenseMatrixContainer(const MatrixContainer& other);

        /** \brief The assignment operator.
         *
         * \param other The other DenseMatrixContainer object to copy.
         * \return A DenseMatrixContainer reference to this object.
         *
         */
        DenseMatrixContainer& operator= (const DenseMatrixContainer& other);

        /** \brief Returns the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \return The value at the given indexes.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        double GetValueAt(unsigned int rowIndex, unsigned int columnIndex) const;

        /** \brief Set the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to assign.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        void SetValueAt(unsigned int rowIndex, unsigned int columnIndex, double value);

        /** \brief Add a value to the value at the given row and column indexes.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to add.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        void AddValueAt(unsigned int rowIndex, unsigned int columnIndex, double value);

        /** \brief Multiply the value at the given position by the given value.
         *
         * \param rowIndex Row index that starts at 0.
         * \param columnIndex Column index that starts at 0.
         * \param value The value to multiply to the existing value.
         *
         * \throw std::runtime_error if \a rowIndex or \a columnIndex are beyond matrix dimensions.
         */
        void MultiplyValueAt(unsigned int rowIndex, unsigned int columnIndex, double value);

        /** \brief Returns the number of row of the DenseMatrixContainer.
         *
         * \return The number of row of the matrix.
         */
        unsigned int GetRowCount() const;

        /** \brief Returns the number of column of the DenseMatrixContainer.
         *
         * \return The number of column of the matrix.
         */
        unsigned int GetColumnCount() const;

        /** \brief Compute and get the inverse matrix.
         *
         * \return A MatrixContainer that is the inverse of the current MatrixContainer.
         *
         * The actual method is based on the Eigen inverse() method.
         * Do not use with big matrices or to solve problem.
         *
         * \throw std::runtime_error if the matrix is not square or if it is not invertible.
         */
        DenseMatrixContainer GetInverse() const;

    private:

        /** \brief Constructor with an Eigen matrix.
         *
         * \param newEigenMatrix An Eigen matrix.
         *
         * The matrix will be initialized with the given Eigen matrix.
         *
         * \throw std::runtime_error if \a rowCount or \a columnCount are 0.
         */
        DenseMatrixContainer(const Eigen::MatrixXd& newEigenMatrix);

        Eigen::MatrixXd eigenMatrix;
    };

}//namespace PetitPas


#endif // DENSE_MATRIX_CONTAINER_H
