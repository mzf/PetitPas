/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ELEMENT_ID_NODE_ID_PAIR_H
#define ELEMENT_ID_NODE_ID_PAIR_H

#include <vector>

namespace PetitPas
{

    /** \brief Small class that handle a key that is a combination of element id and node id.
     *         See StrainResult or StressResult class for use of this object.
     *
     *  \remarks There is no check of the validity of the given ids. This class is just a container.
     */
    class ElementIdNodeIdPair
    {
    public:
        /** \brief Pair constructor.
         *
         * \param elementId The element id of the pair.
         * \param nodeId The node id of the pair.
         *
         * \remarks There is no check of the validity of the given ids. This class is just a container.
         */
        ElementIdNodeIdPair(unsigned int elementId, unsigned int nodeId);


        /** \brief Less operator. Useful if this class is used in a std::map container.
         *
         * \param other The other ElementIdNodeIdPair to compare to this pair.
         *
         * \return true if this element id is inferior to \a other element id.
         *              If the element ids are equal, node id is used for comparison.
         *
         * \example In the text below, (a, b) means a pair of element with id 'a' and node with id 'b':
         * \code
         * (1, 2) < (3, 2) => true
         * (1, 2) < (1, 2) => false
         * (3, 2) < (1, 2) => false
         * (1, 1) < (1, 2) => true
         * (1, 2) < (1, 1) => false
         * \endcode
         */
        bool operator< (const ElementIdNodeIdPair& other) const;

    private:
        unsigned int elementId;
        unsigned int nodeId;
    };


}//namespace PetitPas


#endif // ELEMENT_ID_NODE_ID_PAIR_H
