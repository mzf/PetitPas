/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RESULT_NOT_FOUND_EXCEPTION_H
#define RESULT_NOT_FOUND_EXCEPTION_H

#include <exception>
#include <string>

namespace PetitPas
{

    /** \brief An exception to represent the lack of result at a given element and node id.
     */
    class ResultNotFoundException : public std::exception
    {
    public:

        /** \brief Constructor with no parameter.
         *         If you want to precise an element or a node, use the appropriate constructor.
         *
         */
        ResultNotFoundException();

        /** \brief Constructor with an element and a node.
         *
         * \param elementId Id of the element when the error occurred. If negative, the error message will not mention this id.
         * \param nodeId Id of the node where the error occurred. If negative, the error message will not mention this id.
         *
         */
        ResultNotFoundException(int elementId, int nodeId);

        /** \brief Exception destructor.
         *
         */
        ~ResultNotFoundException() throw();

        /** \brief Overrides the standard exception message.
         *
         * \return Message for this exception.
         *
         */
        const char* what() const throw();

    private:

        void InitializeMessage(int elementId, int nodeId);

        std::string message;

    };

}//namespace PetitPas


#endif // RESULT_NOT_FOUND_EXCEPTION_H
