/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef STRAIN_RESULT_H
#define STRAIN_RESULT_H

#include "ElementIdNodeIdPair.h"
#include <map>

namespace PetitPas
{
    class Solution;
    class Matrix;
    class Element;

    /** \brief The Strain Result.
     *         The computation of this result is based on the basis function derivatives an element.
     * \code
     * [Strain] = [B] * [U]
     * \endcode
     * with [B] the matrix of basis function derivatives of the element and [U] the nodal displacement vector.
     */
    class StrainResult
    {
    public:
        /** \brief Constructor from a solution object.
         *
         * \param solution A Solution object from a solved analysis.
         *
         */
        StrainResult(const Solution& solution);

        /** \brief Get the strain at an element's node.
         *
         * \param elementId Id of the element we want to process.
         *                  The solution's mesh must contains this element.
         * \param nodeId Id of the node of the element where we want the strain result.
         *               The \a elementId element must contains this node Id.
         * \return A vertical Matrix object that contains the strain values at the given node for the given element.
         *
         * \throw ResultNotFoundException if there is not result associated to the given pair \a elementId \a nodeId.
         */
        const Matrix& GetElementalStrainAtNode(unsigned int elementId, unsigned int nodeId) const;

        /** \brief Get the elements that have a strain result.
         *
         * \return A list of pointer to Element objects.
         *
         */
        const std::vector<Element*>& GetElements() const;

    private:

        const Solution& solution;
        std::map<ElementIdNodeIdPair, Matrix> strainAtElementNode;
        std::vector<Element*> elements;

    };

}//namespace PetitPas


#endif // STRAIN_RESULT_H
