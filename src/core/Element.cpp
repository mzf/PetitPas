/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "Element.h"
#include "ReferenceElementBuilder.h"
#include "ReferenceElement.h"
#include "MaterialBehavior.h"
#include "MatrixNotInvertibleException.h"

#include <stdexcept>
#include <sstream>

namespace PetitPas
{

Element::Element(const std::vector<Node*>& elementNodes, unsigned int elementId, const MaterialBehavior& materialBehavior) :
    id(elementId),
    materialBehavior(&materialBehavior)
{
    if(elementNodes.empty())
    {
        throw std::runtime_error("The element's node list can not be empty.");
    }

    //Check nodes dimension
    std::vector<Node*>::const_iterator nodeIterator = elementNodes.begin();
    unsigned int dimension = (*nodeIterator)->GetDimension();
    while(nodeIterator != elementNodes.end())
    {
        if(dimension != (*nodeIterator)->GetDimension())
        {
            throw std::runtime_error("The element nodes dimensions are not the same.");
        }
        ++nodeIterator;
    }

    this->nodes = elementNodes;

    this->CheckMaterialHypothesis();

    ReferenceElementBuilder& referenceElementBuilder = ReferenceElementBuilder::GetInstance();
    this->referenceElement = referenceElementBuilder.GetReferenceElement(this->GetNodeCount(), this->GetDimension());
}

Element::~Element()
{
}

unsigned int Element::GetNodeCount() const
{
    return this->nodes.size();
}

unsigned int Element::GetDimension() const
{
    return this->nodes[0]->GetDimension();
}

const std::vector<Node*>& Element::GetNodes() const
{
    return this->nodes;
}

unsigned int Element::GetId() const
{
    return this->id;
}

const MaterialBehavior* Element::GetMaterialBehavior() const
{
    return this->materialBehavior;
}

Matrix Element::GetRawStiffnessMatrix() const
{
    return this->GetStiffnessMatrix().GetRawMatrix();
}

StiffnessMatrix Element::GetStiffnessMatrix() const
{
    //Get the material behavior Hooke matrix
    const Matrix& behaviorMatrix =  this->materialBehavior->GetHookeMatrix();

    const std::vector<Point>& integrationPoints = this->referenceElement->GetIntegrationPoints();
    const std::vector<double>& weights = this->referenceElement->GetIntegrationWeights();

    // Get basis functions
    const std::vector<PolynomialFunction>& basisFunctions = this->referenceElement->GetBasisFunctions();

    // The future stiffness matrix.
    unsigned int dimension = this->GetDimension();
    unsigned int nodeStressComponentCount = behaviorMatrix.GetRowCount();
    unsigned int unknownsNumber = this->GetDegreesOfFreedomNumber();
    Matrix rawStiffnessMatrix(unknownsNumber, unknownsNumber);

    for(unsigned int pointIndex = 0; pointIndex < integrationPoints.size(); pointIndex++)
    {
        // Get integration point and weight.
        std::vector<double> integrationPoint = integrationPoints[pointIndex].GetCoordinates();
        double weight = weights[pointIndex];

        // Compute the Jacobian Matrix determinant
        Matrix jacobianMatrix = this->GetJacobianMatrixAtIntegrationPoint(pointIndex);
        Matrix inverseJacobianMatrix = this->GetInverseJacobianMatrix(jacobianMatrix);
        double jacobianDeterminant = jacobianMatrix.GetDeterminant();

        // Compute B Matrix (basis derivative functions)
        Matrix B(nodeStressComponentCount, unknownsNumber);
        for(unsigned int nodeIndex = 0; nodeIndex < this->GetNodeCount(); nodeIndex++)
        {
            if(dimension == 2)
            {
                this->Fill2DBasisDerivativeFunctionMatrixAtNode(nodeIndex, basisFunctions, integrationPoint, inverseJacobianMatrix, B);
            }
            else if(dimension == 3)
            {
                this->Fill3DBasisDerivativeFunctionMatrixAtNode(nodeIndex, basisFunctions, integrationPoint, inverseJacobianMatrix, B);
            }
            else
            {
                throw std::runtime_error("Unknown dimension when computing stiffness matrix.");
            }
        }

        // Compute stiffness at integration point
        // Bt * behaviorMatrix * B
        Matrix integrationStiffnessMatrix = this->GetBTransposedByCByB(B, behaviorMatrix);

        //Multiply by weight and Jacobian determinant
        integrationStiffnessMatrix.MultiplyBy(weight * jacobianDeterminant);

        // Add it to element stiffness matrix
        rawStiffnessMatrix = rawStiffnessMatrix + integrationStiffnessMatrix;
    }

    // Create the StiffnessMatrix object
    StiffnessMatrix stiffnessMatrix(rawStiffnessMatrix, this->nodes);
    return stiffnessMatrix;

}

void Element::Fill2DBasisDerivativeFunctionMatrixAtNode(unsigned int nodeIndex,
                                                        const std::vector<PolynomialFunction>& basisFunctions,
                                                        const std::vector<double>& computationPoint,
                                                        const Matrix& inverseJacobianMatrix,
                                                        Matrix& B) const
{
    // In 2D, the [B] matrix is:
    //
    // [ dN1/dx  0       dN2/dx  0       dN3/dx  0       ...
    //   0       dN1/dy  0       dN2/dy  0       dN3/dy  ...
    //   dN1/dy  dN1/dx  dN2/dy  dN2/dx  dN3/dy  dN3/dx  ... ]
    //
    // with:
    // [ dNi/dx   = [J^-1] * [ dNi/de
    //   dNi/dy ]              dNi/dn ]

    //Derivatives on reference element
    double dNde = basisFunctions[nodeIndex].GetDerivativeValue(0, computationPoint);
    double dNdn = basisFunctions[nodeIndex].GetDerivativeValue(1, computationPoint);
    Matrix intrinsicDerivatives(2,1);
    intrinsicDerivatives.SetValueAt(0,0, dNde);
    intrinsicDerivatives.SetValueAt(1,0, dNdn);

    //Use inverse of jacobian to find the real derivatives
    Matrix realDerivatives = inverseJacobianMatrix * intrinsicDerivatives;

    //dN/dx
    double dndx = realDerivatives.GetValueAt(0,0);

    //dN/dy
    double dndy = realDerivatives.GetValueAt(1,0);

    //Fill the matrix
    B.SetValueAt(0, nodeIndex*2, dndx);
    B.SetValueAt(1, nodeIndex*2 + 1, dndy);
    B.SetValueAt(2, nodeIndex*2, dndy);
    B.SetValueAt(2, nodeIndex*2 + 1, dndx);
}

void Element::Fill3DBasisDerivativeFunctionMatrixAtNode(unsigned int nodeIndex,
                                                        const std::vector<PolynomialFunction>& basisFunctions,
                                                        const std::vector<double>& computationPoint,
                                                        const Matrix& inverseJacobianMatrix,
                                                        Matrix& B) const
{
    // In 3D, the [B] matrix is:
    //
    // [ dN1/dx  0       0       | dN2/dx  0       0      | ...
    //   0       dN1/dy  0       | 0       dN2/dy  0      | ...
    //   0       0       dN1/dz  | 0       0       dN2/dz | ...
    //   dN1/dy  dN1/dx  0       | dN2/dy  dN2/dx  0      | ...
    //   dN1/dz  0       dN1/dx  | dN2/dz  0       dN2/dx | ...
    //   0       dN1/dz  dN1/dy  | 0       dN2/dz  dN2/dy | ... ]
    //
    // with:
    // [ dNi/dx   = [J^-1] * [ dNi/de
    //   dNi/dy ]              dNi/dn ]
    //   dNi/dz ]              dNi/dm ]

    //Derivatives on reference element
    double dNde = basisFunctions[nodeIndex].GetDerivativeValue(0, computationPoint);
    double dNdn = basisFunctions[nodeIndex].GetDerivativeValue(1, computationPoint);
    double dNdm = basisFunctions[nodeIndex].GetDerivativeValue(2, computationPoint);
    Matrix intrinsicDerivatives(3,1);
    intrinsicDerivatives.SetValueAt(0,0, dNde);
    intrinsicDerivatives.SetValueAt(1,0, dNdn);
    intrinsicDerivatives.SetValueAt(2,0, dNdm);

    //Use inverse of jacobian to find the real derivatives
    Matrix realDerivatives = inverseJacobianMatrix * intrinsicDerivatives;

    //dN/dx
    double dndx = realDerivatives.GetValueAt(0,0);

    //dN/dy
    double dndy = realDerivatives.GetValueAt(1,0);

    //dN/dz
    double dndz = realDerivatives.GetValueAt(2,0);

    //Fill the matrix
    B.SetValueAt(0, nodeIndex*3, dndx);
    B.SetValueAt(1, nodeIndex*3 + 1, dndy);
    B.SetValueAt(2, nodeIndex*3 + 2, dndz);
    B.SetValueAt(3, nodeIndex*3, dndy);
    B.SetValueAt(3, nodeIndex*3 + 1, dndx);
    B.SetValueAt(4, nodeIndex*3, dndz);
    B.SetValueAt(4, nodeIndex*3 + 2, dndx);
    B.SetValueAt(5, nodeIndex*3 + 1, dndz);
    B.SetValueAt(5, nodeIndex*3 + 2, dndy);
}


Matrix Element::GetNodeCoordinatesMatrix() const
{
    // Compute Node Coordinates Matrix for Jacobian computation
    Matrix nodeCoordinatesMatrix(this->GetNodeCount(), this->GetDimension());
    for(unsigned int nodeIndex = 0; nodeIndex < this->GetNodeCount(); nodeIndex++)
    {
        const std::vector<double>& nodeCoordinates = this->nodes[nodeIndex]->GetCoordinates();

        for(unsigned int dimensionIndex = 0; dimensionIndex < this->GetDimension(); dimensionIndex++)
        {
            nodeCoordinatesMatrix.SetValueAt(nodeIndex, dimensionIndex, nodeCoordinates[dimensionIndex]);
        }
    }

    return nodeCoordinatesMatrix;
}

Matrix Element::GetFieldDerivativeValue(const Matrix& nodalValues, unsigned int targetNodeIndex)
{
    if(targetNodeIndex >= this->GetNodeCount())
    {
        throw std::runtime_error("Bad target node index.");
    }

    if((nodalValues.GetColumnCount() != 1) || (nodalValues.GetRowCount() != this->GetDegreesOfFreedomNumber()))
    {
        throw std::runtime_error("The given nodal vector does not have the right dimensions.");
    }

    unsigned int nodeStressComponentCount = this->materialBehavior->GetHookeMatrix().GetRowCount();
    unsigned int unknownsNumber = this->GetDegreesOfFreedomNumber();

    // Compute the Jacobian Matrix Inverse and determinant.
    unsigned int dimension = this->GetDimension();
    Matrix jacobianMatrix = this->GetJacobianMatrixAtNode(targetNodeIndex);
    Matrix inverseJacobianMatrix = this->GetInverseJacobianMatrix(jacobianMatrix);

    // Get Basis Functions of the reference element.
    const std::vector<PolynomialFunction>& basisFunctions = this->referenceElement->GetBasisFunctions();

    // Compute B Matrix (basis derivative functions);
    // See method GetStiffnessMatrix() for more informations.
    Matrix DerivativeMatrix(nodeStressComponentCount, unknownsNumber);
    const std::vector<Node>& referenceElementNodes = this->referenceElement->GetNodes();
    for(unsigned int nodeIndex = 0; nodeIndex < this->GetNodeCount(); nodeIndex++)
    {
        const std::vector<double>& nodeCoordinates = referenceElementNodes[nodeIndex].GetCoordinates();
        if(dimension == 2)
        {
            this->Fill2DBasisDerivativeFunctionMatrixAtNode(nodeIndex, basisFunctions, nodeCoordinates, inverseJacobianMatrix, DerivativeMatrix);
        }
        else if(dimension == 3)
        {
            this->Fill3DBasisDerivativeFunctionMatrixAtNode(nodeIndex, basisFunctions, nodeCoordinates, inverseJacobianMatrix, DerivativeMatrix);
        }
        else
        {
            throw std::runtime_error("Unknown dimension when computing element field derivative value.");
        }
    }

    return DerivativeMatrix * nodalValues;
}

Matrix Element::GetJacobianMatrixAtIntegrationPoint(unsigned int integrationPointIndex) const
{
    return this->referenceElement->GetBasisFunctionDerivativeMatrix(integrationPointIndex) * this->GetNodeCoordinatesMatrix();
}

Matrix Element::GetJacobianMatrixAtNode(unsigned int nodeIndex) const
{
    return this->referenceElement->GetBasisFunctionDerivativeMatrixAtNode(nodeIndex) * this->GetNodeCoordinatesMatrix();
}

unsigned int Element::GetDegreesOfFreedomNumber() const
{
    // Only Structural for now: number of degree of freedom per node is node's dimension.
    return this->GetNodeCount() * this->GetDimension();
}

void Element::CheckMaterialHypothesis()
{
    MaterialBehavior::MaterialHypothesis hypothesis = this->materialBehavior->GetHypothesis();
    unsigned int dimension = this->GetDimension();

    bool badDimension = false;
    switch(hypothesis)
    {
    case MaterialBehavior::MaterialHypothesis_2DPlaneStrain:
    case MaterialBehavior::MaterialHypothesis_2DPlaneStress:
        badDimension = (dimension != 2);
        break;
    case MaterialBehavior::MaterialHypothesis_3D:
        badDimension = (dimension != 3);
        break;
    default:
        throw std::runtime_error("Unknown material hypothesis.");
    }

    if(badDimension)
    {
        throw std::runtime_error("The element material hypothesis is not compatible with the element dimension.");
    }
}

Matrix Element::GetInverseJacobianMatrix(const Matrix& jacobianMatrix) const
{
    try
    {
        return jacobianMatrix.GetInverse();
    }
    catch(MatrixNotInvertibleException& e)
    {
        // Customize the message.
        std::stringstream message;
        message << "The jacobian matrix of element " << this->GetId();
        message << " is not invertible. Check the shape of the element.";
        throw MatrixNotInvertibleException(message.str());
    }
}

Matrix Element::GetBTransposedByCByB(const Matrix& B, const Matrix& C) const
{
    //This method computes Bt*C*B without calculating B transposed.

    Matrix CB = C*B;

    unsigned int resultSize = B.GetColumnCount();
    Matrix resultMatrix(resultSize, resultSize);

    unsigned int rowCount = B.GetRowCount();
    for(unsigned int cbColumnIndex=0; cbColumnIndex < resultSize; cbColumnIndex++)
    {
        for(unsigned int bColumnIndex=0; bColumnIndex < resultSize; bColumnIndex++)
        {
            double value = 0.0;
            for(unsigned int rowIndex=0; rowIndex < rowCount; rowIndex++)
            {
                value += B.GetValueAt(rowIndex, bColumnIndex) * CB.GetValueAt(rowIndex, cbColumnIndex);
            }

            resultMatrix.SetValueAt(bColumnIndex, cbColumnIndex, value);
        }
    }

    return resultMatrix;
}

} //namespace PetitPas
