/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "BasisFunctionGenerator.h"
#include "Matrix.h"

#include <sstream>

namespace PetitPas
{

std::vector<PolynomialFunction> BasisFunctionGenerator::GetPolynomialFunctions(const std::vector<Node*>& nodes)
{
    if(nodes.empty())
    {
        throw std::runtime_error("The node list is empty!");
    }

    //TODO: Check input nodes dimensions

    unsigned int nodeCount = nodes.size();
    unsigned int dimension = nodes[0]->GetDimension();

    //Get the function terms for this element.
    std::vector<PolynomialTerm> terms = BasisFunctionGenerator::GetTerms(nodeCount, dimension);
    unsigned int termCount = terms.size();

    //Find the coefficients for each term according to element nodes positions.
    Matrix coefficientMatrix(nodeCount, termCount);
    for(unsigned int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
    {
        //Extract node coordinates.
        PetitPas::Node* currentNode = nodes[nodeIndex];
        const std::vector<double>& coords = currentNode->GetCoordinates();

        //Get the value of each term at the node coordinates.
        for(unsigned int termIndex = 0; termIndex < termCount; termIndex++)
        {
            double value = terms[termIndex].GetValue(coords);
            coefficientMatrix.SetValueAt(nodeIndex, termIndex, value);
        }
    }

    //Inverse the matrix to get the coefficients.
    Matrix resultMatrix = coefficientMatrix.GetInverse();

    //Create the basis function for each node with the computed coefficients.
    std::vector<PolynomialFunction> basisFunctions;
    for(unsigned int nodeIndex = 0; nodeIndex < nodeCount; nodeIndex++)
    {
        std::vector<PolynomialTerm> functionTerms;
        for(unsigned int termIndex = 0; termIndex < termCount; termIndex++)
        {
            double coefficient = resultMatrix.GetValueAt(termIndex, nodeIndex);
            terms[termIndex].SetCoefficient(coefficient);
            functionTerms.push_back(terms[termIndex]);
        }
        PolynomialFunction nodeFunction(functionTerms);
        basisFunctions.push_back(nodeFunction);
    }

    return basisFunctions;
}

std::vector<PolynomialFunction> BasisFunctionGenerator::GetPolynomialFunctions(const Element& element)
{
    return BasisFunctionGenerator::GetPolynomialFunctions(element.GetNodes());
}

std::vector<PolynomialFunction> BasisFunctionGenerator::GetPolynomialFunctions(std::vector<Node>& nodes)
{
    // Create a node pointer list based on the given node list.
    std::vector<Node*> nodePointers;
    nodePointers.reserve(nodes.size());
    for(std::vector<Node>::iterator nodeIterator = nodes.begin();
        nodeIterator != nodes.end();
        ++nodeIterator)
    {
        nodePointers.push_back(&(*nodeIterator));
    }

    return BasisFunctionGenerator::GetPolynomialFunctions(nodePointers);
}

std::vector<PolynomialTerm> BasisFunctionGenerator::GetTerms(unsigned int nodeCount, unsigned int dimension)
{
    std::vector<PolynomialTerm> terms;

    if(nodeCount == 3 && dimension == 2)
    {
        // 2D Triangle: f(x, y) = a + b*x + c*y;
        std::vector<int> degrees(2);
        degrees[0] = 0;
        degrees[1] = 0;
        PolynomialTerm term1(degrees);
        degrees[0] = 1;
        degrees[1] = 0;
        PolynomialTerm term2(degrees);
        degrees[0] = 0;
        degrees[1] = 1;
        PolynomialTerm term3(degrees);

        terms.push_back(term1);
        terms.push_back(term2);
        terms.push_back(term3);
    }
    else if(nodeCount == 4 && dimension == 3)
    {
        // 3D Tetrahedron: f(x, y, z) = a + b*x + c*y + d*z;
        std::vector<int> degrees(3);
        degrees[0] = 0;
        degrees[1] = 0;
        degrees[2] = 0;
        PolynomialTerm term1(degrees);
        degrees[0] = 1;
        degrees[1] = 0;
        degrees[2] = 0;
        PolynomialTerm term2(degrees);
        degrees[0] = 0;
        degrees[1] = 1;
        degrees[2] = 0;
        PolynomialTerm term3(degrees);
        degrees[0] = 0;
        degrees[1] = 0;
        degrees[2] = 1;
        PolynomialTerm term4(degrees);

        terms.push_back(term1);
        terms.push_back(term2);
        terms.push_back(term3);
        terms.push_back(term4);
    }
    else
    {
        std::stringstream message;
        message << "Can not generate basis function for nodeCount=" << nodeCount;
        message << " and dimension=" << dimension << ".";
        throw std::runtime_error(message.str());
    }


    return terms;
}

} //namespace PetitPas
