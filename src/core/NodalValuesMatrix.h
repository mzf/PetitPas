/*
    This file is a part of the PetitPas Project
    Copyright (C) 2014  Francois Mazen

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef NODAL_VALUES_MATRIX_H
#define NODAL_VALUES_MATRIX_H

#include <vector>
#include <map>

namespace PetitPas
{
    // Forward declarations.
    class Matrix;
    class Node;

    class NodalValuesMatrix
    {
    public:

        /** \brief NodalValuesMatrix constructor that initialize a Matrix object according to the node list.
         *         The associated matrix will be initialized with zeros.
         *
         * \param nodes List of pointers to the nodes of of the mesh.
         *              The order of the list must be identical to the node coefficients.
         *
         * \throw std::runtime_error if the list \a nodes is empty.
         */
        explicit NodalValuesMatrix(const std::vector<Node*>& nodes);

        /** \brief NodalValuesMatrix constructor from an existing vertical Matrix.
         *
         * \param rawMatrix The raw matrix. It must be vertical (only one column) and the row count must be the total of nodes' degrees of freedom.
         *                  This class will keep a copy of this matrix so the original could be safely deleted.
         * \param nodes List of pointers to the nodes of the matrix.
         *              The order of the list must be identical to the node coefficients.
         *
         * \throw std::runtime_error if \a rawMatrix is not vertical (column count = 1) or
         *                           if its row count is not equal to the total nodes' degrees of freedom count or
         *                           if the list \a nodes is empty.
         */
        NodalValuesMatrix(const Matrix& rawMatrix, const std::vector<Node*>& nodes);

        /** \brief Copy constructor.
         *
         * \param other The other NodalValuesMatrix to copy data from.
         *
         */
        NodalValuesMatrix(const NodalValuesMatrix& other);

        /** \brief Assignment operator
         *
         * \param other The other NodalValuesMatrix to copy data from.
         * \return A reference to this object.
         *
         */
        NodalValuesMatrix& operator=(const NodalValuesMatrix& other);

        /** \brief NodalValuesMatrix destructor.
         *
         */
        ~NodalValuesMatrix();

        /** \brief Get the associated Matrix object.
         *
         * \return The raw Matrix object of this NodalValuesMatrix.
         *         It is a vertical matrix with only one column number.
         *
         */
        Matrix& GetRawMatrix() const;

        /** \brief Get the list of node ids.
         *
         * \return The list of the node ids that this vector contains.
         *
         */
        const std::vector<unsigned int>& GetNodeIds() const;

        /** \brief Check if the NodalValuesMatrix contains a specific node.
         *
         * \param nodeId The id of the node we want to check.
         * \return true if one of the coefficient of the matrix is relative to the given \a nodeId,
         *         false otherwise.
         *
         */
        bool ContainsNode(unsigned int nodeId) const;

        /** \brief Set the value for a degree of freedom of a node.
         *
         * \param nodeId The id of the node.
         * \param degreeOfFreedomIndex The index of the degree of freedom.
         *                             It starts at 0 and can not exceed the number of freedom degree of this node minus 1.
         * \param value The value that we want to set.
         *
         * \throw std::runtime_error if \a nodeId does not match a node or if \a degreeOfFreedomIndex is out of range.
         */
        void SetValue(unsigned int nodeId, unsigned int degreeOfFreedomIndex, double value);

        /** \brief Get the value of a node's degree of freedom.
         *
         * \param nodeId The id of the node.
         * \param degreeOfFreedomIndex The index of the degree of freedom.
         *                             It starts at 0 and can not exceed the number of freedom degree of this node minus 1.
         * \return The value of the degree of freedom of the node with the given \a nodeId.
         *
         * \throw std::runtime_error if \a nodeId does not match a node or if \a degreeOfFreedomIndex is out of range.
         */
        double GetValue(unsigned int nodeId, unsigned int degreeOfFreedomIndex) const;


    private:

        void InitData(const std::vector<Node*>& nodes);
        void CheckNodeIdAndDOFValidity(unsigned int nodeId, unsigned int degreeOfFreedomIndex) const;

        Matrix* rawMatrix;
        std::vector<unsigned int> nodeIds;
        std::map<unsigned int, unsigned int> nodeIndexes;
        unsigned int degreesOfFreedomPerNode;
    };

}//namespace PetitPas


#endif // NODAL_VALUES_MATRIX_H
