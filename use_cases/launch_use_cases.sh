#!/bin/bash

PETITPAS_PATH=../src/petitpas/bin/Release/PetitPas_Release

CURRENT_DATE=$(date +%Y%m%d%H%M%S)
LOG_FILENAME=log/$CURRENT_DATE.log
RESULT_DIR=result

touch $LOG_FILENAME

$PETITPAS_PATH -i very_coarse.json -o $RESULT_DIR/very_coarse.vtk -of vtk | tee -a $LOG_FILENAME
$PETITPAS_PATH -i coarse.json -o $RESULT_DIR/coarse.vtk -of vtk | tee -a $LOG_FILENAME
$PETITPAS_PATH -i medium.json -o $RESULT_DIR/medium.vtk -of vtk | tee -a $LOG_FILENAME
#$PETITPAS_PATH -i very_very_fine.json -o $RESULT_DIR/very_very_fine.vtk -of vtk | tee -a $LOG_FILENAME
