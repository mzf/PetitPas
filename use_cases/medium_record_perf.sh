#!/bin/bash

PETITPAS_PATH=../src/petitpas/bin/Release/PetitPas_Symbols

CURRENT_DATE=$(date +%Y%m%d%H%M%S)
LOG_FILENAME=perf/$CURRENT_DATE.log
RESULT_DIR=result
PERF_DIR=perf
FLAMEGRAPH_DIR=perf/flamegraph
SVG_FILENAME=perf/$CURRENT_DATE.svg

touch $LOG_FILENAME

time perf record -F 99 -g -o $PERF_DIR/perf.data -- ./$PETITPAS_PATH -i medium.json -o $RESULT_DIR/medium.vtk -of vtk | tee -a $LOG_FILENAME
perf script -i $PERF_DIR/perf.data | $FLAMEGRAPH_DIR/stackcollapse-perf.pl | $FLAMEGRAPH_DIR/flamegraph.pl > $SVG_FILENAME

