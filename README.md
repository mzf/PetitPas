PetitPas
========

PetitPas is a [finite element](http://en.wikipedia.org/wiki/Finite_element_method) solver for mechanical structural problems.

"PetitPas" means "small step" in French.

##Features

* 2D linear triangles
* 3D linear tetrahedrons
* elastic material (Hooke's law): 2D plane stress, 2D place strain and 3D
* [JSON](https://en.wikipedia.org/wiki/JSON) file as input and/or output via [JsonCpp](https://github.com/open-source-parsers/jsoncpp)
* export output to [vtk](http://www.vtk.org/) files (use [Paraview](http://www.paraview.org/) for visualization)
* compute displacements, strains and stresses
* use of sparse matrices to reduce memory usage thanks to [Eigen](http://eigen.tuxfamily.org) library
* code fully tested with [unittest++](https://github.com/unittest-cpp)
* Object-Oriented code


##Usage

Here is a minimal input [JSON](https://en.wikipedia.org/wiki/JSON) file that defines a 2D plane strain problem with 4 elements and 6 nodes:

```
{
	"nodes":
	[
		{"id":1,"coordinates":[0,0]},
		{"id":2,"coordinates":[3,0]},
		{"id":3,"coordinates":[6,0]},
		{"id":4,"coordinates":[0,3]},
		{"id":5,"coordinates":[3,3]},
		{"id":6,"coordinates":[0,6]}
	],
	"elements":
	[
		{"id":1,"node_ids":[1,2,4],"material":"material_1"},
		{"id":2,"node_ids":[2,5,4],"material":"material_1"},
		{"id":3,"node_ids":[2,3,5],"material":"material_1"},
		{"id":4,"node_ids":[4,5,6],"material":"material_1"}
	],
	"materials":
	[
		{"id":"material_1", "young_modulus":30e3, "poisson_ratio":0.2, "hypothesis":"2d_plane_strain"}
	],
	"boundary_conditions":
	[
		{"type":"force", "node_id":4, "degree_of_freedom":0, "value":90.0},
		{"type":"force", "node_id":4, "degree_of_freedom":1, "value":-112.5},
		{"type":"force", "node_id":5, "degree_of_freedom":1, "value":-112.5},
		{"type":"force", "node_id":6, "degree_of_freedom":0, "value":15.0},
		{"type":"force", "node_id":6, "degree_of_freedom":1, "value":-37.5},
		{"type":"zero_displacement", "node_id":1, "degree_of_freedom":0},
		{"type":"zero_displacement", "node_id":1, "degree_of_freedom":1},
		{"type":"zero_displacement", "node_id":2, "degree_of_freedom":0},
		{"type":"zero_displacement", "node_id":2, "degree_of_freedom":1},
		{"type":"zero_displacement", "node_id":3, "degree_of_freedom":0},
		{"type":"zero_displacement", "node_id":3, "degree_of_freedom":1}
	]

}
```
The command line to run the simulation:

`PetitPas -i input.json -o output.json`

Add the switch `-of vtk` to export the output to [vtk](http://www.vtk.org/) format:

`PetitPas -i input.json -o output.vtk -of vtk`

Then you may open the generated file `output.vtk` in [Paraview](http://www.paraview.org/) for post-processing.

##Compilation

Open [`petitpas.workspace`](/src/petitpas.workspace) in [Code::Blocks](http://www.codeblocks.org/) IDE (version >= 13.12), select the Release target and build the software.

##Disclaimer

The output values of the software are not guaranteed, so don't build a nuclear power plant with PetitPas.

Thanks a lot to @flolagale for his advices about the Finite Element Method.







